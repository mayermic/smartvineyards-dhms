/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.validation.constraints.AssertTrue;
import org.junit.Assert;
import org.junit.Test;

public class ConfigurationControllerTest {

  @Test
  public void testInit() throws Exception {
      Configuration testConfig = new Configuration();
      Assert.assertNotNull(testConfig);
  }

  @Test
  public void testSaveConfiguration() throws Exception {

    Configuration testConfiguration = new Configuration();
    String testFile = "testDHMS1.config";

    int testInt = 0;
    double testDouble = 0.0;

    testConfiguration.setSystemUpdatePeriod(testInt);
    testConfiguration.setSelfHealingPeriod(testInt);
    testConfiguration.setWarningPeriod(testInt);
    testConfiguration.setFailurePeriod(testInt);
    testConfiguration.setSelfHealingPeriod(testInt);
    testConfiguration.setMoistureWarningLevel(testDouble);
    testConfiguration.setMoistureFailureLevel(testDouble);
    testConfiguration.setInternalTemperatureWarningLevel(testDouble);
    testConfiguration.setInternalTemperatureFailureLevel(testDouble);
    testConfiguration.setExternalTemperatureWarningLevel(testDouble);
    testConfiguration.setExternalTemperatureFailureLevel(testDouble);
    testConfiguration.setPowerWarningLevel(testDouble);
    testConfiguration.setPowerFailureLevel(testDouble);
    testConfiguration.setCapacitorWarningLevel(testDouble);
    testConfiguration.setCapacitorFailureLevel(testDouble);

    ConfigurationController.configuration = testConfiguration;
    ConfigurationController.saveConfiguration(testFile);

    FileReader fr = new FileReader(testFile);
    BufferedReader br = new BufferedReader(fr);

    int testInt2 = 1;
    double testDouble2 = 1.1;
    do{
      for (int i = 0; i < 4; i++) {
        testInt2 = Integer.valueOf(br.readLine());
        assert testInt2 == testInt;
        testInt2 = 1;
      }
      for (int j = 0; j < 10; j++) {
        testDouble2 = Double.valueOf(br.readLine());
        assert testDouble2 == testDouble;
        testDouble2 = 1.1;
      }
    }while(br.ready());
    testConfiguration = null;
    br.close();
    fr.close();
    File file = new File(testFile);
    file.delete();
  }

  @Test
  public void testLoadConfiguration() throws Exception {

    Configuration testConfiguration = new Configuration();
    ConfigurationController.configuration = testConfiguration;
    String testFile = "testDHMS2.config";
    File tf = null;
    tf = new File(testFile);
    FileWriter fw = null;
    fw = new FileWriter(tf, false);

    int testInt = 99;
    double testDouble = 9.9;

    for(int i = 0; i < 4; i++){
      fw.write(Integer.toString(testInt) + "\n");
    }
    for(int j = 0; j < 10; j++){
      fw.write(Double.toString(testDouble) + "\n");
    }
    fw.flush();
    fw.close();

    ConfigurationController.loadConfiguration(testFile);
    assert (testInt == ConfigurationController.getSystemUpdatePeriod());
    assert (testInt == ConfigurationController.getWarningPeriod());
    assert (testInt == ConfigurationController.getFailurePeriod());
    assert (testInt == ConfigurationController.getSelfHealingPeriod());
    assert (testDouble == ConfigurationController.getMoistureWarningLevel());
    assert (testDouble == ConfigurationController.getMoistureFailureLevel());
    assert (testDouble == ConfigurationController.getInternalTemperatureWarningLevel());
    assert (testDouble == ConfigurationController.getInternalTemperatureFailureLevel());
    assert (testDouble == ConfigurationController.getExternalTemperatureWarningLevel());
    assert (testDouble == ConfigurationController.getExternalTemperatureFailureLevel());
    assert (testDouble == ConfigurationController.getPowerWarningLevel());
    assert (testDouble == ConfigurationController.getPowerFailureLevel());
    assert (testDouble == ConfigurationController.getCapacitorWarningLevel());
    assert (testDouble == ConfigurationController.getCapacitorFailureLevel());
    tf.delete();
  }
}
