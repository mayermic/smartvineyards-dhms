package com.nemo.dhms.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DatabaseConnectorTest {

  private final String testName = "TestDevice";
  private final LocalDateTime testTimeNow = LocalDateTime.now();
  private final LocalDateTime testTime1 = LocalDateTime.of(2017, 1, 1, 0, 0);
  private final LocalDateTime testTime2 = LocalDateTime.of(2018, 1, 2, 0, 0);
  private final LocalDateTime testTime3 = LocalDateTime.now(ZoneId.of("GMT"));


  private final DeviceHistoryEntry testDevice1
      = new DeviceHistoryEntry(testName, testTimeNow, DeviceStatus.INACTIVE);
  private final DeviceHistoryEntry testDevice2
      = new DeviceHistoryEntry(testName, testTime1, DeviceStatus.ACTIVE);
  private final DeviceHistoryEntry testDevice3
      = new DeviceHistoryEntry(testName, testTime2, DeviceStatus.ACTIVE);
  // diff time zone
  private final DeviceHistoryEntry testDevice4
      = new DeviceHistoryEntry(testName, testTime3, DeviceStatus.ACTIVE);


  private final MeshHistoryEntry testMesh1
      = new MeshHistoryEntry(testName, testTimeNow, MeshStatus.INACTIVE);
  private final MeshHistoryEntry testMesh2
      = new MeshHistoryEntry(testName, testTime1, MeshStatus.ACTIVE);
  private final MeshHistoryEntry testMesh3
      = new MeshHistoryEntry(testName, testTime2, MeshStatus.ACTIVE);
  // diff time zone
  private final MeshHistoryEntry testMesh4
      = new MeshHistoryEntry(testName, testTime3, MeshStatus.ACTIVE);

  private DatabaseConnector databaseConnector;

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void testDeviceStatuses() throws Exception {
    List<DeviceHistoryEntry> entries = databaseConnector.getDeviceStatus(testName);
    int count = entries.size();

    assertTrue(databaseConnector.putDeviceStatus(testDevice1));

    entries = databaseConnector.getDeviceStatus(testName);
    assertEquals(count + 1, entries.size());

    assertFalse(databaseConnector.getDeviceStatus(testName).isEmpty());
    assertTrue(databaseConnector.getDeviceStatus("InvalidDevice").isEmpty());

    assertTrue(databaseConnector.removeDeviceStatus(testDevice1));
    entries = databaseConnector.getDeviceStatus(testName);
    assertEquals(count, entries.size());
  }


  @Test
  // Add test to test appended devices/meshes and see if it updates existing device/mesh or add new
  // Add test to test if time will stay the same after inputting data in db.
  // Test preservation of order
  public void testAppendDeviceStatus() throws Exception {
    List<DeviceHistoryEntry> entries = databaseConnector.getDeviceStatus(testName);
    int count = entries.size();

    assertTrue(databaseConnector.putDeviceStatus(testDevice1));
    assertTrue(databaseConnector.putDeviceStatus(testDevice2));
    assertTrue(databaseConnector.putDeviceStatus(testDevice3));

    entries = databaseConnector.getDeviceStatus(testName);
    assertEquals(count + 3, entries.size());

    DeviceHistoryEntry devEntry1 = databaseConnector.getDeviceStatus(testName).get(0);
    DeviceHistoryEntry devEntry2 = databaseConnector.getDeviceStatus(testName).get(1);
    DeviceHistoryEntry devEntry3 = databaseConnector.getDeviceStatus(testName).get(2);

    assertEquals(devEntry1, testDevice1);
    assertEquals(devEntry2, testDevice2);
    assertEquals(devEntry3, testDevice3);

    assertTrue(databaseConnector.removeDeviceStatus(testDevice1));
    assertTrue(databaseConnector.removeDeviceStatus(testDevice2));
    assertTrue(databaseConnector.removeDeviceStatus(testDevice3));
    entries = databaseConnector.getDeviceStatus(testName);
    assertEquals(count, entries.size());
  }


  @Test
  // Add device with diff time zone
  public void testDeviceTimeZone() throws Exception {

    assertTrue(databaseConnector.putDeviceStatus(testDevice4));

    DeviceHistoryEntry device = databaseConnector.getDeviceStatus(testName).get(0);

    String stringTime = testTime3.toString();
    LocalDateTime localTime = LocalDateTime.parse(stringTime);

    assertEquals(device.getDate(), localTime);

    assertTrue(databaseConnector.removeDeviceStatus(testDevice4));

  }

  @Test
  public void testMeshStatuses() throws Exception {
    List<MeshHistoryEntry> entries = databaseConnector.getMeshStatus(testName);
    int count = entries.size();

    assertTrue(databaseConnector.putMeshStatus(testMesh1));

    entries = databaseConnector.getMeshStatus(testName);
    assertEquals(count + 1, entries.size());

    assertFalse(databaseConnector.getMeshStatus(testName).isEmpty());
    assertTrue(databaseConnector.getMeshStatus("InvalidMesh").isEmpty());

    assertTrue(databaseConnector.removeMeshStatus(testMesh1));
    entries = databaseConnector.getMeshStatus(testName);
    assertEquals(count, entries.size());
  }

  @Test
  // Add test to test appended devices/meshes and see if it updates existing device/mesh or add new
  // Add test to test if time will stay the same after inputting data in db.
  // Test preservation of order
  public void testAppendMeshStatus() throws Exception {
    List<MeshHistoryEntry> entries = databaseConnector.getMeshStatus(testName);
    int count = entries.size();

    assertTrue(databaseConnector.putMeshStatus(testMesh1));
    assertTrue(databaseConnector.putMeshStatus(testMesh2));
    assertTrue(databaseConnector.putMeshStatus(testMesh3));

    entries = databaseConnector.getMeshStatus(testName);
    assertEquals(count + 3, entries.size());

    MeshHistoryEntry mesEntry1 = databaseConnector.getMeshStatus(testName).get(0);
    MeshHistoryEntry mesEntry2 = databaseConnector.getMeshStatus(testName).get(1);
    MeshHistoryEntry mesEntry3 = databaseConnector.getMeshStatus(testName).get(2);

    assertEquals(mesEntry1, testMesh1);
    assertEquals(mesEntry2, testMesh2);
    assertEquals(mesEntry3, testMesh3);

    assertTrue(databaseConnector.removeMeshStatus(testMesh1));
    assertTrue(databaseConnector.removeMeshStatus(testMesh2));
    assertTrue(databaseConnector.removeMeshStatus(testMesh3));
    entries = databaseConnector.getMeshStatus(testName);
    assertEquals(count, entries.size());
  }

  @Test
  // Add mesh with diff time zone
  public void testMeshTimeZone() throws Exception {

    assertTrue(databaseConnector.putMeshStatus(testMesh4));

    MeshHistoryEntry mesh = databaseConnector.getMeshStatus(testName).get(0);

    String stringTime = testTime3.toString();
    LocalDateTime localTime = LocalDateTime.parse(stringTime);

    assertEquals(mesh.getDate(), localTime);

    assertTrue(databaseConnector.removeMeshStatus(testMesh4));

  }

  @Test
  public void putMeshStatus() throws Exception {

  }

  @Test
  public void getMeshStatus() throws Exception {

  }

  @Autowired
  private void setDatabaseConnector(DatabaseConnector databaseConnector) {
    this.databaseConnector = databaseConnector;
  }
}
