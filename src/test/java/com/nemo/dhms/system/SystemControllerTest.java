package com.nemo.dhms.system;

import static junit.framework.TestCase.assertTrue;

import com.nemo.dhms.config.ConfigurationController;
import com.nemo.dhms.data.DataController;
import java.util.Collection;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class SystemControllerTest {

  private static String testDeviceID = "C2D9BEA3";
  private static String testMeshID = "C2D9BEA3";
  //private static int meshCount = 17;
  //private static int deviceCount = 100;

  @BeforeClass
  public static void init() {
    ConfigurationController.init();

    String sensmitKey = System.getenv("SENSMIT_KEY");
    ConfigurationController.setSensmitApiKey(sensmitKey);

    String googleMapsKey = System.getenv("GOOGLE_MAPS_KEY");
    ConfigurationController.setGoogleMapsApiKey(googleMapsKey);

    DataController.init();

    SystemController.init();
  }

  /**
   * Method to test the parsing of a Device passed into the system controller TODO: DataController
   * needs implementation finished or mocked
   */
  /**@Test
  /*public void parseDevice() {
    ArrayList<DeviceHistoryEntry> list = SystemController.parseDevice(testDeviceID);
    System.out.println("Device ID: " + testDeviceID);
    if( list.equals(null) )
    {
      System.out.printf("List is null");
    }
    else if ( list.size() > 0 ) {
      for (int i = 0; i < list.size(); i++) {
        System.out.println("Timestamp" + list.get(i).getDate());
        System.out.println("Status:" + list.get(i).getStatus());
      }
    }
    else
    {
      System.out.println("Unknown error");
    }
  }*/

  @Test
  public void getDeviceCount() {
    assert (SystemController.getDeviceCount() >= 0);
  }

  @Test
  public void getMeshCount() {
    assert (SystemController.getMeshCount() >= 0);
  }

  @Test
  public void getDeviceHealthyCount() {
    assert (SystemController.getDeviceHealthyCount() >= 0);
  }

  @Test
  public void getMeshHealthyCount() {
    assert (SystemController.getMeshHealthyCount() >= 0);
  }

  @Test
  public void getDeviceIds() {
    assert (!SystemController.getDigitalIds().isEmpty());
    //assert (SystemController.getDigitalIds().size() == deviceCount);
    assert (SystemController.getDigitalIds().contains(testDeviceID));
  }

  @Test
  public void getMeshIds() {
    assert (!SystemController.getMeshIds().isEmpty());
    //assert (SystemController.getMeshIds().size() == meshCount);
    assert (SystemController.getMeshIds().contains(testMeshID));
  }

  @Test
  public void getAllDevices() {
    Collection<Device> list = SystemController.getAllDevices();

    //assert (list.size() == deviceCount);
    assert (list.size() >= 0);
  }

  @Test
  public void getAllMeshes() {
    Collection<Mesh> list = SystemController.getAllMeshes();

    //assert (list.size() == meshCount);
    assert (list.size() >= 0);
  }

  @Test
  public void getDevice() {
    Device temp = SystemController.getDevice(testDeviceID);

    assert (temp != null);
    assert (temp.getDigitalId().equals(testDeviceID));
  }

  @Test
  public void getMesh() {
    Mesh temp = SystemController.getMesh(testMeshID);

    assert (temp != null);
    assert (temp.getMeshId().equals(testMeshID));
  }

  @Test
  public void getDeviceStatus() {
    assert (SystemController.getDeviceStatus(testDeviceID) == DeviceStatus.ACTIVE);
  }

  @Test
  public void getMeshStatus() {
    assert (SystemController.getMeshStatus(testMeshID) == MeshStatus.ACTIVE);
  }

  @Test
  public void getDeviceHistory() {
    String deviceID = "DHIOEWC";
  }

  @Test
  public void getMeshHistory() {
  }

  @Test
  public void getJiraHTTP200Status() {
    assertTrue(SystemController.getJiraHTTP200Status());
  }
}