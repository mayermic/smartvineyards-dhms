package com.nemo.dhms.issues;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import com.nemo.dhms.system.Device;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.Project;
import org.junit.Test;

public class JiraControllerTest {

  private String defaultProject = new String("DHMS");
  private String defaultIssueType = new String("Hardware issue");
  private String statusField = new String("customfield_10100");
  private String defaultStatus = new String("Need Support");

  @Test
  public void getStatusOfJira() {
    assertTrue(JiraController.getStatusOfJira());
  }

  /*
  @Before
  public void clearSysCreatedList(){
    JiraController.clearTicketLists();
  }
  */

  @Test
  public void isValidTypeName() {
    // "Epic" is a built-in Jira type that should always be there.
    try {
      assertTrue(JiraController.isValidTypeName("Epic"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      assertFalse(JiraController.isValidTypeName("SomeInvalidTypeName!"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void isValidPriorityId() throws Exception {
    // The highest priority starts at "1" and the rest are descending values
    assertTrue(JiraController.isValidPriorityId("1"));
    assertFalse(JiraController.isValidPriorityId("-1"));
  }

  @Test
  public void isValidProjectKey() throws Exception {
    List<Project> projects = JiraController.client.getProjects();
    assertFalse(projects.isEmpty());

    Project project = projects.get(0);
    assertTrue(JiraController.isValidProjectKey(project.getKey()));

    assertFalse(JiraController.isValidProjectKey("SomeInvalidProjectName"));
  }

  @Test
  public void dhmsIsValidProjectKey() {
    try {
      assertTrue(JiraController.isValidProjectKey(defaultProject));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void isValidStatus() throws Exception {

    if (JiraController.isValidProjectKey(defaultProject) && JiraController.isValidTypeName(
        defaultIssueType)) {

      List<CustomFieldOption> list = JiraController.client
          .getCustomFieldAllowedValues(statusField, defaultProject, defaultIssueType);

      for (CustomFieldOption item : list) {
        System.out.println(item.toString());
      }

      assertFalse(list.isEmpty());
      CustomFieldOption status = list.get(0);
      String statusName = status.getValue();
      assertTrue(JiraController.isValidStatus(statusName, defaultProject, defaultIssueType));

    }
    String invalidStatus = "InvalidStatus";
    assertFalse(JiraController.isValidStatus(invalidStatus, defaultProject, defaultIssueType));
  }

  @Test
  public void needSupportIsValidStatus() {
    try {
      assertTrue(JiraController.isValidStatus(defaultStatus, defaultProject, defaultIssueType));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void removefromTicketsList() {
    String deviceID = new String();
    assertFalse(JiraController.removeFromTicketsLists(deviceID));
  }

  @Test
  public void createNewDeviceIssueFor() throws JiraException {
    Device testDevice = new Device();
    //testDevice.setDigitalId("");
    assertFalse(JiraController.createNewDeviceIssueFor(testDevice, "InvalidTimePeriod"));

    testDevice.setDigitalId("NCC-1701");
    testDevice.setMeshId("MDD+2712");
    testDevice.setHardwareId("ENTERPRISE");
    testDevice.setStatus(DeviceStatus.WARNING);
    String validTime = "Thursday, September 8, 1966 T:20:30-21:30";
    assertTrue(JiraController.createNewDeviceIssueFor(testDevice, validTime));
  }

  @Test
  public void changeStatusDueToSelfHealingForDevice() throws JiraException {
    assertFalse(JiraController.changeStatusDueToSelfHealingForDevice("InvalidDeviceID",
        "InvalidTimePeriod", "InvalidTimePeriod"));

    Device testDevice = new Device();
    //String digitalID = "NCC-2893";
    String digitalID = "NCC-Term";
    testDevice.setDigitalId(digitalID);
    testDevice.setMeshId("MDD+3904");
    testDevice.setHardwareId("ENTERPRISE");
    String validTime = "Monday, September 28, 1987 T:20:30-21:30";
    assertTrue(JiraController.createNewDeviceIssueFor(testDevice, validTime));

    String validStartTime = "Monday, September 28, 1987 T:20:30-21:30";
    String validEndTime = "Monday, September 28, 1987 T:20:30-21:30";
    assertTrue(JiraController
        .changeStatusDueToSelfHealingForDevice(digitalID, validStartTime, validEndTime));
  }

  @Test
  public void parseDevicesForIssuesToCloseTicket() throws JiraException {

    Device t1 = new Device();
    String digitalID = "NCC-Term";
    t1.setDigitalId(digitalID);
    t1.setMeshId("MDD+3904");
    t1.setHardwareId("ENTERPRISE");
    //History
    LocalDateTime ValidDatetime = LocalDateTime.now();
    DeviceHistoryEntry t1History = new DeviceHistoryEntry(digitalID, ValidDatetime, DeviceStatus.FAILURE);
    List<DeviceHistoryEntry> t1List = new ArrayList<>();
    t1List.add(t1History);
    t1.setHistory(t1List);

    assertTrue(
        JiraController.createNewDeviceIssueFor(t1, t1.getHistory().get(0).getDate().toString()));

    ValidDatetime = LocalDateTime.now();
    t1History = new DeviceHistoryEntry(digitalID, ValidDatetime, DeviceStatus.ACTIVE);
    t1List.add(0, t1History);
    t1.setHistory(t1List);

    Collection<Device> devices = new ArrayList<>();
    devices.add(t1);
    assertTrue(JiraController.parseDevicesForIssues(devices));
  }

  @Test
  public void parseDevicesForIssuesToCreateTicket() throws JiraException {
    Device t1 = new Device();
    String digitalID = "NCC-Term";
    t1.setDigitalId(digitalID);
    t1.setMeshId("MDD+3904");
    t1.setHardwareId("ENTERPRISE");
    //History
    LocalDateTime ValidDatetime = LocalDateTime.now();
    DeviceHistoryEntry t1History = new DeviceHistoryEntry(digitalID, ValidDatetime, DeviceStatus.FAILURE);
    List<DeviceHistoryEntry> t1List = new ArrayList<>();
    t1List.add(t1History);
    t1.setHistory(t1List);

    Collection<Device> devices = new ArrayList<>();
    devices.add(t1);
    assertTrue(JiraController.parseDevicesForIssues(devices));
  }

  @Test
  public void removeFromTicketsLists() {
    JiraTicket tic = new JiraTicket();
    tic.setTicketNodeID("DHMS-999");
    tic.setTicketSummary("Enterprise");
    String ticketID = new String(tic.getTicketNodeID());
    String validStartTime = "Monday, September 28, 1987 T:20:30-21:30";

    assertTrue(JiraController.addToTicketsList(tic, validStartTime, ticketID));
    assertTrue( JiraController.removeFromTicketsLists(tic.getTicketNodeID()) );
  }
}
