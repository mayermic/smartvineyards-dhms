package com.nemo.dhms.users;

import org.junit.BeforeClass;
import org.junit.Test;

public class JiraAuthenticatorTest {

  @BeforeClass
  public static void init() {
    JiraAuthenticator.setBaseUrl(System.getenv("JIRA_URL"));
  }

  @Test
  public void authenticate_bad() throws Exception {
    JiraUser user = JiraAuthenticator.authenticate("bad_username", "bad_password");
    assert (user == null);
  }

  @Test
  public void authenticate_good() throws Exception {
    String username = System.getenv("JIRA_USER");
    String password = System.getenv("JIRA_PASS");
    JiraUser user = JiraAuthenticator.authenticate(username, password);
    assert (user != null);
    assert (user.isAdmin());
  }

}