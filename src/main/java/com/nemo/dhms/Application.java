/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms;

import com.nemo.dhms.config.ConfigurationController;
import com.nemo.dhms.data.DataController;
import com.nemo.dhms.system.SystemController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * TODO: Write class Javadoc
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.nemo.dhms.data", "com.nemo.dhms.system",
    "com.nemo.dhms.dashboard", "com.nemo.dhms.api", "com.nemo.dhms.issues", "com.nemo.dhms.config", "com.nemo.dhms.users"})
@EnableJpaRepositories
@EnableTransactionManagement
@EnableScheduling
public class Application implements ApplicationListener<ApplicationReadyEvent> {

  private static final Logger log = LoggerFactory.getLogger(Application.class);

  //TODO: Write JavaDoc
  public static void main(String[] args) {
    log.info("Initializing Configuration Controller...");
    ConfigurationController.init();

    log.info("Loading 'dhms.config'");
    ConfigurationController.loadConfiguration("dhms.config");

    log.info("Setting SensMit API key...");
    String sensmitKey = System.getenv("SENSMIT_KEY");
    ConfigurationController.setSensmitApiKey(sensmitKey);

    log.info("Setting Google Maps API key...");
    String googleMapsKey = System.getenv("GOOGLE_MAPS_KEY");
    ConfigurationController.setGoogleMapsApiKey(googleMapsKey);

    log.info("Initializing Data Controller...");
    DataController.init();

    log.info("Initializing System Controller...");
    SystemController.init();

    log.info("Starting Spring application...");
    SpringApplication.run(Application.class);
  }

  @Override
  public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

  }
}
