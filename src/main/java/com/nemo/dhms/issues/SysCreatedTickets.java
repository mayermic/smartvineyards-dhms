package com.nemo.dhms.issues;

/**
 * SysCreatedTickets: Purpose of this class is to create a object to store the most relevant
 * attributes/data from created JIRA Ticket for later retrieval and modification. This class is not
 * to used outside of the context of the JiraController Class.
 */
class SysCreatedTickets {

  private String ticketId = "";
  private String timeOfFirstInactivePeriod = "";
  private String ticketProject; //REQUIRED
  private String ticketType; //REQUIRED
  private String ticketSummary; //REQUIRED
  private String digitalID; //REQUIRED

  public SysCreatedTickets(String ticketID, String timeOfFirstInactivePeriod,
      String ticketProject, String ticketType, String ticketSummary, String digitalID) {
    this.ticketId = ticketID;
    this.timeOfFirstInactivePeriod = timeOfFirstInactivePeriod;
    this.ticketProject = ticketProject;
    this.ticketType = ticketType;
    this.ticketSummary = ticketSummary;
    this.digitalID = digitalID;
  }

  public String getTimeOfFirstInactivePeriod() {
    return timeOfFirstInactivePeriod;
  }

  public String getTicketId() {
    return ticketId;
  }

  public String getTicketProject() {
    return ticketProject;
  }

  public String getTicketType() {
    return ticketType;
  }

  public String getTicketSummary() {
    return ticketSummary;
  }

  public String getDigitalID() {
    return digitalID;
  }

  public void setDigitalID(String digitalID) {
    this.digitalID = digitalID;
  }

}
