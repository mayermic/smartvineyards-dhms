package com.nemo.dhms.issues;

import com.nemo.dhms.system.Device;
import com.nemo.dhms.system.DeviceStatus;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.Field.ValueType;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.IssueType;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.Priority;
import net.rcarz.jiraclient.Project;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.RestException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JiraController Class: Purpose of this class is facilitate the creation of a Jira Ticket
 */
public class JiraController {

  private static final Logger log = LoggerFactory.getLogger(JiraController.class);
  static JiraClient client;
  static List<SysCreatedTickets> createdTickets = new ArrayList<>();
  private static String defaultProject = new String("DHMS");
  private static String defaultIssueType = new String("Hardware issue");
  private static String defaultPriority = new String("3");       //"3" corresponds to Medium
  private static String defaultAssignee = new String(System.getenv("JIRA_USER").toString());
  private static String defaultStatus = new String("Need Support");
  private static String defaultCloseStatus = new String("Installed in the field");
  private static String statusField = new String("customfield_10100");
  private static String ticketDone = new String("Done");


  static {
    String baseUrl = System.getenv("JIRA_URL");
    String username = System.getenv("JIRA_USER");
    String password = System.getenv("JIRA_PASS");
    BasicCredentials credentials = new BasicCredentials(username, password);
    try {
      client = new JiraClient(baseUrl, credentials);
    } catch (JiraException e) {
      e.printStackTrace();
    }
  }

  public JiraController() {
  }

  /**
   * Method to check the status of JIRA and make sure its up
   */
  public static boolean getStatusOfJira() {
    return sendHttpRequest(System.getenv("JIRA_URL"));
  }

  public static String getDefaultPriority() {
    return defaultPriority;
  }

  public static void setDefaultPriority(String defaultPriority) {
    if (defaultPriority.isEmpty()) {
      JiraController.defaultPriority = new String("3");       //"3" corresponds to Medium
    } else {
      JiraController.defaultPriority = defaultPriority;
    }
  }

  public static String getDefaultAssignee() {
    return defaultAssignee;
  }

  public static void setDefaultAssignee(String defaultAssignee) {
    if (defaultAssignee.isEmpty()) {
      JiraController.defaultAssignee = new String(System.getenv("JIRA_USER").toString());
    } else {
      JiraController.defaultAssignee = defaultAssignee;
    }
  }

  /**
   * Method to check validity of Ticket Type against current JIRA settings
   *
   * @param typeName String; Value of the typeName variable; Refers to JIRA's Type field
   * @return Boolean; Return true if typeName is in the current JIRA settings else return stackTrace
   * and false value
   */
  static protected boolean isValidTypeName(String typeName) {
    try {
      List<IssueType> types = client.getIssueTypes();
      for (IssueType type : types) {
        if (typeName.equals(type.getName())) {
          return true;
        }
      }
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Method to check validity of Ticket priority against current JIRA settings
   *
   * @param priorityId String; Value of the priorityID variable; Refers to JIRA's priority field
   * @return Boolean; Return true if priority is in the current JIRA settings else return stackTrace
   * and false value
   */
  static protected boolean isValidPriorityId(String priorityId) {
    try {
      List<Priority> priorities = client.getPriorities();
      for (Priority priority : priorities) {
        if (priorityId.equals(priority.getId())) {
          return true;
        }
      }
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Method to check validity of Ticket project value against current JIRA settings
   *
   * @param projectKey String; Value of the projectKey variable; Refers to JIRA's project field
   * @return Boolean; Return true if project is in the current JIRA settings else return false and a
   * stackTrace
   */
  static protected boolean isValidProjectKey(String projectKey) {
    try {
      List<Project> projects = client.getProjects();
      for (Project project : projects) {
        if (projectKey.equals(project.getKey())) {
          return true;
        }
      }
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Method to check the validity of the Ticket status value against current JIRA settings
   *
   * @param status String; Refers to the JIRA's status field
   * @return Boolean; Return true if status is in the current JIRA settings else return false and a
   * StackTrace
   */
  static protected boolean isValidStatus(String status, String projectKey, String issueType) {
    try {
      if (isValidProjectKey(projectKey) && isValidTypeName(issueType)) {
        List<CustomFieldOption> list = client
            .getCustomFieldAllowedValues(statusField, projectKey, issueType);
        for (CustomFieldOption item : list) {
          if (status.toString().equals(item.toString())) {
            return true;
          }
        }
      }
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Method to create a new Issue/Ticket for a Device
   *
   * @param deviceID Device object; Used for most of the information for the JIRA ticket
   * @param timeOfFirstInactivePeriod String; Value of the first time in which the device was not
   * active
   * @return Boolean; Return true if the issue was successfully created
   * @throws JiraException Exception from the JiraClient Library
   */
  static protected boolean createNewDeviceIssueFor(Device deviceID,
      String timeOfFirstInactivePeriod)
      throws JiraException {

    if (deviceID.getDigitalId() == null) {
      return false;
    }

    JiraTicket newIssue = new JiraTicket();

    if (isValidProjectKey(defaultProject)) {
      newIssue.setTicketProject(defaultProject);
    }

    if (isValidTypeName(defaultIssueType)) {
      newIssue.setTicketType(defaultIssueType);
    }

    String ticketSummaryBuilder = new String("Hardware Issue: Status: " +
        deviceID.getStatus() + ": Device: " + deviceID.getDigitalId());
    newIssue.setTicketSummary(ticketSummaryBuilder);

    newIssue.setTicketAssignee(defaultAssignee);

    if (isValidPriorityId(defaultPriority)) {
      newIssue.setTicketPriority(defaultPriority);
    }

    newIssue.setTicketComponents(deviceID.getHardwareId());
    newIssue.setTicketLabels("Node");

    if (isValidStatus(defaultStatus, defaultProject, defaultIssueType)) {
      newIssue.setTicketStatus(defaultStatus.toString());
    }

    String descriptionBuilder = new String(
        "\nDigitalId: " + deviceID.getDigitalId() +
            "\nHardware ID: " + deviceID.getHardwareId() +
            "\nMesh ID: " + deviceID.getMeshId() +
            "\nLast Capture Time: " + deviceID.getLastCaptureTime() +
            "\nFirst Inactive Time: " + timeOfFirstInactivePeriod +
            "\nStatus: " + deviceID.getStatus() +
            "\nLocation: " + deviceID.getLocation() +
            "\nLast System Update: " + deviceID.getLastSystemUpdate());

    newIssue.setTicketDescription(descriptionBuilder);

    newIssue.setTicketNodeID(deviceID.getDigitalId());
    newIssue.setTicketNodePIN(deviceID.getMeshId());

    return createIssue(newIssue, timeOfFirstInactivePeriod);
  }

  /**
   * Method to update the issue in case of Device self-healing
   *
   * @param deviceID String; Contains the id value of the Device
   * @param timeOfFirstInactivePeriod String; Value of the first time in which the device was not
   * active
   * @param timePeriodofSelfHealing String; Value of the time in which the device self-healed
   * @return Boolean; True is the issue is updated, false otherwise
   * @throws JiraException Exception from the JiraClient Library
   */
  static protected boolean changeStatusDueToSelfHealingForDevice(String deviceID,
      String timeOfFirstInactivePeriod, String timePeriodofSelfHealing) throws JiraException {

    if (!isValidStatus(defaultCloseStatus, defaultProject, defaultIssueType)) {
      return false;
    }

    if (createdTickets == null || createdTickets.isEmpty()) {
      return false;
    }

    for (SysCreatedTickets item : createdTickets) {
      if (item.getTimeOfFirstInactivePeriod().equals(timeOfFirstInactivePeriod)
          && item.getTicketSummary().endsWith(deviceID)) {
        Issue ticketToBeUpdated = client.getIssue(item.getTicketId());

        //Add comment to the ticket with the relevant information
        ticketToBeUpdated.addComment(
            "\nDevice went inactive at: " + timeOfFirstInactivePeriod +
                "\nDevice self-healed at: " + timePeriodofSelfHealing);

        //Update the ticket to reflect the self-healing
        setStatus(ticketToBeUpdated.getKey(), defaultCloseStatus);
        removeFromTicketsLists(deviceID);
        ticketToBeUpdated.transition().execute(ticketDone);
        return true;
      }
    }
    return false;
  }


  /**
   * Method for System Controller to easily pass in List of Devices and let JiraController handle
   * parsing and exception handling
   *
   * @param devices List of Devices; Required for proper parsing
   * @return Boolean; True if list is successfully parse, false otherwise
   * @throws JiraException Standard JiraException for jira-client's library catch for debug
   * purposes;
   */
  public static boolean parseDevicesForIssues(Collection<Device> devices) throws JiraException {
    //Check if List is null or empty
    if (devices == null || devices.isEmpty()) {
      return false;
    }

    //JIRA Site HTTP Status 200
    if (!getStatusOfJira()) {
      return false;
    }

    boolean ticketIsMade = false;
    boolean deviceIsHealthy = false;

    //1. Loop through list for device status inactive
    for (Device device : devices) {
      //Status is inactive
      if (device.getHistory() == null || device.getHistory().size() == 0) {
        continue;
      }

      if (device.getHistory().get(0).getStatus().equals(DeviceStatus.ACTIVE) ||
          device.getHistory().get(0).getStatus().equals(DeviceStatus.HEALTHY)) {
        deviceIsHealthy = true;
      }

      //2. Loop through createdTickets list if a ticket exists for this device
      String timeOfFirstInactivePeriod = new String();
      for (SysCreatedTickets ticket : createdTickets) {
        if (ticket.getDigitalID().equals(device.getDigitalId())) {
          //Ticket exists, ignore
          ticketIsMade = true;
          timeOfFirstInactivePeriod = ticket.getTimeOfFirstInactivePeriod();
          break;
        }
      }

      //3. Check status for it
      //ticketIsMade, deviceIsHealthy, Action
      //True, True,  Modify ticket to close ticket
      //True, False, Ignore
      //False, True, Ignore
      //False, False, Create ticket with Device Information
      if (!ticketIsMade && !deviceIsHealthy) {

        return createNewDeviceIssueFor(device, device.getHistory().get(0).getDate().toString());

      } else if (ticketIsMade && deviceIsHealthy) {

        //Ticket exists and device is Active or Healthy, modify to close ticket
        //Method also removes the ticket from the System created Tickets List
        return changeStatusDueToSelfHealingForDevice(device.getDigitalId(),
            timeOfFirstInactivePeriod, device.getHistory().get(0).getDate().toString());
      }
    }
    return false;
  }

  /**
   * Method to remove a device from the list. This method does not modify the while iterating over
   * it.
   *
   * @param deviceID String; DigitalID of the Device
   * @return boolean value depending on whether it gets removed from the list
   */
  static protected boolean removeFromTicketsLists(String deviceID) {
    //Check if List is null or empty
    if (createdTickets == null || createdTickets.isEmpty()) {
      return false;

    } else if (deviceID != null && !deviceID.isEmpty()) {

      List<SysCreatedTickets> toRemove = new ArrayList<>();

      for (SysCreatedTickets ticket : createdTickets) {
        if (!ticket.getTicketSummary().isEmpty() || ticket.getTicketSummary() != null) {
          if (ticket.getTicketSummary().contains(deviceID)) {
            toRemove.add(ticket);
          }
        }
      }
      createdTickets.removeAll(toRemove);
      return true;
    }
    return false;
  }

  /**
   * Method to create a JIRA Ticket with a JiraTicket object
   *
   * @param tic JiraTicket object; Contains all the relevant values
   * @return Boolean; true if ticket created successfully, false if not and returns StackTrace
   */
  static private boolean createIssue(JiraTicket tic, String timeOfFirstInactivePeriod) {
    try {
      Issue temp;
      if (isValidTypeName(tic.getTicketType()) && isValidPriorityId(tic.getTicketPriority())
          && isValidProjectKey(tic.getTicketProject())) {
        Issue newIssue = client.createIssue(tic.getTicketProject(), tic.getTicketType())
            .field(Field.SUMMARY, tic.getTicketSummary())
            .field(Field.ASSIGNEE, tic.getTicketAssignee())
            .field(Field.PRIORITY, Field.valueById(tic.getTicketPriority()))
            .field(Field.DESCRIPTION, tic.getTicketDescription())
            //.field(Field.REPORTER, tic.getTicketReporter()) // READ ONLY - DONOT UNCOMMENT
            .field("customfield_10101", tic.getTicketNodeID())
            .field("customfield_10102", tic.getTicketNodePIN())
            .field("customfield_10104", tic.getTicketDataStreamURL())
            .execute("Open");
        //.field(Field.STATUS, tic.getTicketStatus())
        temp = client.getIssue(newIssue.getKey());//Keep for the update
        log.debug("\n" + newIssue + "==" + temp);
      } else {
        return false;
      }

      if (!temp.getKey().isEmpty()) {
        Issue issue = client.getIssue(temp.getKey());
        issue.update()
            .field(Field.LABELS, new ArrayList() {{
              add(tic.getTicketLabels());
            }})
            .execute();
        setStatus(temp.getKey(), tic.getTicketStatus());
      }
      System.out.println("\nATTENTION: " + temp + "\n");
      addToTicketsList(tic, timeOfFirstInactivePeriod, temp.getKey().toString());

      return true;
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }


  /**
   * Method to add the created ticket to the List of System Created Tickets
   *
   * @param tic JiraTicket object; Contains the information for the Jira Ticket
   * @param timeOfFirstInactivePeriod String; Time of the first inactive period
   * @param ticketID String; ID of the ticket in Jira
   * @return Boolean; True if the ticket is added to the Created Tickets Lists, false otherwise
   */
  public static boolean addToTicketsList(JiraTicket tic, String timeOfFirstInactivePeriod,
      String ticketID) {
    //Record-keeping for the system
    if (!ticketID.isEmpty() || ticketID != null) {
      SysCreatedTickets ticket = new SysCreatedTickets(ticketID, timeOfFirstInactivePeriod,
          tic.getTicketProject(), tic.getTicketType(), tic.getTicketSummary(),
          tic.getTicketNodeID());
      createdTickets.add(ticket);
      return true;
    }
    return false;
  }

  /**
   * Method to change the status, customfield version, on the ticket
   *
   * @param issueKey String; Unique value of the ticket from JIRA
   * @param status String; Status value that the ticket will be changed to
   * @return Boolean; True if the status was successfully changed
   */
  static boolean setStatus(String issueKey, String status) {
    RestClient rest = client.getRestClient();
    try {
      URI editStatusUri = rest.buildURI(Issue.getBaseUri() + "issue/" + issueKey);

      JSONObject statusPayload = new JSONObject();
      statusPayload.put(ValueType.VALUE, status);
      JSONObject fieldsPayload = new JSONObject();
      fieldsPayload.put("customfield_10100", statusPayload);
      JSONObject updatePayload = new JSONObject();
      updatePayload.put("fields", fieldsPayload);
      rest.put(editStatusUri, updatePayload);

      return true;
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (RestException e) {
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Method check the HTTP Status of the SmartVineyards(SV) JIRA
   *
   * @param requestUrl The full url, including protocol, of SV's Jira installation
   * @return Boolean; True if 200, else False
   */
  private static boolean sendHttpRequest(String requestUrl) {
    try {
      URL url = new URL(requestUrl);

      // Create Http connection
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      // Get Http response code
      int responseCode = connection.getResponseCode();

      if (responseCode != 200) {
        // No such page exists
        return false;
      }

      return true;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }
}