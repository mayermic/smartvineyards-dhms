package com.nemo.dhms.issues;

/**
 * JiraTicket Class: Purpose of this class is to create a object to store all the relevant data from
 * the System for creating a JIRA Ticket. This class is not to be used outside of the context of
 * the JiraController Class
 */
class JiraTicket {

  private String ticketID; //JIRA Specific, not required for creation, just pull from JIRA
  private String ticketProject; //REQUIRED
  private String ticketType; //REQUIRED
  private String ticketSummary; //REQUIRED
  private String ticketAssignee;
  private String ticketReporter;
  private String ticketPriority;
  //private String ticketResolution;
  private String ticketStatus;
  private String ticketLabels;
  private String ticketDescription;
  private String ticketComponents;
  //Project Type SVD -- Labels
  private String ticketNodeID;
  private String ticketNodePIN;
  //private String ticketUniqueNodeID; //This is "TBD"
  private String ticketDataStreamURL;

  public JiraTicket() {
  }

  /**
   * Constructor w/ Attributes passed in to this object
   *
   * @param ticketID String; Ticket number in the JIRA system
   * @param ticketProject String; Project that this ticket is associated with
   * @param ticketSummary String; Title seen in ticket, aka Ticket summary during ticket creation
   * @param ticketAssignee String; User that the ticket is assigned to
   * @param ticketReporter String; User that created the ticket
   * @param ticketType String; Type of the ticket, for this project, usually "Hardware ticket"
   * @param ticketPriority String Array; Priority of this ticket, limited by what it is set to in
   * the Jira system
   * @param ticketLabels String; Label of the ticket, observed values "Node" or "Mesh"
   * @param ticketStatus String Array; Status of the ticket, depends on the workflow given by Smart
   * Vineyards; Dependent on the project type
   * @param ticketDescription String; Description of the ticket; observed long string
   */
  public JiraTicket(String ticketID, String ticketProject, String ticketSummary,
      String ticketAssignee, String ticketReporter, String ticketComponent,
      String ticketType, String ticketPriority, String ticketLabels, String ticketStatus,
      String ticketDescription) {
    setTicketID(ticketID);
    setTicketProject(ticketProject);
    setTicketSummary(ticketSummary);
    setTicketAssignee(ticketAssignee);
    setTicketReporter(ticketReporter);
    setTicketType(ticketType);
    setTicketComponents(ticketComponent);
    setTicketPriority(ticketPriority);
    setTicketLabels(ticketLabels);
    setTicketStatus(ticketStatus);
    setTicketDescription(ticketDescription);
  }

  /**
   * Copy constructor of another JiraTicket Object
   *
   * @param other Another JiraTicket Object
   */
  public JiraTicket(JiraTicket other) {
    //setTicketID(other.getTicketID());
    this.ticketID = other.getTicketID();
    this.ticketProject = other.getTicketProject();
    this.ticketSummary = other.getTicketSummary();
    this.ticketAssignee = other.getTicketAssignee();
    this.ticketReporter = other.getTicketReporter();
    this.ticketType = other.getTicketType();
    this.ticketComponents = other.getTicketComponents();
    this.ticketPriority = other.getTicketPriority();
    this.ticketLabels = other.getTicketLabels();
    this.ticketStatus = other.getTicketStatus();
    this.ticketDescription = other.getTicketDescription();
  }

  public String getTicketID() {
    return ticketID;
  }

  public void setTicketID(String ticketID) {
    this.ticketID = ticketID;
  }

  public String getTicketProject() {
    return ticketProject;
  }

  public void setTicketProject(String ticketProject) {
    this.ticketProject = ticketProject;
  }

  public String getTicketType() {
    return ticketType;
  }

  public void setTicketType(String ticketType) {
    this.ticketType = ticketType;
  }

  public String getTicketSummary() {
    return ticketSummary;
  }

  public void setTicketSummary(String ticketSummary) {
    this.ticketSummary = ticketSummary;
  }

  public String getTicketAssignee() {
    return ticketAssignee;
  }

  public void setTicketAssignee(String ticketAssignee) {
    this.ticketAssignee = ticketAssignee;
  }

  public String getTicketReporter() {
    return ticketReporter;
  }

  public void setTicketReporter(String ticketReporter) {
    this.ticketReporter = ticketReporter;
  }

  public String getTicketPriority() {
    return ticketPriority;
  }

  public void setTicketPriority(String ticketPriority) {
    this.ticketPriority = ticketPriority;
  }

  public String getTicketLabels() {
    return ticketLabels;
  }

  public void setTicketLabels(String ticketLabels) {
    this.ticketLabels = ticketLabels;
  }

  public String getTicketStatus() {
    return ticketStatus;
  }

  public void setTicketStatus(String ticketStatus) {
    this.ticketStatus = ticketStatus;
  }

  public String getTicketDescription() {
    return ticketDescription;
  }

  public void setTicketDescription(String ticketDescription) {
    this.ticketDescription = ticketDescription;
  }

  public String getTicketNodeID() {
    return ticketNodeID;
  }

  public void setTicketNodeID(String ticketNodeID) {
    this.ticketNodeID = ticketNodeID;
  }

  public String getTicketNodePIN() {
    return ticketNodePIN;
  }

  public void setTicketNodePIN(String ticketNodePIN) {
    this.ticketNodePIN = ticketNodePIN;
  }

  public String getTicketDataStreamURL() {
    return ticketDataStreamURL;
  }

  public void setTicketDataStreamURL(String ticketDataStreamURL) {
    this.ticketDataStreamURL = ticketDataStreamURL;
  }

  public String getTicketComponents() {
    return ticketComponents;
  }

  public void setTicketComponents(String ticketComponents) {
    this.ticketComponents = ticketComponents;
  }

}
