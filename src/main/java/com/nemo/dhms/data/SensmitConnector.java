/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.data;

import com.nemo.dhms.system.Location;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class queries the Sensmit API for Sensmit and Mesh data. It uses a JSON parser to parse the
 * Sensmit API data and retrieve the requested data back to the caller.
 */
public class SensmitConnector {

  private static Logger log = LoggerFactory.getLogger(SensmitConnector.class);

  //Custom time formatter to parse Sensmit API data history
  private static DateTimeFormatter historyFormatter = DateTimeFormatter
      .ofPattern("dd MMM uuuu HH:mm:ssXXX");

  private static String sensmitApiKey;
  private static String httpHeader;

  /**
   * Sets the Sensmit API key
   *
   * @param apiKey Sensmit API key
   */
  public static void setSensmitApiKey(String apiKey) {
    sensmitApiKey = apiKey;
    httpHeader = "ApiKey " + sensmitApiKey;
  }

  /**
   * Returns all Sensmit Mesh networks queried from the Sensmit API. Will return an empty Collection
   * if an error occurs or if there are no Meshes returned.
   *
   * @return Collection of Sensmit Meshes from the Sensmit API
   * @see SensmitMesh
   */
  static Collection<SensmitMesh> getAllMeshes() {
    String requestUrl = "http://www.sensmitweb.com/api/v1/meshes/";

    Collection<SensmitMesh> sensmitMeshes = new ArrayList<>();

    String response = sendHttpRequest(requestUrl);
    JSONObject obj = new JSONObject(response);

    JSONArray meshObjects = obj.getJSONArray("objects");

    for (int i = 0; i < meshObjects.length(); i++) {
      JSONObject meshJson = meshObjects.getJSONObject(i);

      SensmitMesh sensmitMesh = new SensmitMesh();

      int id = meshJson.getInt("id");
      sensmitMesh.setId(id);

      String meshId = meshJson.getString("mesh_id");
      sensmitMesh.setMeshId(meshId);

      String alias = meshJson.getString("alias");
      sensmitMesh.setAlias(alias);

      String activatedOn = getJsonString(meshJson, "activated_on");
      if (activatedOn != null) {
        sensmitMesh.setActivatedOn(
            LocalDateTime.parse(activatedOn, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
      }

      String lastDataTime = getJsonString(meshJson, "last_data_time");
      if (lastDataTime != null) {
        sensmitMesh.setLastDataTime(
            LocalDateTime.parse(lastDataTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
      }

      String statusCode = getJsonString(meshJson, "status_code");
      sensmitMesh.setStatusCode(statusCode);

      sensmitMeshes.add(sensmitMesh);
    }

    return sensmitMeshes;
  }

  /**
   * Returns the SensmitMesh object with the given Sensmit database ID.
   *
   * @param databaseId the Sensmit database id of the SensmitMesh
   * @return the SensmitMesh object from the Sensmit API or null if none exists
   * @see SensmitMesh
   */
  static SensmitMesh getMesh(int databaseId) {
    String requestUrl = "http://www.sensmitweb.com/api/v1/meshes/" + databaseId + "/";

    SensmitMesh sensmitMesh = new SensmitMesh();

    String response = sendHttpRequest(requestUrl);
    JSONObject meshJson = new JSONObject(response);

    int id = meshJson.getInt("id");
    sensmitMesh.setId(id);

    String meshId = meshJson.getString("mesh_id");
    sensmitMesh.setMeshId(meshId);

    String alias = meshJson.getString("alias");
    sensmitMesh.setAlias(alias);

    String activatedOn = getJsonString(meshJson, "activated_on");
    if (activatedOn != null) {
      sensmitMesh.setActivatedOn(
          LocalDateTime.parse(activatedOn, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }

    String lastDataTime = getJsonString(meshJson, "last_data_time");
    if (lastDataTime != null) {
      sensmitMesh.setLastDataTime(
          LocalDateTime.parse(lastDataTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }

    String statusCode = meshJson.getString("status_code");
    sensmitMesh.setStatusCode(statusCode);

    return sensmitMesh;
  }

  /**
   * Returns all of the Sensmits queried from the Sensmit API. Will return an empty Collection if an
   * error occurs or if there are no Sensmits returned.
   *
   * @return Collection of Sensmits from the Sensmit API
   * @see Sensmit
   */
  static Collection<Sensmit> getAllSensmits() {
    String requestUrl = "http://www.sensmitweb.com/api/v1/sensmits/";

    return getSensmitsByRequestUrl(requestUrl);
  }

  /**
   * Returns all of the Sensmits queried from the Sensmit API from a specified Mesh network. Will
   * return an empty Collection if an error occurs or if there are no Sensmits returned.
   *
   * @return Collection of Mesh Sensmits from the Sensmit API
   * @see Sensmit
   */
  static Collection<Sensmit> getAllSensmits(int meshDatabaseId) {
    String requestUrl = "http://www.sensmitweb.com/api/v1/sensmits/?mesh_id=" + meshDatabaseId;

    return getSensmitsByRequestUrl(requestUrl);
  }

  /**
   * Returns a Sensmit object from the Sensmit API specified by the given Sensmit database ID.
   *
   * @param databaseId Sensmit database id
   * @return Sensmit object or null if it does not exist
   * @see Sensmit
   */
  static Sensmit getSensmit(int databaseId) {
    String requestUrl = "http://www.sensmitweb.com/api/v1/sensmits/" + databaseId + "/";

    Sensmit sensmit = new Sensmit();

    String response = sendHttpRequest(requestUrl);
    JSONObject sensmitJson = new JSONObject(response);

    int id = sensmitJson.getInt("id");
    sensmit.setId(id);

    String sensmitId = sensmitJson.getString("sensmit_id");
    sensmit.setSensmitId(sensmitId);

    String alias = sensmitJson.getString("alias");
    sensmit.setAlias(alias);

    boolean isHidden = sensmitJson.getBoolean("is_hidden");
    sensmit.setHidden(isHidden);

    JSONObject locationJson = sensmitJson.getJSONObject("location");
    float latitude = (float) locationJson.getDouble("latitude");
    float longitude = (float) locationJson.getDouble("longitude");
    Location location = new Location(latitude, longitude);
    sensmit.setLocation(location);

    String lastCaptureTime = getJsonString(sensmitJson, "last_capture_time");
    if (lastCaptureTime != null) {
      sensmit.setLastCaptureTime(
          LocalDateTime.parse(lastCaptureTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }

    String status = sensmitJson.getString("status_code");
    sensmit.setStatus(status);

    return sensmit;
  }

  /**
   * Wrapper function which calls getSensmitData(..) to retrieve the complete history of a Sensmit
   * device.
   *
   * @param databaseId Sensmit database ID
   * @return list of historical data of a Sensmit device
   */
  static List<SensmitData> getSensmitDataHistory(int databaseId) {
    List<SensmitData> sensmitDataHistoryList = new ArrayList<>();

    LocalDate endTime = LocalDate.now();
    LocalDate startTime = endTime.minusDays(30);

    List<SensmitData> aList = getSensmitData(databaseId, startTime, endTime);
    while (!aList.isEmpty()) {
      sensmitDataHistoryList.addAll(aList);
      endTime = startTime;
      startTime = endTime.minusDays(30);
      aList = getSensmitData(databaseId, startTime, endTime);
    }
    return sensmitDataHistoryList;
  }

  /**
   * Wrapper function which calls getData(..) to retrieve a list of Sensmit data up to 30 days from
   * Sensmit API.
   *
   * @param databaseId Sensmit database ID
   * @param startTime start time of the range of data (exclusive)
   * @param endTime end time of the range of data (inclusive)
   * @return list of 30 days of historical data of a Sensmit device
   */
  private static List<SensmitData> getSensmitData(int databaseId,
      LocalDate startTime, LocalDate endTime) {

    int maxDay = 30;
    if (endTime.isAfter(startTime.plusDays(maxDay))) {
      log.error("Can't retrieve more than 30 days of data.");
      return Collections.emptyList();
    }

    String requestUrl = "http://www.sensmitweb.com/api/v1/sensmits/" + databaseId +
        "/" + "data/?start_time=" + startTime + "&end_time=" + endTime;

    return getData(requestUrl);
  }

  /**
   * Wrapper function which calls getMeshData(..) to retrieve the complete history of a Sensmit
   * Mesh
   *
   * @param meshId Sensmit Mesh ID
   * @return list of historical data of a Sensmit Mesh
   */
  static List<SensmitData> getMeshDataHistory(int meshId) {
    List<SensmitData> meshDataHistoryList = new ArrayList<>();

    // Unlike SensmitData the MeshData can only be retrieved at 15 day increments
    LocalDate endTime = LocalDate.now();
    LocalDate startTime = endTime.minusDays(15);

    List<SensmitData> aList = getMeshData(meshId, startTime, endTime);
    while (!aList.isEmpty()) {
      meshDataHistoryList.addAll(aList);
      endTime = startTime;
      startTime = endTime.minusDays(15);
      aList = getMeshData(meshId, startTime, endTime);
    }
    return meshDataHistoryList;
  }

  /**
   * Wrapper function to call getData(..) to retrieve a list of Sensmit Mesh data up to 30 days from
   * Sensmit API.
   *
   * @param databaseId Sensmit ID
   * @param startTime start time of the range of data (exclusive)
   * @param endTime end time of the range of data (inclusive)
   * @return list of 15 days of historical data of a Sensmit Mesh
   */
  private static List<SensmitData> getMeshData(int databaseId,
      LocalDate startTime, LocalDate endTime) {

    // Unlike SensmitData the MeshData can only be retrieved at 15 day increments
    int maxDay = 15;
    if (endTime.isAfter(startTime.plusDays(maxDay))) {
      log.error("Can't retrieve more than 15 days of data.");
      return Collections.emptyList();
    }

    String requestUrl = "http://www.sensmitweb.com/api/v1/meshes/" + databaseId +
        "/" + "data/?start_time=" + startTime + "&end_time=" + endTime;

    return getData(requestUrl);
  }

  /**
   * Connects to the Sensmit API to retrieve data about a device.
   *
   * @param requestUrl Sensmit API url
   * @return list of Sensmit data
   */
  private static List<SensmitData> getData(String requestUrl) {
    List<SensmitData> DataList = new ArrayList<>();

    String nextUrl = requestUrl;

    // Loop if there is a next page
    while (nextUrl != null) {
      String response = sendHttpRequest(requestUrl);
      JSONObject obj = new JSONObject(response);
      JSONObject meta = obj.getJSONObject("meta");

      if (meta.isNull("next")) {
        nextUrl = null;
      } else {
        nextUrl = meta.getString("next");
      }

      // Parse JSON
      //JSONObject obj = new JSONObject(response.toString());
      JSONArray sensmitObjects = obj.getJSONArray("objects");

      for (int i = 0; i < sensmitObjects.length(); i++) {
        JSONObject sensmitJson = sensmitObjects.getJSONObject(i);

        SensmitData sensmitData = new SensmitData();
        Map<String, String> sensorValues = new HashMap<>();

        int id = sensmitJson.getInt("id");
        sensmitData.setId(id);

        String newSensmitId = sensmitJson.getString("sensmit_id");
        sensmitData.setSensmitId(newSensmitId);

        String timestamp = sensmitJson.getString("timestamp");
        if (timestamp != null) {
          sensmitData.setTimestamp(
              LocalDateTime.parse(timestamp, historyFormatter));
        }

        if (sensmitJson.has("MO1")) {
          String mo1 = sensmitJson.getString("MO1");
          sensorValues.put("MO1", mo1);
        } else {
          sensorValues.put("MO1", "No Data");
        }

        if (sensmitJson.has("MO2")) {
          String mo2 = sensmitJson.getString("MO2");
          sensorValues.put("MO2", mo2);
        } else {
          sensorValues.put("MO2", "No Data");
        }

        if (sensmitJson.has("MO3")) {
          String mo3 = sensmitJson.getString("MO3");
          sensorValues.put("MO3", mo3);
        } else {
          sensorValues.put("MO3", "No Data");
        }

        if (sensmitJson.has("INT")) {
          String _int = sensmitJson.getString("INT");
          sensorValues.put("INT", _int);
        } else {
          sensorValues.put("INT", "No Data");
        }

        if (sensmitJson.has("EXT")) {
          String ext = sensmitJson.getString("EXT");
          sensorValues.put("EXT", ext);
        } else {
          sensorValues.put("EXT", "No Data");
        }

        if (sensmitJson.has("POW")) {
          String pow = sensmitJson.getString("POW");
          sensorValues.put("POW", pow);
        } else {
          sensorValues.put("POW", "No Data");
        }

        if (sensmitJson.has("CAP")) {
          String cap = sensmitJson.getString("CAP");
          sensorValues.put("CAP", cap);
        } else {
          sensorValues.put("CAP", "No Data");
        }

        if (sensmitJson.has("TIP")) {
          String tip = sensmitJson.getString("TIP");
          sensorValues.put("TIP", tip);
        } else {
          sensorValues.put("TIP", "No Data");
        }

        if (sensmitJson.has("IRR")) {
          String irr = sensmitJson.getString("IRR");
          sensorValues.put("IRR", irr);
        } else {
          sensorValues.put("IRR", "No Data");
        }

        if (sensmitJson.has("length")) {
          String length = sensmitJson.getString("length");
          sensorValues.put("length", length);
        } else {
          sensorValues.put("length", "No Data");
        }
        sensmitData.setSensorValues(sensorValues);

        DataList.add(sensmitData);
      }
    }

    return DataList;
  }

  /**
   * Checks if a JSON Object is null.
   *
   * @param jsonObject JSON Object
   * @param key JSON Object key
   * @return JSON object if not null, else return null
   */
  private static String getJsonString(JSONObject jsonObject, String key) {
    Object o = jsonObject.get(key);
    if (o.toString().equals("null")) {
      return null;
    } else {
      return jsonObject.getString(key);
    }
  }

  private static String sendHttpRequest(String requestUrl) {
    try {
      URL url = new URL(requestUrl);

      // Create Http connection
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Authorization", httpHeader);

      // Get Http response code
      int responseCode = connection.getResponseCode();
      //log.info("Sending 'GET' request to URL : " + url);
      //log.debug("Response Code : " + responseCode);

      if (responseCode != 200) {
        // No such page exists
        log.error("Got response code: " + responseCode);
        return null;
      }

      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      String inputLine;
      StringBuilder response = new StringBuilder();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      return response.toString();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  private static Collection<Sensmit> getSensmitsByRequestUrl(String requestUrl) {
    Collection<Sensmit> sensmits = new ArrayList<>();

    String response = sendHttpRequest(requestUrl);
    JSONObject obj = new JSONObject(response);
    JSONArray sensmitObjects = obj.getJSONArray("objects");

    for (int i = 0; i < sensmitObjects.length(); i++) {
      JSONObject sensmitJson = sensmitObjects.getJSONObject(i);

      Sensmit sensmit = new Sensmit();

      int id = sensmitJson.getInt("id");
      sensmit.setId(id);

      String sensmitId = sensmitJson.getString("sensmit_id");
      sensmit.setSensmitId(sensmitId);

      String alias = sensmitJson.getString("alias");
      sensmit.setAlias(alias);

      boolean isHidden = sensmitJson.getBoolean("is_hidden");
      sensmit.setHidden(isHidden);

      JSONObject locationJson = sensmitJson.getJSONObject("location");
      float latitude = (float) locationJson.getDouble("latitude");
      float longitude = (float) locationJson.getDouble("longitude");
      Location location = new Location(latitude, longitude);
      sensmit.setLocation(location);

      String lastCaptureTime = getJsonString(sensmitJson, "last_capture_time");
      if (lastCaptureTime != null) {
        sensmit.setLastCaptureTime(
            LocalDateTime.parse(lastCaptureTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
      }

      String statusCode = sensmitJson.getString("status_code");
      sensmit.setStatus(statusCode);

      sensmits.add(sensmit);
    }

    return sensmits;
  }
}
