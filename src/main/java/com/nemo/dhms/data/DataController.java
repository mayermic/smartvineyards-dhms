/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.data;

import com.nemo.dhms.system.DatabaseConnector;
import com.nemo.dhms.system.Device;
import com.nemo.dhms.system.DeviceData;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import com.nemo.dhms.system.Location;
import com.nemo.dhms.system.Mesh;
import com.nemo.dhms.system.MeshHistoryEntry;
import com.nemo.dhms.system.MeshStatus;
import com.nemo.dhms.system.SystemController;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DataController is a facade class that is used by the SystemController to retrieve outside data.
 * This class uses the SensmitConnector to retrieve Sensmit and Mesh data from Sensmit web.
 */
@Service
public class DataController {

  private static final Logger log = LoggerFactory.getLogger(DataController.class);
  private static long refreshTime = 30;
  private static Collection<SensmitMesh> sensmitMeshesStatic = null;
  private static Collection<Sensmit> sensmitsStatic = null;

  private static Map<String, Integer> sensmitDatabaseIdMap = new HashMap<>();
  private static Map<String, Integer> meshDatabaseIdMap = new HashMap<>();
  private static Map<String, String> sensmitMeshMap = new HashMap<>();

  private static DatabaseConnector databaseConnector;

  private static boolean initialized = false;

  private DataController() {

  }

  public static void init() {
    if (initialized) {
      return;
    }

    sensmitMeshesStatic = SensmitConnector.getAllMeshes();
    sensmitsStatic = SensmitConnector.getAllSensmits();

    Collection<SensmitMesh> meshes = getAllSensmitMeshes();
    Collection<Sensmit> sensmits = getAllSensmits();
    //log.info("{}: Entered {} method {} ", LocalDateTime.now(), "DataController", "init()");
    //log.info("{}: DataController Mesh Construction started", LocalDateTime.now());
    for (SensmitMesh mesh : meshes) {
      meshDatabaseIdMap.put(mesh.getMeshId(), mesh.getId());
      //log.debug("{}: mesh.getMeshID: {}, mesh.getID: {}", LocalDateTime.now(), mesh.getMeshId(), mesh.getId());

      // Build map of Sensmit Digital IDs to Mesh IDs
      Collection<Sensmit> sensmitMeshes = SensmitConnector.getAllSensmits(mesh.getId());
      for (Sensmit sensmit : sensmitMeshes) {
        sensmitMeshMap.put(sensmit.getSensmitId(), mesh.getMeshId());
      }
    }
    //log.info("{}: DataController Mesh Construction finished", LocalDateTime.now());

    //log.info("{}: DataController Sensmit ID Construction started", LocalDateTime.now());
    for (Sensmit sensmit : sensmits) {
      sensmitDatabaseIdMap.put(sensmit.getSensmitId(), sensmit.getId());
      //log.debug("{}:, Action: PUT, Object: sensmit.getSensmitID: {}, sensmit.getID: {}", LocalDateTime.now(), sensmit.getSensmitId(), sensmit.getId());
    }
    //log.info("{}: DataController Sensmit ID Construction finished", LocalDateTime.now());

    if (databaseConnector == null) {
      log.warn("Database connector is not wired yet.");
    } else {
      //log.debug("Database connector is wired.");
    }
  }

  /**
   * This method retrieves all of the Mesh networks and extracts the digital IDs from them. <p>
   * Returns all of the digital IDs for the Mesh networks.
   *
   * @return Collection of Strings equal to the digital IDs for all Mesh networks
   */
  public static Collection<String> getMeshIds() {
    Collection<String> meshIds = new ArrayList<>();
    //log.debug("{}: Entered {} method {} ()", LocalDateTime.now(), "DataController", "getMeshIDs()");
    Collection<SensmitMesh> sensmitMeshes = getAllSensmitMeshes();

    //log.info("{}: DataController Sensmit Mesh ID Construction started", LocalDateTime.now());
    for (SensmitMesh sensmitMesh : sensmitMeshes) {
      String temp = sensmitMesh.getMeshId();
      meshIds.add(temp);
      //log.debug("{}:, Action: ADD TO SYSTEM, Object: sensmit.getMeshID: {}", LocalDateTime.now(), temp);
    }
    //log.info("{}: DataController Sensmit Mesh ID Construction finished", LocalDateTime.now());

    return meshIds;
  }

  /**
   * Get the digital ID of the Mesh network that the specified Device digital ID is part of.
   *
   * @param deviceId the Device digital ID
   * @return the Mesh network digital ID that the Device is part of
   */
  public static String getMeshId(String deviceId) {
    return sensmitMeshMap.get(deviceId);
  }

  /**
   * This method gets a list of all the Sensmits then it creates a list of Sensmit IDs from it. <p>
   * Returns a list of all the digital IDs for the Sensmits.
   *
   * @return Collection of Strings equal to the digital IDs for all Sensmits
   */
  public static Collection<String> getDeviceIds() {
    Collection<String> deviceIds = new ArrayList<>();
    Collection<Sensmit> sensmits = SensmitConnector.getAllSensmits(); //TODO: remove
    for (Sensmit sensmit : sensmits) {
      deviceIds.add(sensmit.getSensmitId());
      //log.debug("{}:, Action: ADD TO SYSTEM, Object: sensmit.getSensmitID: {}", LocalDateTime.now(), sensmit.getSensmitId());
    }
    //log.info("{}: DataController Sensmit Device ID Construction finished", LocalDateTime.now());

    return deviceIds;
  }

  /**
   * This method retrieves all of the Mesh networks and creates Mesh objects with the values from
   * the Mesh networks. <p> Returns Mesh objects for all of the Mesh networks
   *
   * @return Collection of Mesh objects with data from all Sensmit Meshes
   */
  public static Collection<Mesh> getAllMeshes() {
    //log.debug("{}: Entered DataController getAllMeshes()", LocalDateTime.now());
    Collection<Mesh> meshes = new ArrayList<>();
    Collection<SensmitMesh> sensmitMeshes = getAllSensmitMeshes();
    //log.info("{}: DataController Sensmit Meshes Construction started", LocalDateTime.now());
    for (SensmitMesh sensmitMesh : sensmitMeshes) {
      Location meshLocation;

      Device meshBase = getDevice(sensmitMesh.getMeshId());
      if (meshBase != null) {
        meshLocation = meshBase.getLocation();
      } else {
        meshLocation = new Location(0, 0);  //Sensmit web defaults location to 0,0 if not available.
      }

      Mesh mesh = new Mesh(
          sensmitMesh.getMeshId(),
          sensmitMesh.getLastDataTime(),
          sensmitMesh.getActivatedOn(),
          MeshStatus.parseString(sensmitMesh.getStatusCode())
      );

      mesh.setLocation(meshLocation);

      meshes.add(mesh);
      //log.debug("{}:, ACTION: ADD TO MESHES, MeshID: {}", LocalDateTime.now(), mesh.getMeshId());

      // Save mesh status to database
      if (databaseConnector != null && sensmitMesh.getLastDataTime() != null) {
        MeshHistoryEntry databaseEntry = new MeshHistoryEntry();
        databaseEntry.setMesh(mesh.getMeshId());
        databaseEntry.setDate(mesh.getLastDataTime());
        databaseEntry.setStatus(mesh.getStatus());
        databaseConnector.putMeshStatus(databaseEntry);
      }
    }
    //log.info("{}: DataController Sensmit Meshes Construction finished", LocalDateTime.now());
    return meshes;
  }

  /**
   * This method gets a specified Mesh network then creates a mesh object. <p> Returns the Mesh
   * object with the digital ID equal to the given meshId.
   *
   * @param meshId the Digital ID of the Mesh network
   * @return the Mesh object with the given digital ID from the Sensmit API
   */
  public static Mesh getMesh(String meshId) {
    //log.debug("Entered getMesh() with parameter: {}", meshId);
    SensmitMesh sensmitMesh = SensmitConnector.getMesh(meshDatabaseIdMap.get(meshId));

    //log.info("{}: DataController Sensmit Mesh Construction started", LocalDateTime.now());
    if (sensmitMesh != null) {
      Location meshLocation;

      Device meshBase = getDevice(sensmitMesh.getMeshId());
      if (meshBase != null) {
        meshLocation = meshBase.getLocation();
      } else {
        meshLocation = new Location(0, 0);  //Sensmit web defaults location to 0,0 if not available.
      }

      Mesh mesh = new Mesh(
          sensmitMesh.getMeshId(),
          sensmitMesh.getLastDataTime(),
          sensmitMesh.getActivatedOn(),
          MeshStatus.parseString(sensmitMesh.getStatusCode())
      );

      mesh.setLocation(meshLocation);
      //log.info("{}: DataController Sensmit Mesh Construction finished", LocalDateTime.now());

      // Save mesh status to database
      if (databaseConnector != null && sensmitMesh.getLastDataTime() != null) {
        MeshHistoryEntry databaseEntry = new MeshHistoryEntry();
        databaseEntry.setMesh(meshId);
        databaseEntry.setDate(mesh.getLastDataTime());
        databaseEntry.setStatus(mesh.getStatus());
        databaseConnector.putMeshStatus(databaseEntry);
      }

      return mesh;
    } else {
      //log.info("{}: DataController Sensmit Mesh Construction returned null", LocalDateTime.now());
      return null;
    }
  }

  /**
   * Returns a list of historical statuses for a mesh.
   *
   * @param meshId mesh ID
   * @return list of mesh status history
   */
  public static List<MeshHistoryEntry> getMeshStatus(String meshId) {
    // Make sure most recent status is cached.
    Mesh mesh = getMesh(meshId);

    return databaseConnector.getMeshStatus(meshId);
  }

  public static boolean putMeshStatus(MeshHistoryEntry entry) {
    if (databaseConnector == null) {
      return false;
    }

    return databaseConnector.putMeshStatus(entry);
  }

  /**
   * Get data from all Devices in the specified Mesh network
   *
   * @param meshId mesh ID
   * @return List of DeviceData from a specified Mesh network
   */
  public static List<DeviceData> getMeshData(String meshId) {
    List<DeviceData> deviceData = new ArrayList<>();

    int databaseId = meshDatabaseIdMap.get(meshId);

    List<SensmitData> meshData = SensmitConnector.getMeshDataHistory(databaseId);
    for (SensmitData meshDatum : meshData) {
      LocalDateTime timestamp = meshDatum.getTimestamp();

      Map<String, String> sensorValues = new HashMap<>();
      for (Map.Entry<String, String> sensorPair : meshDatum.getSensorValues().entrySet()) {
        String sensorType = sensorPair.getKey();
        String sensorValue = sensorPair.getValue();
        sensorValues.put(sensorType, sensorValue);
      }

      deviceData.add(new DeviceData(meshId, timestamp, sensorValues));
    }

    return Collections.unmodifiableList(deviceData);
  }

  /**
   * Returns all devices part of the specified Mesh network
   *
   * @param meshId the Digital ID of a Mesh network
   * @return Collection of devices from the specifed Mesh network
   */
  public static Collection<Device> getMeshDevices(String meshId) {
    //log.debug("{}: Entered getMeshDevices()", LocalDateTime.now());
    int databaseId = meshDatabaseIdMap.get(meshId);
    Collection<Device> devices = new ArrayList<>();
    Collection<Sensmit> sensmits = SensmitConnector.getAllSensmits(databaseId);
    for (Sensmit sensmit : sensmits) {
      Device device = new Device(
          sensmit.getSensmitId(),
          "",
          getMeshId(sensmit.getSensmitId()),
          sensmit.getLastCaptureTime(),
          DeviceStatus.parseString(sensmit.getStatus()),
          sensmit.getLocation()
      );

      devices.add(device);
    }

    return devices;
  }

  /**
   * This method retrieves all the Sensmits and creates Devices objects from them. <p> Returns
   * Device objects for all of the Sensmits
   *
   * @return List of Device objects with data from all Sensmits
   */
  public static Collection<Device> getAllDevices() {
    //log.debug("{}: Entered getAllDevices()", LocalDateTime.now());
    Collection<Device> devices = new ArrayList<>();
    Collection<Sensmit> sensmits = getAllSensmits();
    //log.info("{}: DataController Sensmit All Devices Construction started", LocalDateTime.now());
    for (Sensmit sensmit : sensmits) {
      Device device = new Device(
          sensmit.getSensmitId(),
          "",
          getMeshId(sensmit.getSensmitId()),
          sensmit.getLastCaptureTime(),
          DeviceStatus.parseString(sensmit.getStatus()),
          sensmit.getLocation()
      );

      devices.add(device);
      //log.debug("{}:, ACTION: ADD TO DEVICES, OBJECT: deviceID: {}", LocalDateTime.now(), device.getDigitalId());

      // Save device status to database
      if (databaseConnector != null && sensmit.getLastCaptureTime() != null) {
        DeviceHistoryEntry databaseEntry = new DeviceHistoryEntry();
        databaseEntry.setDevice(device.getDigitalId());
        databaseEntry.setDate(device.getLastCaptureTime());
        databaseEntry.setStatus(device.getStatus());
        databaseConnector.putDeviceStatus(databaseEntry);
      }
    }
    //log.info("{}: DataController Sensmit All Devices Construction finished", LocalDateTime.now());

    return devices;
  }

  /**
   * This method gets a specified Sensmit then creates a device object. <p> Returns the Device
   * object with the digital ID equal to the given deviceId.
   *
   * @param digitalId the Digital ID of the Device
   * @return the Device object with the given digital ID from the Sensmit API
   */
  public static Device getDevice(String digitalId) {
    //log.debug("{}: Entered getDevice with digitalID parameter: {}", LocalDateTime.now(), digitalId);
    Integer databaseId = sensmitDatabaseIdMap.get(digitalId);

    //log.info("{}: DataController Sensmit Device Construction started", LocalDateTime.now());
    if (databaseId != null) {
      Sensmit sensmit = SensmitConnector.getSensmit(databaseId);

      //log.info("{}: DataController Sensmit Device Construction finished", LocalDateTime.now());
      //log.info("{}: DataController Sensmit Device returned deviceID: {}", LocalDateTime.now(), sensmit.getSensmitId());
      return new Device(
          sensmit.getSensmitId(),
          "",
          getMeshId(sensmit.getSensmitId()),
          sensmit.getLastCaptureTime(),
          DeviceStatus.parseString(sensmit.getStatus()),
          sensmit.getLocation());
    } else {
      //log.info("{}: DataController Sensmit Device Construction return null", LocalDateTime.now());
      return null;
    }
  }

  /**
   * Returns a list of historical statuses for a device.
   *
   * @param deviceId String value representing current device digitalID
   * @return - DeviceStatus object for requested current Device, and throw exception if null.
   */
  public static List<DeviceHistoryEntry> getDeviceStatus(String deviceId) {
    // Make sure most recent status is cached.
    Device device = getDevice(deviceId);

    return databaseConnector.getDeviceStatus(deviceId);
  }

  public static boolean putDeviceStatus(DeviceHistoryEntry entry) {
    if (databaseConnector == null) {
      return false;
    }

    return databaseConnector.putDeviceStatus(entry);
  }

  /**
   * @param devId - String value representing current device digitalID
   * @return - DeviceStatus object for requested current Device, and throw exception if null.
   */
  public static DeviceStatus getCurrentDeviceStatus(String devId) {
    DeviceStatus devStatus = SystemController.getDeviceStatus(devId);

    if (devStatus == null) {
      throw new RuntimeException("device status null");
    }
    return devStatus;
  }

  /**
   * Returns a list of historical data for the device.
   *
   * @param digitalId the digital ID of the device
   * @return list of historical data for the device
   */
  public static List<DeviceData> getDeviceData(String digitalId) {
    //log.debug("{}: Entered getDeviceData() with parameter digitalId: {}", LocalDateTime.now(), digitalId);
    List<DeviceData> deviceData = new ArrayList<>();

    int databaseId = sensmitDatabaseIdMap.get(digitalId);

    List<SensmitData> sensmitData = SensmitConnector.getSensmitDataHistory(databaseId);
    //log.info("{}: DataController Device Data retrieval started", LocalDateTime.now());
    for (SensmitData sensmitDatum : sensmitData) {
      LocalDateTime timestamp = sensmitDatum.getTimestamp();

      Map<String, String> sensorValues = new HashMap<>();
      for (Map.Entry<String, String> sensorPair : sensmitDatum.getSensorValues().entrySet()) {
        String sensorType = sensorPair.getKey();
        String sensorValue = sensorPair.getValue();
        sensorValues.put(sensorType, sensorValue);
      }

      deviceData.add(new DeviceData(digitalId, timestamp, sensorValues));
      //log.debug("{}:, ACTION: ADD TO SYSTEM, OBJECT: DeviceData ID: {}", LocalDateTime.now(), sensmitDatum.getSensmitId());
    }
    //log.info("{}: DataController Device Data retrieval finished", LocalDateTime.now());

    return Collections.unmodifiableList(deviceData);
  }


  /**
   * Returns list of sensmits api
   */
  public static Collection<Sensmit> getAllSensmits() {
    //log.debug("{}: Entered getAllSensmits()", LocalDateTime.now());
    if (sensmitsStatic != null && !sensmitsStatic.isEmpty() && sensmitsStatic.iterator().next()
        .getLastCaptureTime()
        .plusMinutes(refreshTime).isBefore(LocalDateTime.now())) {
      return sensmitsStatic;
    } else {
      sensmitsStatic = SensmitConnector.getAllSensmits();
      return sensmitsStatic;
    }
  }

  public static Collection<SensmitMesh> getAllSensmitMeshes() {
    //log.debug("{}: Entered getAllSensmitMeshes()", LocalDateTime.now());
    if (!sensmitMeshesStatic.isEmpty() && sensmitMeshesStatic.iterator().next().getLastDataTime()
        .plusMinutes(refreshTime).isBefore(LocalDateTime.now())) {
      return sensmitMeshesStatic;
    } else {
      sensmitMeshesStatic = SensmitConnector.getAllMeshes();
      return sensmitMeshesStatic;
    }
  }

  @Autowired(required = true)
  void setDatabaseConnector(DatabaseConnector databaseConnector) {
    DataController.databaseConnector = databaseConnector;
  }
}
