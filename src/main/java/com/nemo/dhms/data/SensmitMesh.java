/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.data;

import com.google.common.base.Objects;
import java.time.LocalDateTime;

/**
 * SensmitMesh class used with the Sensmit API. This class is not used outside of the context of the
 * SensmitConnector class.
 */
class SensmitMesh {

  private int id;
  private String meshId = null;
  private String alias = null;
  private LocalDateTime activatedOn = null;
  private LocalDateTime lastDataTime = null;
  private String statusCode = null;

  SensmitMesh() {
  }

  /**
   * Instantiate a SensmitMesh with given parameter values.
   *
   * @param id Identifies a mesh in a mesh network
   * @param meshId Hardware id of a mesh
   * @param alias User defined name of a mesh
   * @param activatedOn Date and time of when the mesh was activated
   * @param lastDataTime Date and time of when the last data was recorded
   * @param statusCode Status of the mesh
   */
  SensmitMesh(int id, String meshId, String alias, LocalDateTime activatedOn,
      LocalDateTime lastDataTime,
      String statusCode) {
    this.id = id;
    this.meshId = meshId;
    this.alias = alias;
    this.activatedOn = activatedOn;
    this.lastDataTime = lastDataTime;
    this.statusCode = statusCode;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMeshId() {
    return meshId;
  }

  public void setMeshId(String meshId) {
    this.meshId = meshId;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public LocalDateTime getActivatedOn() {
    return activatedOn;
  }

  public void setActivatedOn(LocalDateTime activatedOn) {
    this.activatedOn = activatedOn;
  }

  public LocalDateTime getLastDataTime() {
    return lastDataTime;
  }

  public void setLastDataTime(LocalDateTime lastDataTime) {
    this.lastDataTime = lastDataTime;
  }

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SensmitMesh that = (SensmitMesh) o;
    return id == that.id &&
        Objects.equal(meshId, that.meshId) &&
        Objects.equal(alias, that.alias) &&
        Objects.equal(activatedOn, that.activatedOn) &&
        Objects.equal(lastDataTime, that.lastDataTime) &&
        Objects.equal(statusCode, that.statusCode);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, meshId, alias, activatedOn, lastDataTime, statusCode);
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this)
        .add("id", id)
        .add("meshId", meshId)
        .add("alias", alias)
        .add("activatedOn", activatedOn)
        .add("lastDataTime", lastDataTime)
        .add("statusCode", statusCode)
        .toString();
  }
}
