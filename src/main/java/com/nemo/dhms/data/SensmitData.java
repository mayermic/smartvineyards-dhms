/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.data;

import com.google.common.base.Objects;
import java.time.LocalDateTime;
import java.util.Map;


/**
 * SensmitData class used with the Sensmit API. This class is not used outside of the context of the
 * SensmitConnector class.
 */
class SensmitData {

  private int id;
  private String sensmitId;
  private LocalDateTime timestamp;
  private Map<String, String> sensorValues;

  /**
   * Initailizes a new SensmitData.
   */
  public SensmitData() {

  }

  /**
   * Initailizes a new SensmitData with values passed in.
   *
   * @param id Sensmit database id.
   * @param sensmitId Id of the Sensmit device.
   * @param timestamp Time when the data was collected.
   * @param sensorValues The values of the sensor.
   */
  public SensmitData(int id, String sensmitId, LocalDateTime timestamp,
      Map<String, String> sensorValues) {
    this.id = id;
    this.sensmitId = sensmitId;
    this.timestamp = timestamp;
    this.sensorValues = sensorValues;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getSensmitId() {
    return sensmitId;
  }

  public void setSensmitId(String sensmitId) {
    this.sensmitId = sensmitId;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public Map<String, String> getSensorValues() {
    return sensorValues;
  }

  public void setSensorValues(Map<String, String> sensorValues) {
    this.sensorValues = sensorValues;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SensmitData that = (SensmitData) o;
    return id == that.id &&
        Objects.equal(sensmitId, that.sensmitId) &&
        Objects.equal(timestamp, that.timestamp) &&
        Objects.equal(sensorValues, that.sensorValues);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, sensmitId, timestamp, sensorValues);
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this)
        .add("id", id)
        .add("sensmitId", sensmitId)
        .add("timestamp", timestamp)
        .add("sensorValues", sensorValues)
        .toString();
  }
}

