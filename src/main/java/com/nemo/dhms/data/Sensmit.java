/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.data;

import com.google.common.base.Objects;
import com.nemo.dhms.system.Location;
import java.time.LocalDateTime;


/**
 * Sensmit class used with the Sensmit API. This class is not used outside of the context of the
 * SensmitConnector class.
 */
class Sensmit {

  private int id;
  private String sensmitId;
  private String alias;
  private boolean isHidden;
  private Location location;
  private LocalDateTime lastCaptureTime;
  private String status;

  Sensmit() {
  }

  /**
   * Instantiate Semsmit with given parameter values.
   *
   * @param id Identifies a Sensmit in a mesh network
   * @param sensmitId Hardware id of a Sensmit
   * @param alias User defined name of a mesh
   * @param isHidden If a Sensmit is hidden or not
   * @param location Latitude and Longitude coordinate of the Sensmit
   * @param lastCaptureTime Date and time of when the last data was recorded
   * @param status Status of the Sensmit
   */
  Sensmit(int id, String sensmitId, String alias, boolean isHidden,
      Location location, LocalDateTime lastCaptureTime, String status) {
    this.id = id;
    this.sensmitId = sensmitId;
    this.alias = alias;
    this.isHidden = isHidden;
    this.location = location;
    this.lastCaptureTime = lastCaptureTime;
    this.status = status;
  }

  int getId() {
    return id;
  }

  void setId(int id) {
    this.id = id;
  }

  String getSensmitId() {
    return sensmitId;
  }

  void setSensmitId(String sensmitId) {
    this.sensmitId = sensmitId;
  }

  String getAlias() {
    return alias;
  }

  void setAlias(String alias) {
    this.alias = alias;
  }

  boolean isHidden() {
    return isHidden;
  }

  void setHidden(boolean hidden) {
    isHidden = hidden;
  }

  Location getLocation() {
    return location;
  }

  void setLocation(Location location) {
    this.location = location;
  }

  LocalDateTime getLastCaptureTime() {
    return lastCaptureTime;
  }

  void setLastCaptureTime(LocalDateTime lastCaptureTime) {
    this.lastCaptureTime = lastCaptureTime;
  }

  String getStatus() {
    return status;
  }

  void setStatus(String status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Sensmit sensmit = (Sensmit) o;
    return id == sensmit.id &&
        isHidden == sensmit.isHidden &&
        Objects.equal(sensmitId, sensmit.sensmitId) &&
        Objects.equal(alias, sensmit.alias) &&
        Objects.equal(location, sensmit.location) &&
        Objects.equal(lastCaptureTime, sensmit.lastCaptureTime) &&
        Objects.equal(status, sensmit.status);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, sensmitId, alias, isHidden, location, lastCaptureTime, status);
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this)
        .add("id", id)
        .add("sensmitId", sensmitId)
        .add("alias", alias)
        .add("isHidden", isHidden)
        .add("location", location)
        .add("lastCaptureTime", lastCaptureTime)
        .add("status", status)
        .toString();
  }
}
