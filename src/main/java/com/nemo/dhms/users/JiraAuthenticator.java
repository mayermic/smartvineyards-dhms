package com.nemo.dhms.users;

import java.util.HashMap;
import java.util.Map;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.Resource;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.User;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * TODO: Write class Javadoc
 */
public class JiraAuthenticator {

  private static String jiraUrl;

  /**
   * Set the JIRA base URL for performing authentication requests.
   *
   * @param url JIRA base URL
   */
  public static void setBaseUrl(String url) {
    jiraUrl = url;
  }

  /**
   * Authenticate as a JIRA user with a specified username and password.
   *
   * @param username username
   * @param password password
   * @return the authenticated user on success and null on failure
   */
  public static JiraUser authenticate(String username, String password) {
    BasicCredentials credentials = new BasicCredentials(username, password);
    JiraClient jiraClient = null;
    try {
      jiraClient = new JiraClient(jiraUrl, credentials);
    } catch (JiraException e) {
      e.printStackTrace();
    }
    RestClient restClient = jiraClient.getRestClient();

    try {
      // Try to authenticate user with credentials
      User user = User.get(restClient, username);

      // Create user object
      JiraUser authenticatedUser = new JiraUser();
      authenticatedUser.setUsername(username);
      authenticatedUser.setName(user.getName());
      authenticatedUser.setDisplayName(user.getDisplayName());

      // Get user groups data
      JSON result = null;
      try {
        Map<String, String> params = new HashMap<>();
        params.put("expand", "groups");
        result = restClient.get(Resource.getBaseUri() + "myself", params);
      } catch (Exception ex) {
        throw new JiraException("Failed to authenticate as user " + username, ex);
      }

      if (!(result instanceof JSONObject)) {
        throw new JiraException("JSON payload is malformed");
      }

      // Parse user groups data
      JSONObject data = (JSONObject) result;
      JSONObject groups = data.getJSONObject("groups");
      JSONArray groupItems = groups.getJSONArray("items");
      for (int i = 0; i < groupItems.size(); i++) {
        JSONObject group = groupItems.getJSONObject(i);
        authenticatedUser.addGroup(Field.getString(group.get("name")));
      }

      return authenticatedUser;
    } catch (JiraException ex) {
      System.err.println(ex.getMessage());
      return null;
    }
  }
}