package com.nemo.dhms.users;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * TODO: Write class Javadoc
 */
public class JiraUser {

  private String username;
  private String name;
  private String displayName;
  private Collection<String> groups = new HashSet<String>();

  /**
   * Get the username for this JIRA user.
   *
   * @return JIRA username
   */
  public String getUsername() {
    return username;
  }

  void setUsername(String username) {
    this.username = username;
  }

  /**
   * Get the name for this JIRA user.
   *
   * @return name
   */
  public String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  /**
   * Get the display name for this JIRA user.
   *
   * @return display name
   */
  public String getDisplayName() {
    return displayName;
  }

  void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  void addGroup(String group) {
    groups.add(group);
  }

  /**
   * Get the groups that this JIRA user belongs to.
   *
   * @return user groups
   */
  public Collection<String> getGroups() {
    return Collections.unmodifiableCollection(groups);
  }

  /**
   * Check for administrative permissions for this JIRA user.
   *
   * @return true if user has administrative permissions
   */
  public boolean isAdmin() {
    // TODO: maybe make this group configurable?
    return groups.contains("jira-administrators");
  }
}
