/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.nemo.dhms.dashboard.NavigatorUI;

/**
 * Source: https://github.com/vaadin/dashboard-demo/blob/8.0/src/main/java/com/vaadin/demo/dashboard/event/DashboardEventBus.java
 *
 * A simple wrapper for Guava event bus. Defines static convenience methods for relevant actions.
 *
 * Guava event bus is used to synchronize users sessions and views
 *
 *
 **/
public class NavigatorEventBus implements SubscriberExceptionHandler {

  private final EventBus eventBus = new EventBus(this);

  //view change requested
  public static void post(final Object event) {
    NavigatorUI.getNavigatorEventbus().eventBus.post(event);
  }

  //registers calling request
  public static void register(final Object object) {
    NavigatorUI.getNavigatorEventbus().eventBus.register(object);
  }

  public static void unregister(final Object object) {
    NavigatorUI.getNavigatorEventbus().eventBus.unregister(object);
  }

  @Override
  public final void handleException(final Throwable exception,
      final SubscriberExceptionContext context) {
    exception.printStackTrace();
  }
}