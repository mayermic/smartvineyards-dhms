/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.event;

import com.nemo.dhms.dashboard.NavigationViewType;

/**
 * Navigator event is responsible for processing view events received by vaadin.
 * Source: Vaadin Dashboard
 */
public abstract class NavigatorEvent {

  public static final class UserLoginRequestedEvent {

    private final String userName, password;

    public UserLoginRequestedEvent(final String userName,
        final String password) {
      this.userName = userName;
      this.password = password;
    }

    public String getUserName() {
      return userName;
    }

    public String getPassword() {
      return password;
    }
  }

  //TODO: sv - add method to authenticate the username and password user entered in vaadin form
  // fields against persistent record for user-- recommend implementing an authenticator controller
  // to perform this behavior.


  public static class BrowserResizeEvent {
      //TODO: insert code to reload view on browser resize event to better handle resize events
  }

  public static class UserLoggedOutEvent {
    //TODO -sv: insert logout logic to go to login screen if authentication reqs are confirmed
  }

  public static final class PostViewChangeEvent {

    private final NavigationViewType view;

    /**
      user has selected a menu button that indicates a desire to change current view --
      this tells vaadin to switch the current view type to the requested view type so that

     */
    public PostViewChangeEvent(final NavigationViewType view) {
      this.view = view;
    }

    public NavigationViewType getView() {
      return view;
    }
  }

  public static class CloseOpenWindowsEvent {

  }

  public static class ProfileUpdatedEvent {

  }
}
