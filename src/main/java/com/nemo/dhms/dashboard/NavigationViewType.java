/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard;

import com.nemo.dhms.dashboard.view.ConfigurationView;
import com.nemo.dhms.dashboard.view.DashboardView;
import com.vaadin.navigator.View;

/**
 * Enum stores the class view for each view we are interested
 * in letting users navigate to.
 *
 * NavigatorEventBus tells google guava which view should be
 * displayed when a navigation event occurs.
 *
 *
 *
 */
public enum NavigationViewType {

  DASHBOARD("dashboard", DashboardView.class, true),
  CONFIGURATION("configuration", ConfigurationView.class, true);

  private final String viewName;
  private final Class<? extends View> viewClass;
  private final boolean stateful;

  private NavigationViewType(final String viewName,
      final Class<? extends View> viewClass,
      final boolean stateful) {
    this.viewName = viewName;
    this.viewClass = viewClass;
    this.stateful = stateful;
  }

  public static NavigationViewType getByViewName(final String viewName) {
    NavigationViewType result = null;
    for (NavigationViewType viewType : values()) {
      if (viewType.getViewName().equals(viewName)) {
        result = viewType;
        break;
      }
    }
    return result;
  }

  public boolean isStateful() {
    return stateful;
  }

  public String getViewName() {
    return viewName;
  }

  public Class<? extends View> getViewClass() {
    return viewClass;
  }
}
