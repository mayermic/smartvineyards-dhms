/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.form;

import com.nemo.dhms.config.Configuration;
import com.nemo.dhms.config.ConfigurationController;
import com.nemo.dhms.dashboard.view.ConfigurationView;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/**
 * The configuration form is a form that allows users to specify configuration settings for
 * Device health ranges
 */
public class ConfigurationForm extends FormLayout {

  private PasswordField sensmitApiKey = new PasswordField("Sensmit API Key");
  private PasswordField googleMapsApiKey = new PasswordField("Google Maps API Key");

  private TextField systemUpdatePeriod = new TextField("System Update Period (Minutes)");
  private TextField warningPeriod = new TextField("Warning Period (Minutes)");
  private TextField failurePeriod = new TextField("Failure Period (Minutes)");
  private TextField selfHealingPeriod = new TextField("Self-healing Period (Minutes)");
  private TextField moistureWarningLevel = new TextField("Moisture Warning Level");
  private TextField moistureFailureLevel = new TextField("Moisture Failure Level");
  private TextField internalTemperatureWarningLevel = new TextField(
      "Internal Temperature Warning Level");
  private TextField internalTemperatureFailureLevel = new TextField(
      "Internal Temperature Failure Level");
  private TextField externalTemperatureWarningLevel = new TextField(
      "External Temperature Warning Level");
  private TextField externalTemperatureFailureLevel = new TextField(
      "External Temperature Failure Level");
  private TextField powerWarningLevel = new TextField("Power Warning Level");
  private TextField powerFailureLevel = new TextField("Power Failure Level");
  private TextField capacitorWarningLevel = new TextField("Capacitor Warning Level");
  private TextField capacitorFailureLevel = new TextField("Capacitor Failure Level");

  private Button cancel = new Button("Cancel");
  private Button save = new Button("Save");

  private ConfigurationView configurationView;

  /**
   * Configuration Form creates a form to be used in the configuration view in order to allow
   * system users to use the dashboard to set configuration settings.
   * @param configurationView
   */
  public ConfigurationForm(ConfigurationView configurationView) {
    this.configurationView = configurationView;

    save.setStyleName(ValoTheme.BUTTON_PRIMARY);
    save.setClickShortcut(KeyCode.ENTER);

    cancel.addClickListener(e -> this.cancel());
    save.addClickListener(e -> this.save());

    updateValues();

    setWidth("100%");
    HorizontalLayout buttons = new HorizontalLayout(cancel, save);
    addComponents(
        sensmitApiKey,
        googleMapsApiKey,
        systemUpdatePeriod,
        warningPeriod,
        failurePeriod,
        selfHealingPeriod,
        moistureWarningLevel,
        moistureFailureLevel,
        internalTemperatureWarningLevel,
        internalTemperatureFailureLevel,
        externalTemperatureWarningLevel,
        externalTemperatureFailureLevel,
        powerWarningLevel,
        powerFailureLevel,
        capacitorWarningLevel,
        capacitorFailureLevel,
        buttons
    );
  }

  private void updateValues() {
    sensmitApiKey.setPlaceholder("(hidden)");
    googleMapsApiKey.setPlaceholder("(hidden)");

    systemUpdatePeriod.setPlaceholder(
        Integer.toString(ConfigurationController.configuration.getSystemUpdatePeriod()));
    warningPeriod.setPlaceholder(
        Integer.toString(ConfigurationController.configuration.getWarningPeriod()));
    failurePeriod.setPlaceholder(
        Integer.toString(ConfigurationController.configuration.getFailurePeriod()));
    selfHealingPeriod.setPlaceholder(
        Integer.toString(ConfigurationController.configuration.getSelfHealingPeriod()));
    moistureWarningLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getMoistureWarningLevel()));
    moistureFailureLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getMoistureFailureLevel()));
    internalTemperatureWarningLevel.setPlaceholder(
        Double
            .toString(ConfigurationController.configuration.getInternalTemperatureWarningLevel()));
    internalTemperatureFailureLevel.setPlaceholder(
        Double
            .toString(ConfigurationController.configuration.getInternalTemperatureFailureLevel()));
    externalTemperatureWarningLevel.setPlaceholder(
        Double
            .toString(ConfigurationController.configuration.getExternalTemperatureWarningLevel()));
    externalTemperatureFailureLevel.setPlaceholder(
        Double
            .toString(ConfigurationController.configuration.getExternalTemperatureFailureLevel()));
    powerWarningLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getPowerWarningLevel()));
    powerFailureLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getPowerFailureLevel()));
    capacitorWarningLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getCapacitorWarningLevel()));
    capacitorFailureLevel.setPlaceholder(
        Double.toString(ConfigurationController.configuration.getCapacitorFailureLevel()));
  }

  private void clearAll() {
    sensmitApiKey.clear();
    googleMapsApiKey.clear();
    systemUpdatePeriod.clear();
    warningPeriod.clear();
    failurePeriod.clear();
    selfHealingPeriod.clear();
    moistureWarningLevel.clear();
    moistureFailureLevel.clear();
    internalTemperatureWarningLevel.clear();
    internalTemperatureFailureLevel.clear();
    externalTemperatureWarningLevel.clear();
    externalTemperatureFailureLevel.clear();
    powerWarningLevel.clear();
    powerFailureLevel.clear();
    capacitorWarningLevel.clear();
    capacitorFailureLevel.clear();
  }

  public void cancel() {
    clearAll();

    updateValues();
  }

  public void save() {
    // Save each individual value
    if (!sensmitApiKey.getValue().equals("")) {
      ConfigurationController.setSensmitApiKey(sensmitApiKey.getValue());
    }
    if (!googleMapsApiKey.getValue().equals("")) {
      ConfigurationController.setGoogleMapsApiKey(googleMapsApiKey.getValue());
    }
    if (!systemUpdatePeriod.getValue().equals("")) {
      ConfigurationController
          .setSystemUpdatePeriod(Integer.parseInt(systemUpdatePeriod.getValue()));
    }
    if (!warningPeriod.getValue().equals("")) {
      ConfigurationController.setWarningPeriod(Integer.parseInt(warningPeriod.getValue()));
    }
    if (!failurePeriod.getValue().equals("")) {
      ConfigurationController.setFailurePeriod(Integer.parseInt(failurePeriod.getValue()));
    }
    if (!selfHealingPeriod.getValue().equals("")) {
      ConfigurationController.setSelfHealingPeriod(Integer.parseInt(selfHealingPeriod.getValue()));
    }
    if (!moistureWarningLevel.getValue().equals("")) {
      ConfigurationController
          .setMoistureWarningLevel(Double.parseDouble(moistureWarningLevel.getValue()));
    }
    if (!moistureFailureLevel.getValue().equals("")) {
      ConfigurationController
          .setMoistureFailureLevel(Double.parseDouble(moistureFailureLevel.getValue()));
    }
    if (!internalTemperatureWarningLevel.getValue().equals("")) {
      ConfigurationController.setInternalTemperatureWarningLevel(
          Double.parseDouble(internalTemperatureWarningLevel.getValue()));
    }
    if (!internalTemperatureFailureLevel.getValue().equals("")) {
      ConfigurationController.setInternalTemperatureFailureLevel(
          Double.parseDouble(internalTemperatureFailureLevel.getValue()));
    }
    if (!externalTemperatureWarningLevel.getValue().equals("")) {
      ConfigurationController.setExternalTemperatureWarningLevel(
          Double.parseDouble(externalTemperatureWarningLevel.getValue()));
    }
    if (!externalTemperatureFailureLevel.getValue().equals("")) {
      ConfigurationController.setExternalTemperatureFailureLevel(
          Double.parseDouble(externalTemperatureFailureLevel.getValue()));
    }
    if (!powerWarningLevel.getValue().equals("")) {
      ConfigurationController
          .setPowerWarningLevel(Double.parseDouble(powerWarningLevel.getValue()));
    }
    if (!powerFailureLevel.getValue().equals("")) {
      ConfigurationController
          .setPowerFailureLevel(Double.parseDouble(powerFailureLevel.getValue()));
    }
    if (!capacitorWarningLevel.getValue().equals("")) {
      ConfigurationController
          .setCapacitorWarningLevel(Double.parseDouble(capacitorWarningLevel.getValue()));
    }
    if (!capacitorFailureLevel.getValue().equals("")) {
      ConfigurationController
          .setPowerFailureLevel(Double.parseDouble(capacitorFailureLevel.getValue()));
    }

    ConfigurationController.saveConfiguration();

    // TODO: check for input errors and handle

    clearAll();

    updateValues();
  }
}
