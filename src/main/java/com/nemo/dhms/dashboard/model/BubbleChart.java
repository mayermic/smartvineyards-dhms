package com.nemo.dhms.dashboard.model;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BubbleChartConfig;
import com.byteowls.vaadin.chartjs.data.BubbleDataset;
import com.byteowls.vaadin.chartjs.data.Data;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.LinearScale;
import com.byteowls.vaadin.chartjs.options.scale.Scales;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import java.util.List;

/**
 * Represents the desired chart data in a bubble chart format.
 */
public class BubbleChart {
  public static ChartJs createChart(List<BubbleDataset> chartDataList, int scaleType) {

    BubbleChartConfig config = new BubbleChartConfig();
    Data<BubbleChartConfig> bubbleChartConfigData = config.data();
    Scales scales = bubbleChartConfigData.and()
        .options()
        .scales();
    adjustScale(scales, scaleType);  //TODPL
    bubbleChartConfigData
        .and()
        .options()
        .responsive(true)
        .title()
        .display(true)
        .text("24 hour History Chart")
        .and()
        .maintainAspectRatio(true)
        .done();

    for (BubbleDataset bubbleDataset : chartDataList) {
      bubbleChartConfigData.addDataset(bubbleDataset);
    }

    ChartJs chart = new ChartJs(config);
    chart.setJsLoggingEnabled(true);
    chart.addClickListener((a, b) -> {
      Notification.show("Dataset at Idx:" + a + "; Data at Idx: " + b + "; Value: " +
          config.data().getDatasets().get(a).getData().get(b), Type.TRAY_NOTIFICATION);
    });

    return new ChartJs(config);
  }

  private static void adjustScale(Scales scales, int scaleType) {
    switch (scaleType) {
      case 0:
        dayViewScale(scales);
        break;
      case 1:
        weekViewScale(scales);
        break;
      case 2:
        yearViewScale(scales);
        break;
      case 3:
        monthViewScale(scales);
        break;
    }
  }


  private static void dayViewScale(Scales scales) {
    scales
        .add(Axis.Y, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Status")
            .and()
            .ticks()
            .min(0)
            .max(6)
            .fixedStepSize(1)
            .and()
            .position(Position.LEFT))
        .add(Axis.X, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Time")
            .and()
            .ticks()
            .fixedStepSize(100)
            .suggestedMin(0)
            .suggestedMax(2330)
            .and()
            .position(Position.BOTTOM));
  }

  private static void weekViewScale(Scales scales) {
  }

  private static void monthViewScale(Scales scales) {
    scales
        .add(Axis.Y, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Time")
            .and()
            .ticks()
            .min(0)
            .fixedStepSize(1)
            .max(24)
            .fixedStepSize(.5)
            .and()
            .position(Position.LEFT))
        .add(Axis.X, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Day")
            .and()
            .ticks()
            .fixedStepSize(1)
            .suggestedMin(0)
            .suggestedMax(30)
            .and()
            .position(Position.BOTTOM));
  }

  private static void yearViewScale(Scales scales) {
    scales
        .add(Axis.Y, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Time")
            .and()
            .ticks()
            .fixedStepSize(100)
            .suggestedMin(0)
            .suggestedMax(2330)
            .and()
            .position(Position.LEFT))
        .add(Axis.X, new LinearScale()
            .display(true)
            .scaleLabel()
            .display(true)
            .labelString("Days")
            .and()
            .ticks()
            .fixedStepSize(1)
            .suggestedMin(0)
            .suggestedMax(364)
            .and()
            .position(Position.BOTTOM));
  }
}
