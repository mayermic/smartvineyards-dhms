package com.nemo.dhms.dashboard.model;

/**
 * Enum to store the hex values for colors associated with Mesh and Device statuses.
 *
 */
public enum Color {
  GREEN("#4CAF50"), RED("#F44336"), YELLOW("#FFFF00"), BLUE("#0000FF"), DARKGRAY("#696969"),
  LIGHTGRAY("#D3D3D3");

  String hexcode;

  Color(String hexcode) {
    this.hexcode = hexcode;
  }

  public String getHexcode() {
    return hexcode;
  }
}
