/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.model;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.PieChartConfig;
import com.byteowls.vaadin.chartjs.data.PieDataset;
import com.nemo.dhms.dashboard.GeneralUtil;
import com.nemo.dhms.system.MeshStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * MeshesChart class is called by Chart classes to create meshes piecharts for UI graphical data
 * display MeshChart title is initialized to a String
 */
public class MeshesChart extends PieChart {
  int healthy;
  int not_healthy;
  private String title = "Mesh Status Chart";
  ArrayList<String> labels;
  PieDataset pieDataset;
  Map<MeshStatus, AtomicInteger> meshStatusAtomicIntegerMap;

  /**
   * MeshesChart Constructor initializes healthy and unhealthy chart parameters
   */
  public MeshesChart(int healthy, int not_healthy) {
    this.healthy = healthy;
    this.not_healthy = not_healthy;
  }

  public MeshesChart(Map<MeshStatus, AtomicInteger> meshStatusAtomicIntegerMap){
    this.meshStatusAtomicIntegerMap = meshStatusAtomicIntegerMap;
    labels = new ArrayList<>();
    List<String> colors = new ArrayList<>();
    pieDataset = new PieDataset();
    for (MeshStatus meshStatus: MeshStatus.values()){
      labels.add(meshStatus.toString());
      int count = meshStatusAtomicIntegerMap.get(meshStatus).get();
      pieDataset.addData(count);
      colors.add(GeneralUtil.getMeshStatusColor(meshStatus));
    }
    pieDataset.backgroundColor(colors.toArray(new String[colors.size()]));
  }

  /**
   * createChart creates a ChartJs chart object using a piechart config object.
   *
   * @return returns a ChartJs chart object to the caller to display a UI meshes piechart
   */
  @Override
  public ChartJs createChart() {
    PieChartConfig config = new PieChartConfig();
    config
        .data()
        .labelsAsList(labels)
        .addDataset(this.pieDataset)
        .and();
    config.
        options()
        .responsive(true)
        .title()
        .display(true)
               .text("Current Mesh statuses")
        .and()
        .animation()
        .animateScale(true)
        .animateRotate(true)
        .and()
        .legend()
        .fullWidth(true)
        .and()
        .done();
    ChartJs chart = new ChartJs(config);
    chart.setHeight("350px");
    return chart;
  }
}
