package com.nemo.dhms.dashboard.model;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BubbleChartConfig;
import com.byteowls.vaadin.chartjs.data.BubbleDataset;
import com.nemo.dhms.dashboard.GeneralUtil;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Device history bubble chart returns device data in a bubble chart representation.
 */
public class DeviceHistoryBubbleChart {

  private List<BubbleDataset> chartDataSetList = new ArrayList<>();
  private final Collection<DeviceHistoryEntry> deviceHistoryEntries;
  private final String chartDateRange;
  private final Month month = LocalDateTime.now().getMonth();
  private final int day = LocalDateTime.now().getDayOfMonth();
  private final int year = LocalDateTime.now().getYear();

  public DeviceHistoryBubbleChart(Collection<DeviceHistoryEntry> deviceHistoryEntries,
      String chartDateRange) {
    this.deviceHistoryEntries = deviceHistoryEntries;
    this.chartDateRange = chartDateRange;
  }

  public ChartJs createChart() {
    int chartScaleType = 0;
    if (deviceHistoryEntries == null) {
      BubbleChartConfig config = new BubbleChartConfig();
      config.data().clear();
      config.data().labels("Could not find history associated with device");
      config.options()
          .title().display(true).text("Could not find history associated with device");
    } else {
      if (chartDateRange.equals(DateRange.DAY.getTextValue())) {
        createDayHistoryData();
        chartScaleType = 0;
      } else if (chartDateRange.equals(DateRange.WEEK.getTextValue())) {//
        createWeekHistoryData();
        chartScaleType = 1;
      } else if (chartDateRange.equals(DateRange.YEAR.getTextValue())) {
        createYearHistoryData();
        chartScaleType = 2;
      } else {
        createMonthHistoryData();
        chartScaleType = 3;
      }
    }
    return BubbleChart.createChart(chartDataSetList, chartScaleType);
  }

  /**
   * Creates bubbledata set for each status type
   */
  private void createDayHistoryData() {
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      deviceHistoryEntries.stream().filter(e ->
          e.getDate().getDayOfMonth() == day
              && e.getDate().getMonth() == month
              && e.getDate().getYear() == year
              && e.getStatus() == deviceStatus)
          .forEach(e -> {
            bubbleDataset.addData(GeneralUtil.militaryTimeConversion(e.getDate()), y, 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
            bubbleDataset.label((int) y + ":" + deviceStatus.toString());
          });

      if(bubbleDataset.getData() == null){
        bubbleDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
        bubbleDataset.label((int) y + ":" + deviceStatus.toString());
      }
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createWeekHistoryData() {
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      deviceHistoryEntries.stream().filter(e ->
          e.getDate().getDayOfMonth() > day - 7
              && e.getDate().getDayOfMonth() <= day
              && e.getDate().getMonth() == month
              && e.getDate().getYear() == year
              && e.getStatus() == deviceStatus)
          .forEach(e -> {
            double dayOfMonth = e.getDate().getDayOfMonth();
            bubbleDataset.addData(dayOfMonth, GeneralUtil.militaryTimeConversion(e.getDate()), 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
            bubbleDataset.label((int) dayOfMonth + ":" + deviceStatus.toString());
          });
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createYearHistoryData() {
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      deviceHistoryEntries.stream().filter(e ->
          e.getDate().getYear() == year
              && e.getStatus() == deviceStatus)
          .forEach(e -> {
            double dayOfYear = e.getDate().getDayOfYear();
            bubbleDataset.addData(dayOfYear, GeneralUtil.militaryTimeConversion(e.getDate()), 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
            bubbleDataset.label((int) dayOfYear + ":" + deviceStatus.toString());
          });
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createMonthHistoryData() {
    Month month = Month.valueOf(this.chartDateRange);
    System.out.println("month value: " + month);

  }
}
