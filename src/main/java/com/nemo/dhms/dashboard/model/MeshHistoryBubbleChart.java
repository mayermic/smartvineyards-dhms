package com.nemo.dhms.dashboard.model;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BubbleChartConfig;
import com.byteowls.vaadin.chartjs.data.BubbleDataset;
import com.nemo.dhms.dashboard.GeneralUtil;

import com.nemo.dhms.system.MeshHistoryEntry;
import com.nemo.dhms.system.MeshStatus;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MeshHistoryBubbleChart {

  private List<BubbleDataset> chartDataSetList = new ArrayList<>();
  private final Collection<MeshHistoryEntry> meshHistoryEntries;
  private final String chartDateRange;
  private final Month month = LocalDateTime.now().getMonth();
  private final int day = LocalDateTime.now().getDayOfMonth();
  private final int year = LocalDateTime.now().getYear();

  public MeshHistoryBubbleChart(Collection<MeshHistoryEntry> meshHistoryEntries,
      String chartDateRange) {
    this.meshHistoryEntries = meshHistoryEntries;
    this.chartDateRange = chartDateRange;
  }

  public ChartJs createChart() {
    int chartScaleType = 0;
    if (meshHistoryEntries == null) {
      BubbleChartConfig config = new BubbleChartConfig();
      config.data().clear();
      config.data().labels("Could not find history associated with mesh");
      config.options()
          .title().display(true).text("Could not find history associated with mesh");
    } else {
      if (chartDateRange.equals(DateRange.DAY.getTextValue())) {
        createDayHistoryData();
        chartScaleType = 0;
      } else if (chartDateRange.equals(DateRange.WEEK.getTextValue())) {//
        createWeekHistoryData();
        chartScaleType = 1;
      } else if (chartDateRange.equals(DateRange.YEAR.getTextValue())) {
        createYearHistoryData();
        chartScaleType = 2;
      } else {
        createMonthHistoryData();
        chartScaleType = 3;
      }
    }
    return BubbleChart.createChart(chartDataSetList, chartScaleType);
  }


  /**
   * Creates bubbledata set for each status type
   */
  private void createDayHistoryData() {
    MeshStatus[] meshStatuses = MeshStatus.values();
    for (int i = 0; i < meshStatuses.length; i++) {
      final MeshStatus meshStatus = meshStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      meshHistoryEntries.stream().filter(e ->
          e.getDate().getDayOfMonth() == day
              && e.getDate().getMonth() == month
              && e.getDate().getYear() == year
              && e.getStatus() == meshStatus)
          .forEach(e -> {
            bubbleDataset.addData(GeneralUtil.militaryTimeConversion(e.getDate()), y, 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getMeshStatusColor(meshStatus));
            bubbleDataset.label((int) y + ":" + meshStatus.toString());
          });

      if (bubbleDataset.getData() == null){
        bubbleDataset.backgroundColor(GeneralUtil.getMeshStatusColor(meshStatus));
        bubbleDataset.label((int) y + ":" + meshStatus.toString());
      }
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createWeekHistoryData() {
    MeshStatus[] meshStatuses = MeshStatus.values();
    for (int i = 0; i < meshStatuses.length; i++) {
      final MeshStatus meshStatus = meshStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      meshHistoryEntries.stream().filter(e ->
          e.getDate().getDayOfMonth() > day - 7
              && e.getDate().getDayOfMonth() <= day
              && e.getDate().getMonth() == month
              && e.getDate().getYear() == year
              && e.getStatus() == meshStatus)
          .forEach(e -> {
            double dayOfMonth = e.getDate().getDayOfMonth();
            bubbleDataset.addData(dayOfMonth, GeneralUtil.militaryTimeConversion(e.getDate()), 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getMeshStatusColor(meshStatus));
            bubbleDataset.label((int) dayOfMonth + ":" + meshStatus.toString());
          });
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createYearHistoryData() {
    MeshStatus[] meshStatuses = MeshStatus.values();
    for (int i = 0; i < meshStatuses.length; i++) {
      final MeshStatus meshStatus = meshStatuses[i];
      final double y = (double) i + 1;
      BubbleDataset bubbleDataset = new BubbleDataset();
      meshHistoryEntries.stream().filter(e ->
          e.getDate().getYear() == year
              && e.getStatus() == meshStatus)
          .forEach(e -> {
            double dayOfYear = e.getDate().getDayOfYear();
            bubbleDataset.addData(dayOfYear, GeneralUtil.militaryTimeConversion(e.getDate()), 6.0);
            bubbleDataset.backgroundColor(GeneralUtil.getMeshStatusColor(meshStatus));
            bubbleDataset.label((int) dayOfYear + ":" + meshStatus.toString());
          });
      this.chartDataSetList.add(bubbleDataset);
    }
  }

  private void createMonthHistoryData() {
    Month month = Month.valueOf(this.chartDateRange);
    System.out.println("month value: " + month);
  }
}
