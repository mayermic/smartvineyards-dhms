package com.nemo.dhms.dashboard.model;

/**
 *
 * Enum type -- used to indicate what range of history to display for a device/mesh
 */
public enum DateRange {
  DAY("24 HOURS"), WEEK("WEEK"), YEAR("YEAR"), JANUARY("JANUARY"), FEBRUARY(
      "FEBRUARY"), MARCH("MARCH"), APRIL("APRIL"), MAY("MAY"), JUNE("JUNE"), JULY("JULY"), AUGUST(
      "AUGUST"), SEPTEMBER("SEPTEMBER"), OCTOBER("OCTOBER"), NOVEMBER("NOVEMBER"), DECEMBER(
      "DECEMBER");
  private final String dateRange;

  DateRange(String dateRange) {
    this.dateRange = dateRange;
  }

  public String getTextValue() {
    return dateRange;
  }
}
