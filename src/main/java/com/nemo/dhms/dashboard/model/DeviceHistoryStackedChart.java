package com.nemo.dhms.dashboard.model;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.BarChartConfig;
import com.byteowls.vaadin.chartjs.config.BubbleChartConfig;
import com.byteowls.vaadin.chartjs.data.BarDataset;
import com.byteowls.vaadin.chartjs.data.Data;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.DefaultScale;
import com.nemo.dhms.dashboard.GeneralUtil;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Represent device history entries as stacked bar chart
 */
public class DeviceHistoryStackedChart {

  private List<BarDataset> chartDataSetList = new ArrayList<>();
  private final Collection<DeviceHistoryEntry> deviceHistoryEntries;
  private final String chartDateRange;
  private final LocalDateTime dateTimeNow = LocalDateTime.now();
  private final int year = dateTimeNow.getYear();
  private List<String> labels;

  public DeviceHistoryStackedChart(
      Collection<DeviceHistoryEntry> deviceHistoryEntries, String chartDateRange) {
    this.deviceHistoryEntries = deviceHistoryEntries;
    this.chartDateRange = chartDateRange;
    this.labels = new ArrayList<>();
  }

  public ChartJs createChart() {
    if (deviceHistoryEntries == null) {
      BubbleChartConfig config = new BubbleChartConfig();
      config.data().clear();
      config.data().labels("Could not find history associated with device");
      config.options()
          .title().display(true).text("Could not find history associated with device");
    } else {
      if (chartDateRange.equals(DateRange.WEEK.getTextValue())) {
        createWeekHistoryData();
      } else if (chartDateRange.equals(DateRange.YEAR.getTextValue())) {
        createYearHistoryData();
      } else {
        createMonthHistoryData();
      }
    }
    return createStackedChart(labels);
  }

  private void createWeekHistoryData() {
    TreeSet<LocalDateTime> weekValues = new TreeSet<>(new Comparator<LocalDateTime>() {
      @Override
      public int compare(LocalDateTime o1, LocalDateTime o2) {
        return o1.compareTo(o2);
      }
    });
    //get list of local date times for the past 7 days including today
    for (int i = 0; i < 7; i++) {
      weekValues.add(dateTimeNow.minusDays(i));
    }

    //create labels for days -- iterate through loop after adding to ensure labels are ordered
    this.labels = new ArrayList<>();
    for (LocalDateTime date : weekValues) {
      this.labels.add(date.getDayOfWeek().toString());
    }

    //For each day of the week, count the number of times the device was in each status type
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      BarDataset barDataset = new BarDataset();
      //for each Day of the week, count the number of times the device was in this status
      for (LocalDateTime date : weekValues) {
        float statusCountForDay =
            deviceHistoryEntries.stream()
                .filter(entry -> entry.getDate().getDayOfMonth() == date.getDayOfMonth()
                    && entry.getDate().getMonth() == date.getMonth()
                    && entry.getDate().getYear() == date.getYear()
                    && entry.getStatus() == deviceStatus)
                .count();
        barDataset.addData((double) statusCountForDay);
        barDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
        barDataset.label(deviceStatus.toString());
      }
      this.chartDataSetList.add(barDataset);
    }
  }

  //for each day
  private void createMonthHistoryData() {
    Month targetMonth = Month.valueOf(chartDateRange);
    int daysOfMonth = LocalDate.of(LocalDate.now().getYear(), targetMonth, 1).lengthOfMonth();
    ArrayList<String> labels = new ArrayList<>();
    for (int day = 1; day <= daysOfMonth; ++day) {
      labels.add(String.valueOf(day));
    }
    this.labels = labels;
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      BarDataset barDataset = new BarDataset();
      final double y = (double) i + 1;
      List<DeviceHistoryEntry> yearEntries = deviceHistoryEntries.stream().filter(e ->
          e.getDate().getYear() == year
              && e.getDate().getMonth().equals(targetMonth)
              && e.getStatus() == deviceStatus).collect(Collectors.toList());
      //for each Month, count the number of times the device was in this status
      for (int j = 1; j < daysOfMonth; j++) {
        final double day = (double) j;
        float statusCountForDay =
            yearEntries.stream()
                .filter(entry -> entry.getDate().getDayOfMonth() == day)
                .count();
        barDataset.addData((double) statusCountForDay);
        barDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
        barDataset.label(deviceStatus.toString());
      }
      this.chartDataSetList.add(barDataset);
    }
  }

  /**
   * Get a count of the number of
   *
   * for each day --
   */
  private void createYearHistoryData() {
    this.labels = Arrays.asList("January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December");
    DeviceStatus[] deviceStatuses = DeviceStatus.values();
    for (int i = 0; i < deviceStatuses.length; i++) {
      final DeviceStatus deviceStatus = deviceStatuses[i];
      BarDataset barDataset = new BarDataset();
      final double y = (double) i + 1;
      List<DeviceHistoryEntry> yearEntries = deviceHistoryEntries.stream().filter(e ->
          e.getDate().getYear() == year
              && e.getStatus() == deviceStatus).collect(Collectors.toList());
      //for each Month, count the number of times the device was in this status
      for (int j = 1; j < 13; j++) {
        final double month = (double) j;
        float statusCountForDay =
            yearEntries.stream()
                .filter(entry -> entry.getDate().getMonthValue() == month)
                .count();
        barDataset.addData((double) statusCountForDay);
        barDataset.backgroundColor(GeneralUtil.getDeviceStatusColor(deviceStatus));
        barDataset.label(deviceStatus.toString());
      }
      this.chartDataSetList.add(barDataset);
    }
  }

  public ChartJs createStackedChart(List<String> labels) {
    BarChartConfig config = new BarChartConfig();
    Data<BarChartConfig> barChartConfigData = config.data();
    config.data()
        .labelsAsList(labels);
    config
        .options()
        .responsive(true)
        .title()
        .display(true)
        .text("Device History Chart")
        .and()
        .tooltips()
        .mode(InteractionMode.INDEX)
        .intersect(false)
        .and()
        .scales()
        .add(Axis.X, new DefaultScale()
            .stacked(true))
        .add(Axis.Y, new DefaultScale()
            .stacked(true))
        .and()
        .done();

    for (BarDataset barDataset : this.chartDataSetList) {
      barChartConfigData.addDataset(barDataset);
    }

    ChartJs chart = new ChartJs(config);
    chart.addClickListener((a, b) -> {
      BarDataset dataset = (BarDataset) config.data().getDatasets().get(a);
    });
    chart.setJsLoggingEnabled(true);
    return chart;
  }
}