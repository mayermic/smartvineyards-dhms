/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard;

import com.nemo.dhms.dashboard.NavigationViewType;
import com.nemo.dhms.dashboard.event.NavigatorEvent.BrowserResizeEvent;
import com.nemo.dhms.dashboard.event.NavigatorEvent.CloseOpenWindowsEvent;
import com.nemo.dhms.dashboard.event.NavigatorEvent.PostViewChangeEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;

/**
 * Source: https://github.com/vaadin/dashboard-demo/blob/8.0/src/main/java/com/vaadin/demo/dashboard/DashboardNavigator.java
 */
@SuppressWarnings("serial")
public class SiteNavigator extends Navigator {

  private static final NavigationViewType ERROR_VIEW = NavigationViewType.DASHBOARD;
  private ViewProvider errorViewProvider;

  public SiteNavigator(final ComponentContainer container) {
    super(UI.getCurrent(), container);
    initViewChangeListener();
    initViewProviders();
  }

  /**
   * Create a vaadin listener that listens for view change requests.
   */
  private void initViewChangeListener() {
    addViewChangeListener(new ViewChangeListener() {

      @Override
      public boolean beforeViewChange(final ViewChangeEvent event) {
        // Since there's no conditions in switching between the views
        // we can always return true.
        return true;
      }

      /**
       * Upon receipt of view change event, get the new viewType and perform
       * appropriate Navigator Event bus actions.
       * @param event
       */
      @Override
      public void afterViewChange(final ViewChangeEvent event) {
        NavigationViewType view = NavigationViewType.getByViewName(event
            .getViewName());
        // Appropriate events get fired after the view is changed.
        NavigatorEventBus.post(new PostViewChangeEvent(view));
        NavigatorEventBus.post(new BrowserResizeEvent());
        NavigatorEventBus.post(new CloseOpenWindowsEvent());
      }
    });
  }

  private void initViewProviders() {
    // A dedicated view provider is added for each separate view type
    for (final NavigationViewType viewType : NavigationViewType.values()) {
      ViewProvider viewProvider = new ClassBasedViewProvider(
          viewType.getViewName(), viewType.getViewClass()) {

        // This field caches an already initialized view instance if the
        // view should be cached (stateful views).
        private View cachedInstance;

        @Override
        public View getView(final String viewName) {
          View result = null;
          if (viewType.getViewName().equals(viewName)) {
            if (viewType.isStateful()) {
              // Stateful views get lazily instantiated
              if (cachedInstance == null) {
                cachedInstance = super.getView(viewType
                    .getViewName());
              }
              result = cachedInstance;
            } else {
              // Non-stateful views get instantiated every time
              // they're navigated to
              result = super.getView(viewType.getViewName());
            }
          }
          return result;
        }
      };

      if (viewType == ERROR_VIEW) {
        errorViewProvider = viewProvider;
      }

      addProvider(viewProvider);
    }

    setErrorProvider(new ViewProvider() {
      @Override
      public String getViewName(final String viewAndParameters) {
        return ERROR_VIEW.getViewName();
      }

      @Override
      public View getView(final String viewName) {
        return errorViewProvider.getView(ERROR_VIEW.getViewName());
      }
    });
  }
}