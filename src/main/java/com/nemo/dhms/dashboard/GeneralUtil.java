package com.nemo.dhms.dashboard;

import com.nemo.dhms.dashboard.model.Color;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import com.nemo.dhms.system.MeshHistoryEntry;
import com.nemo.dhms.system.MeshStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Stores common utility methods that are used by more than one class.
 */
public class GeneralUtil {


  /**
   * create 24 hours worth of fake device history per day for x days where x = floor(the current day
   * of the year / 5)
   *
   * @return Collection<DeviceHistoryEntry> list of mock device history entries for every day
   * created.
   */
  public static Collection<DeviceHistoryEntry> createFakeDeviceHistory() {
    Random random = new Random();
    ArrayList<DeviceHistoryEntry> deviceHistoryEntries = new ArrayList<>();

    ArrayList<LocalDateTime> dateTimeList = createMockDateTimesForDay(LocalDate.now());
    int currentDay = LocalDate.now().getDayOfYear();
    for (int i = currentDay - 5; i > 0; i -= 5) {
      dateTimeList.addAll(createMockDateTimesForDay(LocalDate.ofYearDay(2018, i)));
    }
    for (LocalDateTime localDateTime : dateTimeList) {
      DeviceStatus deviceStatus = getDeviceStatus(random.nextInt(12));  //random generate num from 0-11
      deviceHistoryEntries.add(new DeviceHistoryEntry("AAAAAA", localDateTime, deviceStatus));
    }
    return deviceHistoryEntries;
  }

  /**
   * create 24 hours worth of fake mesh history per day for x days
   * where x = floor(the current day of the year / 5)
   * @return Collection<MeshHistoryEntry> list of mock mesh history entries for every day
   * created.
   */
  public static Collection<MeshHistoryEntry> createFakeMeshHistory(){
   Random random = new Random();
    ArrayList<MeshHistoryEntry> meshHistoryEntries = new ArrayList<>();
    ArrayList<LocalDateTime> dateTimeList = createMockDateTimesForDay(LocalDate.now());
    int currentDay = LocalDate.now().getDayOfYear();
    for(int i = currentDay - 5; i > 0 ; i -= 5) {
      dateTimeList.addAll(createMockDateTimesForDay(LocalDate.ofYearDay(2018, i)));
    }
    for (LocalDateTime localDateTime : dateTimeList) {
      MeshStatus meshStatus = getMeshStatus(random.nextInt(10));  //random generate num from 0-9
      meshHistoryEntries.add(new MeshHistoryEntry("AAAAAA", localDateTime, meshStatus));
    }
    return meshHistoryEntries;
  }

  /**
   * @return 24 hours of datetime values stored in 30 minutes increments
   */
  private static ArrayList<LocalDateTime> createMockDateTimesForDay(LocalDate localDate) {
    ArrayList<LocalDateTime> dateTimeList = new ArrayList<>();
    int hour = 0;
    int minute = 00;

    //48 half hour increments in a day  -- creates 24 hours worth of fake history using military
    //time
    for (int i = 0; i < 48; ++i) {
      if (i % 2 == 0) {
        if (i > 0) {
          ++hour;
        }
        minute = 00;
      } else {
        minute = 30;
      }
      LocalTime localTime = LocalTime.of(hour, minute);
      dateTimeList.add(LocalDateTime.of(localDate, localTime));
    }
    return dateTimeList;
  }

  private static DeviceStatus getDeviceStatus(int randomNum) {
    DeviceStatus deviceStatus;
    if (0 <= randomNum && randomNum < 2) {
      deviceStatus = DeviceStatus.ACTIVE;
    } else if (2 <= randomNum && randomNum < 4) {
      deviceStatus = DeviceStatus.HEALTHY;
    } else if (4 <= randomNum && randomNum < 6) {
      deviceStatus = DeviceStatus.WARNING;
    } else if (6 <= randomNum && randomNum < 8) {
      deviceStatus = DeviceStatus.FAILURE;
    } else if (8 <= randomNum && randomNum < 10) {
      deviceStatus = DeviceStatus.IDLE;
    } else {
      deviceStatus = DeviceStatus.INACTIVE;
    }
    return deviceStatus;
  }

  private static MeshStatus getMeshStatus(int randomNum) {
    MeshStatus meshStatus;
    if (0 <= randomNum && randomNum < 2) {
      meshStatus = MeshStatus.ACTIVE;
    } else if (2 <= randomNum && randomNum < 4) {
      meshStatus = MeshStatus.HEALTHY;
    } else if (4 <= randomNum && randomNum < 6) {
      meshStatus = MeshStatus.WARNING;
    } else if (6 <= randomNum && randomNum < 8) {
      meshStatus = MeshStatus.FAILURE;
    } else if (8 <= randomNum && randomNum < 10) {
      meshStatus = MeshStatus.IDLE;
    } else {
      meshStatus = MeshStatus.INACTIVE;
    }
    return meshStatus;
  }

  public static String getDeviceStatusColor(DeviceStatus deviceStatus) {
    String color;
    switch (deviceStatus) {
      case ACTIVE:
        color = Color.BLUE.getHexcode();
        break;
      case HEALTHY:
        color = Color.GREEN.getHexcode();
        break;
      case WARNING:
        color = Color.YELLOW.getHexcode();
        break;
      case FAILURE:
        color = Color.RED.getHexcode();
        break;
      case IDLE:
        color = Color.LIGHTGRAY.getHexcode();
        break;
      case INACTIVE:
        color = Color.DARKGRAY.getHexcode();
        break;
      default:
        throw new RuntimeException("Device status does not have a color associated with it "
            + "- update method");
    }
    return color;
  }

  public static String getMeshStatusColor(MeshStatus meshStatus){
      String color;
      switch (meshStatus) {
        case ACTIVE:
          color = Color.BLUE.getHexcode();
          break;
        case HEALTHY:
          color = Color.GREEN.getHexcode();
          break;
        case WARNING:
          color = Color.YELLOW.getHexcode();
          break;
        case FAILURE:
          color = Color.RED.getHexcode();
          break;
        case IDLE:
          color = Color.LIGHTGRAY.getHexcode();
          break;
        case INACTIVE:
          color = Color.DARKGRAY.getHexcode();
          break;
        default:
          throw new RuntimeException("Device status does not have a color associated with it "
              + "- update method");
      }
      return color;
  }

  public static double militaryTimeConversion(LocalDateTime deviceDateTime) {
    return (double) (deviceDateTime.getHour() * 100 + deviceDateTime.getMinute());
  }
}