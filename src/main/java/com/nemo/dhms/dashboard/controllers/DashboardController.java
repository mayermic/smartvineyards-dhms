/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.controllers;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.LineChartConfig;
import com.nemo.dhms.dashboard.GeneralUtil;
import com.nemo.dhms.dashboard.model.DateRange;
import com.nemo.dhms.dashboard.model.DeviceHistoryBubbleChart;
import com.nemo.dhms.dashboard.model.DeviceHistoryStackedChart;
import com.nemo.dhms.dashboard.model.DevicesChart;
import com.nemo.dhms.dashboard.model.MeshHistoryBubbleChart;
import com.nemo.dhms.dashboard.model.MeshHistoryStackedChart;
import com.nemo.dhms.dashboard.model.MeshesChart;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.Location;
import com.nemo.dhms.system.Mesh;
import com.nemo.dhms.system.MeshHistoryEntry;
import com.nemo.dhms.system.SystemController;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.events.MarkerClickListener;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DashboardController is called by DashboardView to create charts for UI graphical data display
 */
public class DashboardController {
  private static final Logger log = LoggerFactory.getLogger(DashboardController.class);
  private static String googleMapsApiKey;

  private DashboardController() {
  }

  /**
   * Called by DashboardView to create meshChart object for graphical display
   *
   * @return returns meshChart object to caller
   */

  public static void setGoogleMapsApiKey(String apiKey) {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController", "setGoogleMapsApiKey()");
    googleMapsApiKey = apiKey;
  }

  /**
   * @return vaadin layout that holds google map layout
   */
  public static VerticalLayout googleMapVerticalLayout() {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "googleMapVerticalLayout()");
    VerticalLayout mapContent = new VerticalLayout();

    GoogleMap googleMap = DashboardController.createGraphicalMeshMap();
    mapContent.addComponent(googleMap);

    Panel console = new Panel();
    console.setHeight("100px");
    double width = Page.getCurrent().getBrowserWindowWidth();
    String widthStr = Integer.toString((int) (width * .75)) + "px";
    console.setWidth(widthStr);
    final CssLayout consoleLayout = new CssLayout();
    console.setContent(consoleLayout);
    mapContent.addComponent(console);
    mapContent.setSizeFull();
    googleMap.addMarkerClickListener(new MarkerClickListener() {
      @Override
      public void markerClicked(GoogleMapMarker clickedMarker) {
        Label consoleEntry = new Label("Mesh located at \""
            + clickedMarker.getCaption() + "\" at ("
            + clickedMarker.getPosition().getLat() + ", "
            + clickedMarker.getPosition().getLon() + ").\n");
        consoleLayout.addComponent(consoleEntry, 0);
      }
    });
    return mapContent;
  }

  /***
   * Creates the google map marker used to represent the location of a mesh
   * @param mesh - The Mesh entity to base the color and location of the marker off of.
   * @return GoogleMap Marker
   */
  private static GoogleMapMarker getMarker(Mesh mesh) {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "getMarker()");
    GoogleMapMarker marker;
    Location location = mesh.getLocation();
    if (mesh.getStatus() == null) {
      //place holder until ticket for non null mesh status is resolved and just return
      return new GoogleMapMarker(mesh.getMeshId() + ":NOT_AVAILABLE",
          new LatLon((double) location.getLatitude(),
              (double) location.getLongitude()),
          false);
    } else {
      switch (mesh.getStatus()) {
        case ACTIVE:
          marker = new GoogleMapMarker(mesh.getMeshId() + ":ACTIVE",
              new LatLon((double) location.getLatitude(),
                  (double) location.getLongitude()),
              false);
          //TODO: add marker icon graphic -- place holder graphic is not  copy right friendly --
          return marker;
        case INACTIVE:
          marker = new GoogleMapMarker(mesh.getMeshId() + ":INACTIVE",
              new LatLon((double) location.getLatitude(), (double) location.getLongitude()),
              false,
              "VAADIN/widgetsets/grey-marker.png");
          return marker;
        default:
          throw new RuntimeException("Invalid Mesh Status");
      }
    }
  }

  /**
   * @return a chart that displays the number of devices that are in each status type
   */
  public static ChartJs createMeshChart() {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createMeshChart()");
    return new MeshesChart(SystemController.getMeshHealthMap()).createChart();
  }

  /***
   *
   * @return GoogleMap Container to store google map
   */
  public static GoogleMap createGraphicalMeshMap() {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createGraphicalMeshMap()");
    Collection<Mesh> meshes = SystemController.getAllMeshes();
    GoogleMap googleMap = new GoogleMap(googleMapsApiKey, null, null);
    googleMap.setHeight("100%");
    double width = Page.getCurrent().getBrowserWindowWidth();
    String widthStr = Integer.toString((int) (width * .75)) + "px";
    googleMap.setWidth(widthStr);
    googleMap.setCenter(new LatLon(46.28267, -119.4273));
    googleMap.setZoom(8);
    for (Mesh mesh : meshes) {
      GoogleMapMarker marker = getMarker(mesh);
      googleMap.addMarker(marker);
    }
    return googleMap;
  }

  /**
   * Used as a place holder view until real history view is selected
   * @return Empty line chart
   */
  private static ChartJs createEmptyLineChart() {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createEmptyLineChart()");
    ChartJs chartJs = new ChartJs(new LineChartConfig());
    return chartJs;
  }

  /**
   * Prompts user to select a mesh id to display mesh history for -- after mesh is selected,
   * prompts user to select data range to view
   * * @return
   */
  public static VerticalLayout meshHistoryPrompt() {
    IdWrapper idWrapper = new IdWrapper();
    VerticalLayout verticalLayout = new VerticalLayout();
    ComboBox comboBox = createMeshHistoryComboBox();
    ComboBox dateRangeComboBox = DashboardController.createDateRangeComboBox();
    dateRangeComboBox.setReadOnly(true);
    //listens to check if user has selected a different date range to display history
    dateRangeComboBox.addValueChangeListener(event -> {
      if (idWrapper != null && idWrapper.id != null) {
        String dateRange = String.valueOf(event.getValue());
        Component prevComponent = verticalLayout.getComponent(1);
        if (prevComponent != null) {
          verticalLayout
              .replaceComponent(prevComponent, createMeshHistory(idWrapper.getId(),
                  dateRange));
        } else {
          verticalLayout
              .addComponent(createMeshHistory(idWrapper.getId(),
                  dateRange));
        }
      }
    });

    comboBox.addValueChangeListener(event -> {
      //change to only show
      dateRangeComboBox.setReadOnly(false);
      dateRangeComboBox.setPlaceholder(DateRange.DAY.getTextValue());
      String deviceId = String.valueOf(event.getValue());
      if (!deviceId.isEmpty()) {
        idWrapper.setId(deviceId);
        Component prevComponent = verticalLayout.getComponent(1);
        if (prevComponent != null) {
          verticalLayout
              .replaceComponent(prevComponent, DashboardController.createMeshHistory(deviceId,
                  DateRange.DAY.getTextValue()));
        } else {
          verticalLayout.addComponent(DashboardController.createMeshHistory(deviceId,
              DateRange.DAY.getTextValue()));
        }
      }
    });
    verticalLayout.addComponent(comboBox);
    verticalLayout
        .addComponent(DashboardController.createEmptyLineChart());
    verticalLayout.addComponent(dateRangeComboBox);
    verticalLayout.setSizeUndefined();
    return verticalLayout;
  }

  /**
   * Called by DashboardView to create deviceChart object for graphical display
   *
   * @return returns deviceChart object to caller
   */
  public static ChartJs createDevicesChart() {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createDevicesChart()");
    return new DevicesChart(SystemController.getDeviceHealthMap()).createChart();
  }

  private static class IdWrapper {
    private String id;
    public IdWrapper() {
      id = null;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }
  }

  /**Prompts the user to select the device and date range they are interested in retrieving
   * the view for.
   * @return
   */
  public static VerticalLayout deviceHistoryPrompt() {
    IdWrapper idWrapper = new IdWrapper();
    Optional<String> dateRangePlaceholder = Optional.empty();

    VerticalLayout verticalLayout = new VerticalLayout();
    ComboBox comboBox = createDeviceHistoryComboBox();
    ComboBox dateRangeComboBox = DashboardController.createDateRangeComboBox();
    dateRangeComboBox.setReadOnly(true);
    dateRangeComboBox.addValueChangeListener(event -> {
      if (idWrapper != null && idWrapper.id != null) {
        String dateRange = String.valueOf(event.getValue());
        Component prevComponent = verticalLayout.getComponent(1);
        if (prevComponent != null) {
          verticalLayout
              .replaceComponent(prevComponent, createDevicesHistory(idWrapper.getId(),
                  dateRange));
        } else {
          verticalLayout
              .addComponent(createDevicesHistory(idWrapper.getId(),
                  dateRange));
        }
      }
    });

    comboBox.addValueChangeListener(event -> {
      //change to only show
      dateRangeComboBox.setReadOnly(false);
      dateRangeComboBox.setPlaceholder(DateRange.DAY.getTextValue());
      String deviceId = String.valueOf(event.getValue());
      if (!deviceId.isEmpty()) {
        idWrapper.setId(deviceId);
        Component prevComponent = verticalLayout.getComponent(1);
        if (prevComponent != null) {
          verticalLayout
              .replaceComponent(prevComponent, DashboardController.createDevicesHistory(deviceId,
                  DateRange.DAY.getTextValue()));
        } else {
          dateRangeComboBox.setPlaceholder(DateRange.DAY.getTextValue());

          verticalLayout.addComponent(DashboardController.createDevicesHistory(deviceId,
              DateRange.DAY.getTextValue()));
        }
      }
    });

    verticalLayout.addComponent(comboBox);
    verticalLayout
        .addComponent(DashboardController.createEmptyLineChart());
    verticalLayout.addComponent(dateRangeComboBox);
    verticalLayout.setSizeUndefined();
    return verticalLayout;
  }

  /**
   * @return Combox box used to allow user to select which device they are interested in retrieving
   * the history view for
   */
  public static ComboBox createDeviceHistoryComboBox() {
    // Creates a new combobox using an existing container
    Collection<String> devices = SystemController.getAllDevices().stream()
        .map(device -> device.getDigitalId()).collect(
            Collectors.toList());
    ComboBox comboBox = new ComboBox<>("Select Device ID", devices);
    comboBox.setPlaceholder("No Device selected");
    comboBox.setEmptySelectionAllowed(false);
    // Set full width
    comboBox.setWidth(100.0f, Unit.PERCENTAGE);
    return comboBox;
  }

  /**
   *
   * Create the date range combo box used to allow users
   *
   * @param
   */
  public static ComboBox createDateRangeComboBox() {
    Collection<String> range = new ArrayList<>();
    for (final DateRange dateRange : DateRange.values()) {
      range.add(dateRange.getTextValue());
    }
    ComboBox comboBox = new ComboBox<>("Select history date range to display", range);
    comboBox.setPlaceholder("Date range not selected");
    comboBox.setEmptySelectionAllowed(false);
    // Set full width
    comboBox.setWidth(100.0f, Unit.PERCENTAGE);
    return comboBox;
  }

  /**
   * Called by DashboardView to create deviceHistory object for graphical display
   *
   * @return returns deviceHistory chart object to caller
   */
  public static ChartJs createDevicesHistory(String digitalId, String chartDateRange) {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createDevicesHistory()");
    Collection<DeviceHistoryEntry> deviceHistoryEntries = SystemController.getDeviceHistory(digitalId);
    ChartJs devHistChart;
    if (chartDateRange.equals(DateRange.DAY.getTextValue())) {
      devHistChart = new DeviceHistoryBubbleChart(deviceHistoryEntries, chartDateRange)
          .createChart();
    } else {
      devHistChart = new DeviceHistoryStackedChart(deviceHistoryEntries, chartDateRange)
          .createChart();
    }
    devHistChart.setHeight("500px");
    devHistChart.setWidth("700px");
    return devHistChart;
  }


  /**
   * Called by DashboardView to create deviceHistory object for graphical display
   *
   * @return returns deviceHistory chart object to caller
   */
  public static ChartJs createMeshHistory(String digitalId, String chartDateRange) {
    log.debug("{}: Entered {} method {}", LocalDateTime.now(), "DashboardController",
        "createDevicesHistory()");
    Collection<MeshHistoryEntry> meshHistoryEntries = SystemController
        .getMeshHistory(digitalId);
    ChartJs devHistChart;

    if (chartDateRange.equals(DateRange.DAY.getTextValue())) {
      devHistChart = new MeshHistoryBubbleChart(meshHistoryEntries, chartDateRange)
          .createChart();
    } else {
      devHistChart = new MeshHistoryStackedChart(meshHistoryEntries, chartDateRange).createChart();
    }
    devHistChart.setHeight("500px");
    devHistChart.setWidth("700px");
    return devHistChart;
  }

  /***
   * Displays a drop down list that allows users to type in or select mesh id associated with
   * desired history
   * @return history combo box
   */
  public static ComboBox createMeshHistoryComboBox() {
    // Creates a new combobox using an existing container
    Collection<String> meshes = SystemController.getAllMeshes().stream()
        .map(device -> device.getMeshId()).collect(
            Collectors.toList());
    ComboBox comboBox = new ComboBox<>("Select Mesh ID", meshes);
    comboBox.setPlaceholder("No Mesh selected");
    comboBox.setEmptySelectionAllowed(false);
    comboBox.setWidth(100.0f, Unit.PERCENTAGE);
    return comboBox;
  }
}