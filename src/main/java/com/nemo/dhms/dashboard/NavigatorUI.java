/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard;

import com.google.common.eventbus.Subscribe;
import com.nemo.dhms.dashboard.event.NavigatorEvent.BrowserResizeEvent;
import com.nemo.dhms.dashboard.event.NavigatorEvent.UserLoginRequestedEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.nemo.dhms.dashboard.model.User;
import com.nemo.dhms.dashboard.view.LoginView;
import com.nemo.dhms.dashboard.view.MainView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import java.util.Locale;

/**
 * Main UI -- Source: https://github.com/vaadin/dashboard-demo/blob/8.0/src/main/java/com/vaadin/demo/dashboard/DashboardUI.java
 */
@SpringUI
@Theme("valo")
@SuppressWarnings("serial")
@Widgetset("com.nemo.dhms.widgetset.SmartVineyardsWidgetSet")
public class NavigatorUI extends UI {

  public static final String MAINVIEW = "main";
  private final UserDataProvider dataProvider = new UserDataProvider();
  private NavigatorEventBus navigatorEventBus = new NavigatorEventBus();

  public static NavigatorEventBus getNavigatorEventbus() {
    return ((NavigatorUI) getCurrent()).navigatorEventBus;
  }

  /**
   * Used to get the real user from the dataProvider controller -
   *
   * @return An instance for accessing the (dummy) services layer.
   */
  public static UserDataProvider getDataProvider() {
    return ((NavigatorUI) getCurrent()).dataProvider;
  }

  @Override
  protected void init(VaadinRequest request) {
    setLocale(Locale.US);
    navigatorEventBus.register(this);
    Responsive.makeResponsive(this);

    //TODO: when user database exists - enforce auth by replacing  call to update content
    // with userLoginRequested(final UserLoginRequestedEvent event
    updateContent();

    // Some views need to be aware of browser resize events so a
    // BrowserResizeEvent gets fired to the event bus on every occasion.
    //Note: this is currently boiler plate code.
    Page.getCurrent().addBrowserWindowResizeListener(
        new BrowserWindowResizeListener() {
          @Override
          public void browserWindowResized(
              final BrowserWindowResizeEvent event) {
            NavigatorEventBus.post(new BrowserResizeEvent());
          }
        });
  }

  /**
   * Displays the correct view depending on if the user is logged in with valid privileges
   *
   * if no user is logged in --display the login view
   * else take the authenticated user to the main dashboard view
   */
  public void updateContent() {
    User user = (User) VaadinSession.getCurrent()
        .getAttribute(User.class.getName());

    //TODO: sv - Currently we do not have a  concept of non-admin users -- to restrict views based
    //on role - create a "non-admin-main-view" and
    if (user != null && "admin".equals(user.getRole())) {
      // Authenticated user  -- TODO -sv: add another view for non admin users
      setContent(new MainView());
      removeStyleName("loginview");
      getNavigator().navigateTo(getNavigator().getState());
    } else {              //else if user != null && "non-admin".equals(user.getRole())
      setContent(new LoginView());
      addStyleName("loginview");
    }
  }

  /**
   * Subsriber listens for UserLoginRequestedEvent sent by the LoginView that is called
   * on application start up (or after sign-out once sign out feature is implemented)
   * @param event
   */
  @Subscribe
  public void userLoginRequested(final UserLoginRequestedEvent event) {
    User user = getDataProvider().authenticate(event.getUserName(),
        event.getPassword());
    VaadinSession.getCurrent().setAttribute(User.class.getName(), user);
    updateContent();
  }
}