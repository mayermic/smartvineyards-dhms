/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard;

/**
 *
 *
 * Used as a placeholder until we create a database to store real user information
 * Can be deleted once database is created to store user model.
 * Source: https://github.com/vaadin/dashboard-demo/blob/8.0/src/main/java/com/vaadin/demo/dashboard/data/dummy/DummyDataGenerator.java
 */
public class DummyUserDataGenerator {
  static String randomFirstName() {
    String[] names = {"Dave", "Mike", "Katherine", "Jonas", "Linus",
        "Bob", "Anne", "Minna", "Elisa", "George", "Mathias", "Pekka",
        "Fredrik", "Kate", "Teppo", "Kim", "Samatha", "Sam", "Linda",
        "Jo", "Sarah", "Ray", "Michael", "Steve"};
    return names[(int) (Math.random() * names.length)];
  }

  static String randomLastName() {
    String[] names = {"Smith", "Lehtinen", "Chandler", "Hewlett",
        "Packard", "Jobs", "Buffet", "Reagan", "Carthy", "Wu",
        "Johnson", "Williams", "Jones", "Brown", "Davis", "Moore",
        "Wilson", "Taylor", "Anderson", "Jackson", "White", "Harris",
        "Martin", "King", "Lee", "Walker", "Wright", "Clark",
        "Robinson", "Garcia", "Thomas", "Hall", "Lopez", "Scott",
        "Adams", "Barker", "Morris", "Cook", "Rogers", "Rivera",
        "Gray", "Price", "Perry", "Powell", "Russell", "Diaz"};
    return names[(int) (Math.random() * names.length)];
  }

  static String randomCompanyName() {

    String name = randomName();
    if (Math.random() < 0.03) {
      name += " Technologies";
    } else if (Math.random() < 0.02) {
      name += " Investment";
    }
    if (Math.random() < 0.3) {
      name += " Inc";
    } else if (Math.random() < 0.2) {
      name += " Ltd.";
    }

    return name;
  }

  static String randomName() {
    int len = (int) (Math.random() * 4) + 1;
    return randomWord(len, true);
  }

  public static String randomWord(int len, boolean capitalized) {
    String[] part = {"ger", "ma", "isa", "app", "le", "ni", "ke", "mic",
        "ro", "soft", "wa", "re", "lo", "gi", "is", "acc", "el", "tes",
        "la", "ko", "ni", "ka", "so", "ny", "mi", "nol", "ta", "pa",
        "na", "so", "nic", "sa", "les", "for", "ce"};
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < len; i++) {
      String p = part[(int) (Math.random() * part.length)];
      if (i == 0 && capitalized) {
        p = Character.toUpperCase(p.charAt(0)) + p.substring(1);
      }
      sb.append(p);
    }
    return sb.toString();
  }
}
