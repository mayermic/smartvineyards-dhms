/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.nemo.dhms.dashboard.view;

import com.google.common.eventbus.Subscribe;
import com.nemo.dhms.dashboard.NavigationViewType;
import com.nemo.dhms.dashboard.event.NavigatorEvent.PostViewChangeEvent;
import com.nemo.dhms.dashboard.event.NavigatorEvent.ProfileUpdatedEvent;
import com.nemo.dhms.dashboard.event.NavigatorEvent.UserLoggedOutEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.nemo.dhms.dashboard.model.User;
import com.nemo.dhms.system.SystemController;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Source: https://github.com/vaadin/dashboard-demo/blob/8.0/src/main/java/com/vaadin/demo/dashboard/view/DashboardMenu.java
 *
 * A responsive menu component providing user information and the controls for primary navigation
 * between the views.
 */
@SuppressWarnings({"serial", "unchecked"})
public final class DashboardMenu extends CustomComponent {

  public static final String ID = "dashboard-menu";
  public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
  private static final String STYLE_VISIBLE = "valo-menu-visible";
  private Label notificationsBadge;
  private MenuItem settingsItem;

  public DashboardMenu() {
    setPrimaryStyleName("valo-menu");
    setId(ID);
    double height = Page.getCurrent().getBrowserWindowHeight();
    String heightStr = Integer.toString((int) (height * .90)) + "px";
    setHeight(heightStr);

    // There's only one DashboardMenu per UI so this doesn't need to be
    // unregistered from the UI-scoped DashboardEventBus.
    NavigatorEventBus.register(this);

    setCompositionRoot(buildContent());
  }

  private Component buildContent() {
    final CssLayout menuContent = new CssLayout();
    menuContent.addStyleName("sidebar");
    menuContent.addStyleName(ValoTheme.MENU_PART);
    menuContent.addStyleName("no-vertical-drag-hints");
    menuContent.addStyleName("no-horizontal-drag-hints");
    menuContent.setWidth(null);

    menuContent.addComponent(buildTitle());

    if (!SystemController.getJiraHTTP200Status()) {
      menuContent.addComponent(buildJiraIndicator());
    }
//    menuContent.addComponent(buildUserMenu());  //TODO sv: remove or keep depending on auth req.
    menuContent.addComponent(buildToggleButton());
    menuContent.addComponent(buildMenuItems());
    return menuContent;
  }

  private Component buildTitle() {
    Label logo = new Label("SmartVineyards <strong>Dashboard</strong>",
        ContentMode.HTML);
    logo.setSizeUndefined();
    HorizontalLayout logoWrapper = new HorizontalLayout(logo);
    logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
    logoWrapper.addStyleName("valo-menu-title");
    logoWrapper.setSpacing(false);
    return logoWrapper;
  }


  private Component buildJiraIndicator() {
    CssLayout menuItemsLayout = new CssLayout();
    //place holder logic until can figure out how to use plugin to create vaadin/theme directory
    Label label = new Label(
        String.format("<font size = \\\"5\\\" style=padding-left:2em color=\\\"white\\\"> "
            + "<b>Jira Status: Down</b>"), ContentMode.HTML);
    menuItemsLayout.addComponent(label);
    return menuItemsLayout;
  }


  private User getCurrentUser() {
    return (User) VaadinSession.getCurrent()
        .getAttribute(User.class.getName());
  }


  /**
   * Displays custom user title icon that displays user info on menu -- adds ability to log out
   **/
  private Component buildUserMenu() {
    final MenuBar settings = new MenuBar();
    settings.addStyleName("user-menu");
    final User user = getCurrentUser();
    settingsItem = settings.addItem("", null);
    updateUserName(null);
    settingsItem.addSeparator();
    settingsItem.addItem("Sign Out", new Command() {
      @Override
      public void menuSelected(final MenuItem selectedItem) {
        NavigatorEventBus.post(new UserLoggedOutEvent());
      }
    });
    return settings;
  }

  private Component buildToggleButton() {
    Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
      @Override
      public void buttonClick(final ClickEvent event) {
        if (getCompositionRoot().getStyleName()
            .contains(STYLE_VISIBLE)) {
          getCompositionRoot().removeStyleName(STYLE_VISIBLE);
        } else {
          getCompositionRoot().addStyleName(STYLE_VISIBLE);
        }
      }
    });
    valoMenuToggleButton.addStyleName("valo-menu-toggle");
    valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
    return valoMenuToggleButton;
  }

  private Component buildMenuItems() {
    CssLayout menuItemsLayout = new CssLayout();
    menuItemsLayout.addStyleName("valo-menuitems");

    for (final NavigationViewType view : NavigationViewType.values()) {
      Component menuItemComponent = new ValoMenuItemButton(view);

      if (view == NavigationViewType.DASHBOARD) {
        notificationsBadge = new Label();
        notificationsBadge.setId(NOTIFICATIONS_BADGE_ID);
        menuItemComponent = buildBadgeWrapper(menuItemComponent,
            notificationsBadge);
      }

      menuItemsLayout.addComponent(menuItemComponent);
    }
    return menuItemsLayout;
  }

  private Component buildBadgeWrapper(final Component menuItemButton,
      final Component badgeLabel) {
    CssLayout dashboardWrapper = new CssLayout(menuItemButton);
    dashboardWrapper.addStyleName("badgewrapper");
    dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
    badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
    badgeLabel.setWidthUndefined();
    badgeLabel.setVisible(false);
    dashboardWrapper.addComponent(badgeLabel);
    return dashboardWrapper;
  }

  @Subscribe
  public void postViewChange(final PostViewChangeEvent event) {
    // After a successful view change the menu can be hidden in mobile view.
    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
  }

  @Subscribe
  public void updateUserName(final ProfileUpdatedEvent event) {
    User user = getCurrentUser();
    settingsItem.setText(user.getFirstName() + " " + user.getLastName());
  }

  public final class ValoMenuItemButton extends Button {

    private static final String STYLE_SELECTED = "selected";

    private final NavigationViewType view;

    public ValoMenuItemButton(final NavigationViewType view) {
      this.view = view;
      setPrimaryStyleName("valo-menu-item");
      setCaption(view.getViewName().substring(0, 1).toUpperCase()
          + view.getViewName().substring(1));
      NavigatorEventBus.register(this);
      addClickListener(new ClickListener() {
        @Override
        public void buttonClick(final ClickEvent event) {
          UI.getCurrent().getNavigator()
              .navigateTo(view.getViewName());
        }
      });
    }

    @Subscribe
    public void postViewChange(final PostViewChangeEvent event) {
      removeStyleName(STYLE_SELECTED);
      if (event.getView() == view) {
        addStyleName(STYLE_SELECTED);
      }
    }
  }
}