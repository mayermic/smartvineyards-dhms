/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.view;

import com.nemo.dhms.dashboard.event.NavigatorEvent.CloseOpenWindowsEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.nemo.dhms.dashboard.form.ConfigurationForm;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * View to load configuration page
 */
public class ConfigurationView extends Panel implements View {

  private final VerticalLayout root;
  private CssLayout dashboardPanels;
  private ConfigurationForm configurationForm = new ConfigurationForm(this);

  public ConfigurationView() {
    addStyleName(ValoTheme.PANEL_BORDERLESS);
    NavigatorEventBus.register(this);

    root = new VerticalLayout();

    root.setSizeUndefined();
    root.setSpacing(false);
    root.addStyleName("configuration-view");
    setContent(root);

    Component content = buildContent();
    root.addComponent(content);
    root.setExpandRatio(content, 1);

    // All the open sub-windows should be closed whenever the root layout
    // gets clicked.
    root.addLayoutClickListener(new LayoutClickListener() {
      @Override
      public void layoutClick(final LayoutClickEvent event) {
        NavigatorEventBus.post(new CloseOpenWindowsEvent());
      }
    });

    Responsive.makeResponsive(root);
  }

  private Component buildContent() {
    dashboardPanels = new CssLayout();
    dashboardPanels.addStyleName("dashboard-panels");
    Responsive.makeResponsive(dashboardPanels);

    dashboardPanels.addComponent(getContentLayout());
    return dashboardPanels;
  }

  public Component getContentLayout() {
    HorizontalLayout rootLayout = new HorizontalLayout();

    rootLayout.addComponent(configurationForm);

    rootLayout.setSizeFull();

    Panel panelM = new Panel();
    panelM.setContent(rootLayout);
    Component panel = createContentWrapper(panelM);
    panel.addStyleName("content");

    return panel;
  }

  private Component createContentWrapper(final Component content) {
    final CssLayout slot = new CssLayout();
    slot.setWidth("100%");
    slot.addStyleName("dashboard-panel-slot");

    CssLayout card = new CssLayout();
    card.setWidth("100%");
    card.addStyleName(ValoTheme.LAYOUT_CARD);

    Label caption = new Label(content.getCaption());
    caption.addStyleName(ValoTheme.LABEL_H4);
    caption.addStyleName(ValoTheme.LABEL_COLORED);
    caption.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    content.setCaption(null);
    card.addComponent(content);
    slot.addComponent(card);
    return slot;
  }
}
