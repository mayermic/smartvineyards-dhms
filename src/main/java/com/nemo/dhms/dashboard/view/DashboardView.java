/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.view;

import static com.nemo.dhms.dashboard.controllers.DashboardController.deviceHistoryPrompt;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.nemo.dhms.dashboard.controllers.DashboardController;
import com.nemo.dhms.dashboard.event.NavigatorEvent.CloseOpenWindowsEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Dashboard UI class creates Mesh, Devices, Mesh History, and Devices History charts to be
 * displayed as chart data for the user
 */

@SuppressWarnings("serial")
public final class DashboardView extends Panel implements View {

  private final VerticalLayout root;
  private CssLayout dashboardPanels;

  public DashboardView() {
    addStyleName(ValoTheme.PANEL_BORDERLESS);
    setSizeUndefined();
    NavigatorEventBus.register(this);

    root = new VerticalLayout();

    root.setSizeUndefined();
    root.setSpacing(false);
    root.addStyleName("dashboard-view");
    setContent(root);

    Component content = buildContent();

    root.addComponent(content);
    root.addLayoutClickListener(new LayoutClickListener() {
      @Override
      public void layoutClick(final LayoutClickEvent event) {
        NavigatorEventBus.post(new CloseOpenWindowsEvent());
      }
    });
    Responsive.makeResponsive(root);
  }

  /**
   * Creates a Mesh Chart using a DashboardController object
   *
   * @return - returns a Mesh chart object
   */
  private static ChartJs meshChart() {
    return DashboardController.createMeshChart();
  }

  private static VerticalLayout meshHistoryChart(){
    return DashboardController.meshHistoryPrompt();
  }
  /**
   * Creates a Devices Chart using a DashboardController object
   *
   * @return - returns a devicesChart object
   */
  private static ChartJs devicesChart() {
    return DashboardController.createDevicesChart();
  }

  private static VerticalLayout devicesHistoryChart(){
    return DashboardController.deviceHistoryPrompt();
  }
  private static VerticalLayout meshHistoryPrompt() {
    return DashboardController.meshHistoryPrompt();
  }

  private static VerticalLayout googleMapVerticalLayout() {
    return DashboardController.googleMapVerticalLayout();
  }

  private Component buildContent() {
    dashboardPanels = new CssLayout();
    dashboardPanels.addStyleName("dashboard-panels");
    dashboardPanels.addComponent(testPanelLayoutComponent());

    return dashboardPanels;
  }


  public Component testPanelLayoutComponent() {
    Panel panelWrapper = new Panel();
    VerticalLayout rootLayout = new VerticalLayout();
    Panel innerPanel = test_panel();
    double height = Page.getCurrent().getBrowserWindowHeight();
    String heightStr = Integer.toString((int) (height * .90)) + "px";
    innerPanel.setHeight(heightStr);
    rootLayout.addComponent(innerPanel);
    panelWrapper.setContent(rootLayout);
    Component component = createContentWrapper(panelWrapper);
    component.addStyleName("wrapper");
    return component;
  }

  /**
   * Builds dashboard view content
   */
  public Component getContentLayout() {
    // Create the content root layout for the UI
    Panel panelM = new Panel();
    VerticalLayout rootLaylout = new VerticalLayout();
    GridLayout gridLayout = new GridLayout(4, 2);

    //Pie Charts
    gridLayout.addComponent(meshHistoryPrompt(), 0, 0);
    gridLayout.addComponent(meshChart(), 1, 0);
    gridLayout.addComponent(devicesChart(), 2, 0);
    gridLayout.addComponent(deviceHistoryPrompt(), 3, 0);
    gridLayout.setSpacing(true);
    gridLayout.setSizeUndefined();

    rootLaylout.addComponent(googleMapVerticalLayout());
    rootLaylout.addComponent(gridLayout);
    rootLaylout.setSpacing(true);
    rootLaylout.setSizeUndefined();
    panelM.setContent(rootLaylout);
    Component panel = createContentWrapper(panelM);
    panel.addStyleName("notes");
    return panel;
  }

  private Component createContentWrapper(final Component content) {
    final CssLayout slot = new CssLayout();
    slot.setWidth("100%");
    slot.addStyleName("dashboard-panel-slot");

    CssLayout card = new CssLayout();
    card.setWidth("100%");
    card.addStyleName(ValoTheme.LAYOUT_CARD);

    Label caption = new Label(content.getCaption());
    caption.addStyleName(ValoTheme.LABEL_H4);
    caption.addStyleName(ValoTheme.LABEL_COLORED);
    caption.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    content.setCaption(null);
    card.addComponents(content);
    slot.addComponent(card);
    return slot;
  }


  //Dev note: cannot make tabs have an undefined size - it breaks google maps.
  //This implies limitations to how small a screen can be
  public Panel test_panel() {
    Panel panelViews = new Panel("View Panel");
    TabSheet tabs = dashboardTabs();
    tabs.setSizeUndefined();
    double width = Page.getCurrent().getBrowserWindowWidth();
    String widthStr = Integer.toString((int) (width * .80)) + "px";
    panelViews.setContent(tabs);
    panelViews.setWidth(widthStr);
    return panelViews;
  }

  /**
   * Tabsheet method to format UI element for DashboardUI
   *
   * @return Tabsheet object with the correct format
   */
    private TabSheet dashboardTabs() {
    TabSheet tabsheet = new TabSheet();
    //Create Tab Sheet Dynamically
    tabsheet.addSelectedTabChangeListener(
        new TabSheet.SelectedTabChangeListener() {
          @Override
          public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
            //Find the dashboardTabs
            TabSheet tabsheet = event.getTabSheet();

            //Find the tab
            Layout tab = (Layout) tabsheet.getSelectedTab();

            //Get the tab caption from the tab object
            String caption = tabsheet.getTab(tab).getCaption();

            //Fill the tab content
            tab.removeAllComponents();
            //Add the Proper content
            if (caption.equals("Devices")) {
              HorizontalLayout devicesHL = new HorizontalLayout();
              devicesHL.addComponent(devicesChart());
              devicesHL.addComponent(devicesHistoryChart());
              tab.addComponent(devicesHL);
            } else if (caption.equals("Meshes")) {

              HorizontalLayout meshesHL = new HorizontalLayout();
              meshesHL.addComponent(meshChart());
              meshesHL.addComponent(meshHistoryChart());
              tab.addComponent(meshesHL);
            } else if (caption.equals("Map")) {
              tab.addComponent(googleMapVerticalLayout());
            } else {
              tab.addComponent(new Panel("The content you requested is not here"));
            }
          }
        }
    );

    //For the captions
    String[] tabs = {"Devices", "Meshes", "Map"};
    for (String caption : tabs) {
      tabsheet.addTab(new VerticalLayout(), caption);
    }
    return tabsheet;
  }

  /**
   * Method to return all UI elements in the Dashboard
   *
   * @return GridLayout object containing the summary
   */
  private GridLayout Summary() {
    GridLayout gridLayout = new GridLayout(3, 2);
    gridLayout.addComponent(meshHistoryPrompt(), 0, 0);
    gridLayout.addComponent(meshChart(), 1, 0);
    gridLayout.addComponent(devicesChart(), 2, 0);
    gridLayout.setSpacing(true);
    gridLayout.setResponsive(true);
    return gridLayout;
  }
}
