/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.dashboard.view;

import com.nemo.dhms.dashboard.event.NavigatorEvent.UserLoginRequestedEvent;
import com.nemo.dhms.dashboard.event.NavigatorEventBus;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * Login view that allows users to enter system login information: username and password
 */
public class LoginView extends VerticalLayout {

  public LoginView() {
    setSizeFull();
    setMargin(false);
    setSpacing(false);
    Component loginForm = buildLoginForm();

    addComponent(loginForm);
    setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
  }

  private Component buildLoginForm() {

    final VerticalLayout rootLayout = new VerticalLayout();
    rootLayout.setSizeUndefined();
    Responsive.makeResponsive(rootLayout);
    rootLayout.addStyleName("login-panel");

    FormLayout content = new FormLayout();
    TextField userName = new TextField("Username");
    rootLayout.addComponent(userName);
    PasswordField password = new PasswordField("Password");
    rootLayout.addComponent(password);

    Button submit = new Button("submit", new Button.ClickListener() {
      @Override
      public void buttonClick(Button.ClickEvent event) {
        NavigatorEventBus.post(new UserLoginRequestedEvent(userName
            .getValue(), password.getValue()));

        //TODO: Add authentication logic to make sure its a valid user -- NavigatorEvent p
      }
    });

    rootLayout.addComponent(submit);
    return rootLayout;
  }
}