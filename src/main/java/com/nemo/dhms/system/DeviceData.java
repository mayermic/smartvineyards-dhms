package com.nemo.dhms.system;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

/**
 * DeviceData Class: Purpose of class is store Device data consisting of digitalID, timestamp, and
 * the sensor values in memory
 */
public class DeviceData {

  private String digitalId;
  private LocalDateTime timestamp;
  private Map<String, String> sensorValues;

  public DeviceData(String digitalId,
      LocalDateTime timestamp,
      Map<String, String> sensorValues) {
    this.digitalId = digitalId;
    this.timestamp = timestamp;
    this.sensorValues = sensorValues;
  }

  public String getDigitalId() {
    return digitalId;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public Map<String, String> getSensorValues() {
    return Collections.unmodifiableMap(sensorValues);
  }

  public String getSensorValue(String type) {
    return sensorValues.get(type);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceData that = (DeviceData) o;
    return Objects.equal(digitalId, that.digitalId) &&
        Objects.equal(timestamp, that.timestamp) &&
        Objects.equal(sensorValues, that.sensorValues);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(digitalId, timestamp, sensorValues);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("digitalId", digitalId)
        .add("timestamp", timestamp)
        .add("sensorValues", sensorValues)
        .toString();
  }
}
