package com.nemo.dhms.system;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class MeshHistoryKey implements Serializable {

  private String mesh;
  private LocalDateTime date;

  @Override
  public final boolean equals(Object other) {
    if (this == other) {
      return true;
    }

    if (other == null) {
      return false;
    }

    if (!(other instanceof MeshHistoryKey)) {
      return false;
    }

    MeshHistoryKey otherKey = (MeshHistoryKey) other;
    return mesh.equals(otherKey.mesh) && date.equals(otherKey.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mesh, date);
  }
}
