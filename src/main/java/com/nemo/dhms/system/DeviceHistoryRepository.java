package com.nemo.dhms.system;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceHistoryRepository extends
    CrudRepository<DeviceHistoryEntry, DeviceHistoryKey> {

  Optional<DeviceHistoryEntry> findByDeviceAndDate(String deviceId, LocalDateTime date);

  List<DeviceHistoryEntry> findAllByDevice(String deviceId);
}
