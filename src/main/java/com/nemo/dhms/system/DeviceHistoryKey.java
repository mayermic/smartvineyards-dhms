package com.nemo.dhms.system;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class DeviceHistoryKey implements Serializable {

  private String device;
  private LocalDateTime date;

  @Override
  public final boolean equals(Object other) {
    if (this == other) {
      return true;
    }

    if (other == null) {
      return false;
    }

    if (!(other instanceof DeviceHistoryKey)) {
      return false;
    }

    DeviceHistoryKey otherKey = (DeviceHistoryKey) other;
    return device.equals(otherKey.device) && date.equals(otherKey.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(device, date);
  }
}
