/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

import com.nemo.dhms.data.DataController;
import com.nemo.dhms.issues.JiraController;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import net.rcarz.jiraclient.JiraException;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * SystemController Class: Purpose of this class is to facilitate the transactions between the
 * different modules of this program
 */
@Component
public class SystemController {

  private static final Logger log = LoggerFactory.getLogger(SystemController.class);

  private static Collection<Mesh> meshes = new HashSet<>();
  private static Collection<Mesh> tempMeshes = new HashSet<>();
  private static Collection<Device> devices = new HashSet<>();
  private static Collection<Device> tempDevices = new HashSet<>();

  private SystemController() {

  }

  public static void init() {
    LocalDateTime updateTime = LocalDateTime.now();

    // Get all new devices
    if (!tempDevices.isEmpty()) {
      tempDevices.clear();
    }
    tempDevices = DataController.getAllDevices();
    for (Device tempDevice : tempDevices) {
      if (devices.contains(tempDevice)) {
        tempDevices.remove(tempDevice);
      }
    }
    log.info("init(): " + tempDevices.size() + " devices");
    // Add new devices history to Collection
    for (Device device : tempDevices) {
      device.setLastSystemUpdate(updateTime);
      device.setHistory(parseDeviceHistory(device.getDigitalId()));
      devices.add(device);
    }
    // Remove all devices from temp
    tempDevices.clear();

    updateTime = LocalDateTime.now();

    // Get all new meshes
    if (!tempMeshes.isEmpty()) {
      tempMeshes.clear();
    }
    tempMeshes = DataController.getAllMeshes();
    for (Mesh tempMesh : tempMeshes) {
      if (meshes.contains(tempMesh)) {
        tempMeshes.remove(tempMesh);
      }
    }
    log.info("init(): " + tempMeshes.size() + " meshes");
    // Add new devices history to Collection
    for (Mesh mesh : tempMeshes) {
      mesh.setLastSystemUpdate(updateTime);
      List<MeshHistoryEntry> initHistories = new ArrayList<>();
      if(mesh.getStatus() != null){
        initHistories.add(new MeshHistoryEntry(mesh.getMeshId(),LocalDateTime.now(), mesh.getStatus()));
      } else {
        initHistories.add(new MeshHistoryEntry(mesh.getMeshId(),LocalDateTime.now(), MeshStatus.FAILURE));
      }
      mesh.setHistory(initHistories);
      //mesh.setHistory(parseMeshHistory(mesh.getMeshId()));
      meshes.add(mesh);
    }
    // Remove all meshes from temp
    tempMeshes.clear();
  }

  /**
   * Update device and mesh data
   */
  @Scheduled(fixedRate = 60000) // Run every 1 minutes
  public static void update() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController", "update()");

    log.info("Update at " + LocalDateTime.now());

    int updatedMeshes = 0;
    int updatedDevices = 0;
    int meshesDown = 0;
    int devicesDown = 0;

    for (Mesh mesh : meshes) {
      // If mesh hasn't been updated in over 30 minutes
      if (LocalDateTime.now().isAfter(mesh.getLastSystemUpdate().plusMinutes(30))) {
        updatedMeshes++;
        mesh.setLastSystemUpdate(LocalDateTime.now());
        Mesh updatedMesh = DataController.getMesh(mesh.getMeshId());
        List<MeshHistoryEntry> meshHistory;

        if (updatedMesh == null) {
          continue;
          //TODO: Handle null pointer
        }

        int size = mesh.getHistory().size();
        // Only need most recent 6 to detect failure
        if (size >= 7) {
          meshHistory = mesh.getHistory()
              .subList(size - 7, size);
        } else {
          meshHistory = mesh.getHistory();
        }

        int meshHistorySize = meshHistory.size();

        // No new data
        if (updatedMesh.getLastDataTime() == null ||
            !updatedMesh.getLastDataTime().isAfter(mesh.getLastDataTime())) {
          if (meshHistorySize == 0 || meshHistory.get(meshHistorySize - 1).getStatus().toString().equals("idle")) {
            meshesDown++;
            mesh.setStatus(MeshStatus.IDLE);
            mesh.addHistory(
                new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.IDLE));
          } else if (meshHistory.get(meshHistorySize - 1).getStatus().toString()
              .equals("warning")) {
            int warnings = 1;
            for (int j = 2; j <= meshHistorySize; j++) {
              if (meshHistory.get(meshHistorySize - j).getStatus().toString().equals("warning")) {
                warnings++;
              }
            }
            // No new data + prev up to 3 warnings = warning
            if (warnings <= 3) {
              meshesDown++;
              mesh.setStatus(MeshStatus.WARNING);
              mesh.addHistory(
                  new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.WARNING));
            } else {
              // No new data + more than 3 prev warnings = failure
              meshesDown++;
              mesh.setStatus(MeshStatus.FAILURE);
              mesh.addHistory(
                  new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.FAILURE));
            }
          } else if (meshHistory.get(meshHistorySize - 1).getStatus().toString()
              .equals("failure")) {
            // No new data + prev failure = failure
            meshesDown++;
            mesh.setStatus(MeshStatus.FAILURE);
            mesh.addHistory(
                new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.FAILURE));
          } else {
            // inactives set to 1 because no new data from mesh, then look in history and
            // count the number of inactives. If it encounters other status besides
            // inactives (active) then break.
            int inactives = 1;
            for (int k = 1; k <= meshHistorySize; k++) {
              if (meshHistory.get(meshHistorySize - k).getStatus().toString().equals("inactive")) {
                inactives++;
              } else {
                break;
              }
            }
            // These 3 conditions are for when the data initially gets pulled from Sensmit API which
            // there are only "active" and "inactive" status. Count the number of inactives and
            // determine the status.
            if (inactives == 1) {
              mesh.setStatus(MeshStatus.INACTIVE);
              mesh.addHistory(
                  new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.INACTIVE));
              meshesDown++;
            } else if (inactives >= 2 && inactives <= 5) {
              mesh.setStatus(MeshStatus.WARNING);
              mesh.addHistory(
                  new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.WARNING));
              meshesDown++;
            } else {
              mesh.setStatus(MeshStatus.FAILURE);
              mesh.addHistory(
                  new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.FAILURE));
              meshesDown++;
            }
          }
        } else {
          // New data from API
          mesh.setStatus(MeshStatus.ACTIVE);
          mesh.addHistory(
              new MeshHistoryEntry(mesh.getMeshId(), LocalDateTime.now(), MeshStatus.ACTIVE));
        }
      }
    }

    for (Device device : devices) {
      // If device hasn't been updated in over 30 minutes
      if (LocalDateTime.now().isAfter(device.getLastSystemUpdate().plusMinutes(30))) {
        updatedDevices++;
        device.setLastSystemUpdate(LocalDateTime.now());
        Device updatedDevice = DataController.getDevice(device.getDigitalId());
        List<DeviceHistoryEntry> deviceHistory;

        if (updatedDevice == null) {
          continue;
          //TODO: Handle null pointer
        }

        int size = device.getHistory().size();
        // Only need most recent 6 to detect failure
        if (size >= 7) {
          deviceHistory = device.getHistory()
              .subList(size - 7, size);
        } else {
          deviceHistory = device.getHistory();
        }
        int deviceHistorySize = deviceHistory.size();

        // No new data
        if (updatedDevice.getLastCaptureTime() == null ||
            !updatedDevice.getLastCaptureTime().isAfter(device.getLastCaptureTime())) {
          if (deviceHistory.get(deviceHistorySize - 1).getStatus().toString().equals("idle")) {
            devicesDown++;
            device.setStatus(DeviceStatus.IDLE);
            device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                DeviceStatus.IDLE));
          } else if (deviceHistory.get(deviceHistorySize - 1).getStatus().toString()
              .equals("warning")) {
            int warnings = 1;
            for (int j = 2; j <= deviceHistorySize; j++) {
              if (deviceHistory.get(deviceHistorySize - j).getStatus().toString()
                  .equals("warning")) {
                warnings++;
              }
            }
            // No new data + prev up to 3 warnings = warning
            if (warnings <= 3) {
              devicesDown++;
              device.setStatus(DeviceStatus.WARNING);
              device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                  DeviceStatus.WARNING));
            } else {
              // No new data + more than 3 prev warnings = failure
              devicesDown++;
              device.setStatus(DeviceStatus.FAILURE);
              device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                  DeviceStatus.FAILURE));
            }
          } else if (deviceHistory.get(deviceHistorySize - 1).getStatus().toString()
              .equals("failure")) {
            // No new data + prev failure = failure
            devicesDown++;
            device.setStatus(DeviceStatus.FAILURE);
            device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                DeviceStatus.FAILURE));
          } else {
            // inactives set to 1 because no new data from device, then look in history and
            // count the number of inactives. If it encounters other status besides
            // inactives (active) then break.
            int inactives = 1;
            for (int k = 1; k <= deviceHistorySize; k++) {
              if (deviceHistory.get(deviceHistorySize - k).getStatus().toString()
                  .equals("inactive")) {
                inactives++;
              } else {
                break;
              }
            }
            // These 3 conditions are for when the data initially gets pulled from Sensmit API which
            // there are only "active" and "inactive" status. Count the number of inactives and
            // determine the status.
            if (inactives == 1) {
              device.setStatus(DeviceStatus.INACTIVE);
              device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                  DeviceStatus.INACTIVE));
              devicesDown++;
            } else if (inactives >= 2 && inactives <= 5) {
              device.setStatus(DeviceStatus.WARNING);
              device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                  DeviceStatus.WARNING));
              devicesDown++;
            } else {
              device.setStatus(DeviceStatus.FAILURE);
              device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
                  DeviceStatus.FAILURE));
              devicesDown++;
            }
          }
        } else {
          // New data from API
          device.setStatus(DeviceStatus.ACTIVE);
          device.addHistory(new DeviceHistoryEntry(device.getDigitalId(), LocalDateTime.now(),
              DeviceStatus.ACTIVE));
        }
      }
    }

    parseForIssuesForDevices(devices); //For Ticket Creation
    log.info("Meshes updated: " + updatedMeshes);
    log.info("Meshes down: " + meshesDown);
    log.info("Devices updated: " + updatedDevices);
    log.info("Devices down: " + devicesDown);
  }

  /**
   * Method to return a list of DeviceIds
   *
   * @return Collection object of Strings containing DeviceIDs
   */
  public static Collection<String> getDigitalIds() {
    ArrayList<String> digitalIds = new ArrayList<>();

    for (Device device : devices) {
      digitalIds.add(device.getDigitalId());
    }

    return digitalIds;
  }

  /**
   * Method to return a unique list of MeshIds
   *
   * @return Collection object of String containing a list of MeshIds
   */
  public static Collection<String> getMeshIds() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController", "getMeshIds()");
    ArrayList<String> meshIds = new ArrayList<>();

    for (Mesh mesh : meshes) {
      meshIds.add(mesh.getMeshId());
    }

    return meshIds;
  }

  /**
   * Method to return the list of Device data for the passed in Mesh ID
   *
   * @return Collection list object of Devices associated with the MeshID
   */
  public static Collection<Device> getAllDevices() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController",
    //"getAllDevices()");
    return Collections.unmodifiableCollection(devices);
  }

  /**
   * Method to return all Meshes in a Collection
   *
   * @return ArrayList object of Meshes
   */
  public static Collection<Mesh> getAllMeshes() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController",
    //"getAllMeshes()");
    return Collections.unmodifiableCollection(meshes);
  }

  /**
   * Method to return the data of Device string passed in
   *
   * @param deviceId String : DigitalID of the device
   * @return Device object containing data matching the deviceID passed in
   */
  public static Device getDevice(String deviceId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "deviceId()", "deviceId", deviceId);
    for (Device device : devices) {
      if (device.getDigitalId().contentEquals(deviceId)) {
        return new Device(device);
      }
    }

    return null;
  }

  /**
   * Method the retrieve the Mesh information for a specific MeshID that is passed in
   *
   * @param meshId String value that contains the specific Mesh ID requested
   * @return Mesh object containing the associated information for the Mesh passed in
   */
  public static Mesh getMesh(String meshId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "meshId()", "meshId", meshId);
    for (Mesh mesh : meshes) {
      if (mesh.getMeshId().contentEquals(meshId)) {
        return new Mesh(mesh);
      }
    }

    return null;
  }

  /**
   * Method to return the DeviceStatus of the passed in deviceID String
   *
   * @param deviceId String value for the requested device
   * @return DeviceStatus object with the passed in deviceID's status
   */
  public static DeviceStatus getDeviceStatus(String deviceId) {
    //TODO Review if this Works
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //Object.class.getClass(), Object.class.getEnclosingMethod(), "deviceId", deviceId);
    Device device = getDevice(deviceId);

    if (device != null) {
      return device.getStatus();
    } else {
      return null;
    }
  }

  /**
   * Method the return the Status of the Mesh
   *
   * @param meshId String value containg the DigitalID of the Mesh
   */
  public static MeshStatus getMeshStatus(String meshId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "getMeshStatus()", "meshId", meshId);
    Mesh mesh = getMesh(meshId);

    if (mesh != null) {
      return mesh.getStatus();
    } else {
      return null;
    }
  }

  /**
   * Method to return the Device History
   *
   * @param digitalId String value containing the identifying information for the Device
   * @return Collection object
   */
  public static Collection<DeviceHistoryEntry> getDeviceHistory(String digitalId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "getDeviceHistory()", "digitalId", digitalId);
    Device device = getDevice(digitalId);

    if (device != null) {
      return device.getHistory();
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * Method to return the MeshHistory
   *
   * @param meshId DigitialId of the Mesh device
   * @return Collection list of MeshHistoryEntry object for use
   */
  public static Collection<MeshHistoryEntry> getMeshHistory(String meshId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "getMeshHistory()", "meshId", meshId);
    Mesh mesh = getMesh(meshId);
    if (mesh != null) {
      return mesh.getHistory();
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * Method to return the count of the Devices in the database
   *
   * @return Int with the count of the all devices
   */
  public static int getDeviceCount() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController",
    //"getDeviceCount()");
    return devices.size();
  }

  public static int getMeshCount() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(), "SystemController",
    //"getMeshCount()");
    return meshes.size();
  }

  /**
   * Method to return the count of Devices with status of ACTIVE to the caller
   *
   * @return Int value that contains the count of ACTIVE meshes
   */
  public static int getDeviceHealthyCount() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(),
    //"SystemController", "getDeviceHealthyCount()");
    int count = 0;

    // Count the number of active devices
    for (Device device : devices) {
      if (device.getStatus() == DeviceStatus.ACTIVE) {
        count++;
      }
    }

    return count;
  }

  /**
   * @return a map that holds the count of devices that are currently in a particular device status
   */

  public static Map<DeviceStatus, AtomicInteger> getDeviceHealthMap() {
    Map<DeviceStatus, AtomicInteger> deviceHealthMap = new HashedMap();
    for (final DeviceStatus deviceStatus : DeviceStatus.values()) {
      deviceHealthMap.put(deviceStatus, new AtomicInteger(0));
    }
    devices.stream().forEach(e -> {
      deviceHealthMap.get(e.getStatus()).incrementAndGet();
    });
    return deviceHealthMap;
  }

  public static Map<MeshStatus, AtomicInteger> getMeshHealthMap() {
    Map<MeshStatus, AtomicInteger> meshHealthMap = new HashedMap();
    for (final MeshStatus meshStatus : MeshStatus.values()) {
      meshHealthMap.put(meshStatus, new AtomicInteger(0));
    }
    meshes.stream().forEach(e -> {
      if (e.getStatus() != null) {
        meshHealthMap.get(e.getStatus()).incrementAndGet();
      } else {
        log.warn("Mesh status is null for mesh id:" + e.getMeshId());
      }
    });
    return meshHealthMap;
  }

  /**
   * Method to return the number of Mesh with status of ACTIVE to the caller
   *
   * @return Int value that contains the count of ACTIVE meshes
   */
  public static int getMeshHealthyCount() {
    //log.debug("{}: Entered {} method {}", LocalDateTime.now(),
    //"SystemController", "getMeshHealthyCount()");
    int count = 0;

    // Count the number of active meshes
    for (Mesh mesh : meshes) {
      if (mesh.getStatus() == MeshStatus.ACTIVE) {
        count++;
      }
    }

    return count;
  }

  /**
   * Method to return the Mesh historical data for all time
   *
   * @param meshId String: DigitialID of the Mesh
   */
  private static List<MeshHistoryEntry> parseMeshHistory(String meshId) {
    Collection<Mesh> toAddToDB = DataController.getAllMeshes();

    // If empty, don't do anything and return out of the function
    if (toAddToDB.isEmpty()) {
      return null;
    }

    ArrayList<MeshHistoryEntry> history = new ArrayList<>();

    /*
    //Populate the ArrayList for Meshes
    for (int i = 0; i < toAddToDB.size(); i++) {
      if (toAddToDB.get(i).getStatus() != null) {
        history.add(make(MeshStatus.ACTIVE, toAddToDB.get(i).getLastDataTime()));
      } else {
        history.add(make(MeshStatus.INACTIVE, toAddToDB.get(i).getLastDataTime()));
      }
    }
    */

    for (Mesh e : toAddToDB) {
      for (Device d : DataController.getMeshDevices(e.getMeshId())) {

      }
    }

    //Append the data to the relevant Device
    for (Mesh e : meshes) {
      if (e.getMeshId().equals(meshId)) {
        e.setHistory(history);
      }
    }
    return history;
  }

  /**
   * Public method to encapsulate the private version
   *
   * @param deviceID String value containing device
   * @return List of DeviceHistoryEntry objects
   */
  public static List<DeviceHistoryEntry> parseDevice(String deviceID) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "parseDevice()", "deviceId", deviceID);
    if (getDevice(deviceID) != null) {
      return parseDeviceHistory(deviceID);
    }
    return null;
  }

  /**
   * Private helper method to encapsulate DeviceHistoryEntry class use by SystemController
   *
   * @param status DeviceStatus object to contain status of Device
   * @param timestamp LocalDateTime object to contain status of Device
   * @return DeviceHistoryEntry object containing Device status and timestamp
   */
  private static DeviceHistoryEntry make(String deviceId, DeviceStatus status,
      LocalDateTime timestamp) {
    //log.debug("{}: Entered {} method {} with parameter {}", LocalDateTime.now(), "SystemController",
    //"parseDevice()", "deviceId");
    return new DeviceHistoryEntry(deviceId, timestamp, status);
  }

  /**
   * Private helper method to encapsulate MeshHistoryEntry class use by SystemController
   *
   * @param status MeshStatus object to contain status of the device
   * @param datatime LocalDateTime object to contain status of Device
   * @return MeshHistoryEntry object containing Device status and timestamp
   */
  private static MeshHistoryEntry make(String meshId, MeshStatus status, LocalDateTime datatime) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "make()", "datatime", datatime);
    return new MeshHistoryEntry(meshId, datatime, status);
  }

  /**
   * Method to return the Device active/health status
   *
   * @param deviceId String value that contains the DigitalID
   * @return ArrayList for DeviceHistoryEntry objects that contains Device History information
   */
  private static List<DeviceHistoryEntry> parseDeviceHistory(String deviceId) {
    //log.debug("{}: Entered {} method {} with parameter {} {}", LocalDateTime.now(),
    //"SystemController", "parseDeviceHistory()", "deviceId", deviceId);
    //Build history status history list if
    List<DeviceData> toAddToDB = DataController.getDeviceData(deviceId);

    // If empty, don't do anything and return out of the function
    if (toAddToDB.isEmpty()) {
      return null;
    }

    //ArrayList of all times in the device data to be build and returned by the function
    ArrayList<DeviceHistoryEntry> history = new ArrayList<>();

    // DURATION CODE
    // Starts at most recent till the oldest
    // Time Interval 30 min plus or minus 5 minutes for finding next valid value
    // Comparison Interval decided via looking at sample data transmitted
    int sensmit_interval = 30;

    //First item check
    //Skip weird data i.e. No timestamp at start of the pulled in list
    int start = 0;
    while (toAddToDB.get(start).getTimestamp().toString().isEmpty()) {
      start++;
    }

    //If not weird, add to the list
    LocalDateTime startTime = toAddToDB.get(start).getTimestamp();
    history.add(make(deviceId, DeviceStatus.ACTIVE, startTime));

    //Time Normalization
    // For 30 minutes to adjust for Sensmit Data
    int interval = 0;
    long startMin = startTime.getMinute();
    if (startMin > 15 && startMin <= 45) {
      interval = 30;
    }

    //Normalized time to nearest 30 minutes interval
    LocalDateTime comparison = startTime.withMinute(interval);

    // After first
    /* Logic:
      Check if next time is not empty, if so status = inactive
      + then check if not within 30 minutes plus or minus 5 minutes, if so status = inactive
      + if not empty and within 30 minutes plus or minus 5 minutes, then status = active;
      + Goto the next one
    */
    int i = ++start;
    while (i < toAddToDB.size()) {
      comparison = comparison.minusMinutes(sensmit_interval);
      LocalDateTime lower = comparison.minusMinutes(5);
      LocalDateTime upper = comparison.plusMinutes(5);
      LocalDateTime current = toAddToDB.get(i).getTimestamp();

      if (current.toString().isEmpty()) {
        history.add(make(deviceId, DeviceStatus.INACTIVE, comparison));
      } else if (!(current.isAfter(lower) || current.isBefore(upper))) {
        history.add(make(deviceId, DeviceStatus.INACTIVE, comparison));
      } else {
        history.add(make(deviceId, DeviceStatus.ACTIVE, current));
        i++;
      }
    }

    //Append the data to the relevant Device
    for (Device e : devices) {
      if (e.getDigitalId().equals(deviceId)) {
        e.setHistory(history);
      }
    }
    return history;
  }

  /**
   * Wrapper method to allow the init to check for ticket creation
   *
   * @param devices Collection of Devices; To pass into method for Ticket Creation if necessary
   * according to the Device's status
   * @return Boolean; True if devices is successfully parsed; Returns false and prints out stack
   * trace
   */
  private static boolean parseForIssuesForDevices(Collection<Device> devices) {
    try {
      return JiraController.parseDevicesForIssues(devices);
    } catch (JiraException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Method to return the HTTP status of Jira: Good if 200, Bad if rest
   *
   * @return Boolean; True/Good if 200, False/Bad if any of the rest of the status codes
   */
  public static boolean getJiraHTTP200Status() {
    //String StatusCodeToBeDisplayed
    return JiraController.getStatusOfJira();
  }
  //EOF
}
