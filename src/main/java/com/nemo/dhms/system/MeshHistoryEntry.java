/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * MeshHistoryEntry Class: Purpose of the class is to store Mesh data in memory of LocalDateTime
 * date and MeshStatus variables for building Mesh's History
 */
@Entity
@Table(name = "mesh_history")
@IdClass(MeshHistoryKey.class)
public class MeshHistoryEntry {

  @Id
  @Column(name = "mesh")
  private String mesh;

  @Id
  @Column(name = "date")
  private LocalDateTime date;

  @Enumerated(EnumType.STRING)
  @Column(name = "status", nullable = false)
  private MeshStatus status;

  public MeshHistoryEntry() {

  }

  public MeshHistoryEntry(String meshId, LocalDateTime date, MeshStatus status) {
    this.mesh = meshId;
    this.date = date;
    this.status = status;
  }

  public MeshHistoryEntry(MeshHistoryEntry copy) {
    this.mesh = copy.mesh;
    this.date = copy.date;
    this.status = copy.status;
  }

  //TODO: Write JavaDoc
  public String getMesh() {
    return mesh;
  }

  public void setMesh(String meshId) {
    this.mesh = meshId;
  }

  //TODO: Write JavaDoc
  public LocalDateTime getDate() {
    return date;
  }

  //TODO: Write JavaDoc
  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  //TODO: Write JavaDoc
  public MeshStatus getStatus() {
    return status;
  }

  public void setStatus(MeshStatus status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MeshHistoryEntry that = (MeshHistoryEntry) o;
    return Objects.equal(date, that.date) &&
        status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(date, status);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("mesh", mesh)
        .add("date", date)
        .add("status", status)
        .toString();
  }
}
