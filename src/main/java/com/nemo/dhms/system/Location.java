/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Location Class: Purpose of this class is to contain, in memory, the latitude and longitude
 * location of the desired Mesh
 */
public class Location {

  private float latitude;
  private float longitude;

  public Location() {
    latitude = 0.0f;
    longitude = 0.0f;
  }

  /**
   * Constructor for Location objects
   *
   * @param latitude float data type for latitude of the Location
   * @param longitude float data type for longitude of the Location
   */
  public Location(float latitude, float longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public Location(Location location) {
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

  public float getLatitude() {
    return latitude;
  }

  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }

  public float getLongitude() {
    return longitude;
  }

  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Location location = (Location) o;
    return Float.compare(location.latitude, latitude) == 0 &&
        Float.compare(location.longitude, longitude) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(latitude, longitude);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("latitude", latitude)
        .add("longitude", longitude)
        .toString();
  }
}
