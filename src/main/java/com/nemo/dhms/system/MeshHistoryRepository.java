package com.nemo.dhms.system;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeshHistoryRepository extends
    CrudRepository<MeshHistoryEntry, MeshHistoryKey> {

  Optional<MeshHistoryEntry> findByMeshAndDate(String meshId, LocalDateTime date);

  List<MeshHistoryEntry> findAllByMesh(String meshId);
}
