package com.nemo.dhms.system;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PostgresConnector implements DatabaseConnector {

  private final DeviceHistoryRepository devices;

  private final MeshHistoryRepository meshes;

  @Autowired
  public PostgresConnector(DeviceHistoryRepository devices, MeshHistoryRepository meshes) {
    this.devices = devices;
    this.meshes = meshes;
  }

  @Override
  public boolean putDeviceStatus(DeviceHistoryEntry entry) {
    if (devices == null) {
      return false;
    }

    DeviceHistoryEntry saved = devices.save(entry);
    return entry.equals(saved);
  }

  @Override
  public boolean removeDeviceStatus(DeviceHistoryEntry entry) {
    if (devices == null) {
      return false;
    }

    try {
      devices.delete(entry);
      return true;
    }
    catch (IllegalArgumentException ex) {
      return false;
    }
  }

  /**
   * Returns a list of historical statuses for a device.
   *
   * @param deviceId String value representing current device digitalID
   * @return - DeviceStatus object for requested current Device, and throw exception if null.
   */
  @Override
  public List<DeviceHistoryEntry> getDeviceStatus(String deviceId) {
    if (devices == null) {
      return Collections.emptyList();
    }

    return Collections.unmodifiableList(devices.findAllByDevice(deviceId));
  }

  @Override
  public boolean putMeshStatus(MeshHistoryEntry entry) {
    if (meshes == null) {
      return false;
    }

    MeshHistoryEntry saved = meshes.save(entry);
    return entry.equals(saved);
  }

  @Override
  public boolean removeMeshStatus(MeshHistoryEntry entry) {
    if (meshes == null) {
      return false;
    }

    try {
      meshes.delete(entry);
      return true;
    }
    catch (IllegalArgumentException ex) {
      return false;
    }
  }

  /**
   * Returns a list of historical statuses for a mesh.
   *
   * @param meshId mesh ID
   * @return list of mesh status history
   */
  @Override
  public List<MeshHistoryEntry> getMeshStatus(String meshId) {
    if (meshes == null) {
      return Collections.emptyList();
    }

    return Collections.unmodifiableList(meshes.findAllByMesh(meshId));
  }

  boolean isActive() {
    return (devices != null && meshes != null);
  }
}
