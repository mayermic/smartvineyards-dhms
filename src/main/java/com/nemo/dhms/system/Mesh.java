/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Mesh Class: Purpose of this class is contain, in memory, all of the attribute variables to
 * represent a Mesh
 */
public class Mesh {

  private String meshId;
  private LocalDateTime lastDataTime;
  private LocalDateTime activateTime;
  private MeshStatus status;
  private Location location;
  private List<MeshHistoryEntry> history = null;
  private LocalDateTime lastSystemUpdate = null;

  public Mesh() {
  }

  /**
   * Mesh Class Constructor to build the data object that is the Mesh
   *
   * @param meshID String ID that identifies the mesh on the SensMit Web data management
   * @param lastDataTime LocalDateTime object to store time of last data transmission
   * @param activateTime LocalDateTime object to store the time of when Mesh was activated
   * @param status MeshStatus object to reflect current status of the Mesh
   */
  public Mesh(String meshID, LocalDateTime lastDataTime, LocalDateTime activateTime,
      MeshStatus status) {
    this.meshId = meshID;
    this.lastDataTime = lastDataTime;
    this.activateTime = activateTime;
    this.status = status;
    this.history = new ArrayList<>(); //add history entry method requires this to be initalized
  }

  public Mesh(Mesh copy) {
    this.meshId = copy.meshId;
    this.lastDataTime = copy.lastDataTime;
    this.activateTime = copy.activateTime;
    this.status = copy.status;
    this.location = new Location(copy.location);
    if (copy.history != null) {
      this.history = new ArrayList<>(copy.history);
    }
    this.lastSystemUpdate = copy.lastSystemUpdate;
  }

  public String getMeshId() {
    return meshId;
  }

  public void setMeshId(String meshId) {
    this.meshId = meshId;
  }

  public LocalDateTime getLastDataTime() {
    return lastDataTime;
  }

  public void setLastDataTime(LocalDateTime lastDataTime) {
    this.lastDataTime = lastDataTime;
  }

  public LocalDateTime getActivateTime() {
    return activateTime;
  }

  public void setActivateTime(LocalDateTime activateTime) {
    this.activateTime = activateTime;
  }

  public MeshStatus getStatus() {
    return status;
  }

  public void setStatus(MeshStatus status) {
    this.status = status;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public List<MeshHistoryEntry> getHistory() {
    return history;
  }

  public void setHistory(List<MeshHistoryEntry> history) {
    this.history = history;
  }

  public void addHistory(List<MeshHistoryEntry> history) {
    this.history.addAll(history);
  }

  public void addHistory(MeshHistoryEntry historyEntry) {
    this.history.add(historyEntry);
  }

  public LocalDateTime getLastSystemUpdate() {
    return lastSystemUpdate;
  }

  public void setLastSystemUpdate(LocalDateTime lastSystemUpdate) {
    this.lastSystemUpdate = lastSystemUpdate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Mesh mesh = (Mesh) o;
    return Objects.equal(meshId, mesh.meshId) &&
        Objects.equal(lastDataTime, mesh.lastDataTime) &&
        Objects.equal(activateTime, mesh.activateTime) &&
        status == mesh.status &&
        Objects.equal(location, mesh.location) &&
        Objects.equal(history, mesh.history) &&
        Objects.equal(lastSystemUpdate, mesh.lastSystemUpdate);
  }

  @Override
  public int hashCode() {
    return Objects
        .hashCode(meshId, lastDataTime, activateTime, status, location, history, lastSystemUpdate);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("meshId", meshId)
        .add("lastDataTime", lastDataTime)
        .add("activateTime", activateTime)
        .add("status", status)
        .add("location", location)
        .add("history", history)
        .add("lastSystemUpdate", lastSystemUpdate)
        .toString();
  }
}