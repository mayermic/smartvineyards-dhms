/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Device Class: Purpose of this class is contain, in memory, all of the attribute variables to
 * represent a Device
 */
public class Device {

  private String digitalId = null;
  private String hardwareId = null;
  private String meshId = null;
  private LocalDateTime lastCaptureTime = null;
  private DeviceStatus status = null;
  private Location location = null;
  private List<DeviceHistoryEntry> history = null;
  private LocalDateTime lastSystemUpdate = null;

  public Device() {
  }

  /**
   * Method is instantiate Device Class with list of all requires paramers
   *
   * @param digitalId ID that identifies the Device in the Mesh Network
   * @param hardwareId ID that identifies that physical hardware of the Device
   * @param meshId ID that identifies the Mesh Network. It is the Base's digital ID
   * @param lastCaptureTime Last capture time of the data in LocalDateTime object
   * @param status Status of the device
   * @param location Location object containing latitiude and longitude pair data information
   */
  public Device(String digitalId, String hardwareId, String meshId, LocalDateTime lastCaptureTime,
      DeviceStatus status, Location location) {
    this.digitalId = digitalId;
    this.hardwareId = hardwareId;
    this.meshId = meshId;
    this.lastCaptureTime = lastCaptureTime;
    this.status = status;
    this.location = new Location(location);
    this.history = new ArrayList<>();  //add history method requires this to be initialized.
  }

  public Device(Device copy) {
    this.digitalId = copy.digitalId;
    this.hardwareId = copy.hardwareId;
    this.meshId = copy.meshId;
    this.lastCaptureTime = copy.lastCaptureTime;
    this.status = copy.status;
    this.location = new Location(copy.location);

    if (copy.history != null) {
      this.history = new ArrayList<>(copy.history);
    }

    this.lastSystemUpdate = copy.lastSystemUpdate;
  }

  public String getDigitalId() {
    return digitalId;
  }

  public void setDigitalId(String digitalId) {
    this.digitalId = digitalId;
  }

  public String getHardwareId() {
    return hardwareId;
  }

  public void setHardwareId(String hardwareId) {
    this.hardwareId = hardwareId;
  }

  public String getMeshId() {
    return meshId;
  }

  public void setMeshId(String meshId) {
    this.meshId = meshId;
  }

  public LocalDateTime getLastCaptureTime() {
    return lastCaptureTime;
  }

  public void setLastCaptureTime(LocalDateTime lastCaptureTime) {
    this.lastCaptureTime = lastCaptureTime;
  }

  public DeviceStatus getStatus() {
    return status;
  }

  public void setStatus(DeviceStatus status) {
    this.status = status;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = new Location(location);
  }

  public List<DeviceHistoryEntry> getHistory() {
    return history;
  }

  public void setHistory(List<DeviceHistoryEntry> history) {
    this.history = history;
  }

  public void addHistory(DeviceHistoryEntry historyEntry) {
    this.history.add(historyEntry);
  }

  public LocalDateTime getLastSystemUpdate() {
    return lastSystemUpdate;
  }

  public void setLastSystemUpdate(LocalDateTime lastSystemUpdate) {
    this.lastSystemUpdate = lastSystemUpdate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Device device = (Device) o;
    return Objects.equal(digitalId, device.digitalId) &&
        Objects.equal(hardwareId, device.hardwareId) &&
        Objects.equal(meshId, device.meshId) &&
        Objects.equal(lastCaptureTime, device.lastCaptureTime) &&
        status == device.status &&
        Objects.equal(location, device.location) &&
        Objects.equal(history, device.history) &&
        Objects.equal(lastSystemUpdate, device.lastSystemUpdate);
  }

  @Override
  public int hashCode() {
    return Objects
        .hashCode(digitalId, hardwareId, meshId, lastCaptureTime, status, location, history,
            lastSystemUpdate);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("digitalId", digitalId)
        .add("hardwareId", hardwareId)
        .add("meshId", meshId)
        .add("lastCaptureTime", lastCaptureTime)
        .add("status", status)
        .add("location", location)
        .add("history", history)
        .add("lastSystemUpdate", lastSystemUpdate)
        .toString();
  }
}
