/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.system;

/**
 * DeviceStatus Class: Purpose of this class is to parse and return the status of the Device
 */
public enum DeviceStatus {
  ACTIVE, HEALTHY, WARNING, FAILURE, IDLE, INACTIVE;

  /**
   * Method to parse the passed in statusCode for the Device
   *
   * @param statusCode String value to be parsed by this method
   * @return Enum value to return status of the Device. Returns ACTIVE if string "active" passed in.
   * Returns INACTIVE if string "inactive" passed in. Return null otherwise
   */
  public static DeviceStatus parseString(String statusCode) {
    if (statusCode.toUpperCase().equals("active".toUpperCase())) {
      return ACTIVE;
    } else if (statusCode.toUpperCase().equals("inactive".toUpperCase())) {
      return INACTIVE;
    } else if (statusCode.toUpperCase().equals("healthy".toUpperCase())) {
      return HEALTHY;
    } else if (statusCode.toUpperCase().equals("warning".toUpperCase())) {
      return WARNING;
    } else if (statusCode.toUpperCase().equals("failure".toUpperCase())) {
      return FAILURE;
    } else if (statusCode.toUpperCase().equals("idle".toUpperCase())) {
      return IDLE;
    } else {
      return null;
    }
  }

  /**
   * Utility method to convert Active/Inactive code from Sensmit to string version of
   * Active/Inactive
   *
   * @return String of "Active" if enum value ACTIVE or String "Inactive" if enum value INACTIVE, or
   * String "null" if enum value for Device Status parses nothing
   */
  @Override
  public String toString() {
    if (this == ACTIVE) {
      return "active";
    } else if (this == INACTIVE) {
      return "inactive";
    } else if (this == HEALTHY) {
      return "healthy";
    } else if (this == WARNING) {
      return "warning";
    } else if (this == FAILURE) {
      return "failure";
    } else if (this == IDLE) {
      return "idle";
    } else {
      return "null";
    }
  }
}
