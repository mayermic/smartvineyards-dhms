package com.nemo.dhms.system;

import java.util.List;

public interface DatabaseConnector {

  boolean putDeviceStatus(DeviceHistoryEntry entry);

  boolean removeDeviceStatus(DeviceHistoryEntry entry);

  List<DeviceHistoryEntry> getDeviceStatus(String deviceId);

  boolean putMeshStatus(MeshHistoryEntry entry);

  boolean removeMeshStatus(MeshHistoryEntry entry);

  List<MeshHistoryEntry> getMeshStatus(String meshId);
}
