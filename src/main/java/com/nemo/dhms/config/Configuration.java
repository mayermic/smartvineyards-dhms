/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.config;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Configuration Class, when called, creates a Configuration object for settings: systemUpdatePeriod,
 * warningPeriod, failurePeriod, selfHealingPeriod, moistureWarningLevel, moistureFailureLevel,
 * internalTemperatureWarningLevel,internalTemperatureFailureLevel, externalTemperatureWarningLevel,
 * externalTemperatureFailureLevel, powerWarningLevel, powerFailureLevel, capacitorWarningLevel,
 * and capacitorFailureLevel.  Methods equals() and hashcode() are utilized for object equivalence checks.
 * A to toString() method is also included.
 */
public class Configuration {
  private int systemUpdatePeriod = 30; // Update period in minutes
  private int warningPeriod = 60; // Warning period in minutes
  private int failurePeriod = 180; // Failure period in minutes
  private int selfHealingPeriod = 90; // Self-healing period in minutes

  private double moistureWarningLevel = 0.0;
  private double moistureFailureLevel = 0.0;
  private double internalTemperatureWarningLevel = 0.0;
  private double internalTemperatureFailureLevel = 0.0;
  private double externalTemperatureWarningLevel = 0.0;
  private double externalTemperatureFailureLevel = 0.0;
  private double powerWarningLevel = 0.0;
  private double powerFailureLevel = 0.0;
  private double capacitorWarningLevel = 0.0;
  private double capacitorFailureLevel = 0.0;

  Configuration() {
  }

  /**
   * Configuration copy Constructor
   *
   * @param copy - configuration object to be copied
   */
  Configuration(Configuration copy) {
    this.systemUpdatePeriod = copy.systemUpdatePeriod;
    this.warningPeriod = copy.warningPeriod;
    this.failurePeriod = copy.failurePeriod;
    this.selfHealingPeriod = copy.selfHealingPeriod;
    this.moistureWarningLevel = copy.moistureWarningLevel;
    this.moistureFailureLevel = copy.moistureFailureLevel;
    this.internalTemperatureWarningLevel = copy.internalTemperatureWarningLevel;
    this.internalTemperatureFailureLevel = copy.internalTemperatureFailureLevel;
    this.externalTemperatureWarningLevel = copy.externalTemperatureWarningLevel;
    this.externalTemperatureFailureLevel = copy.externalTemperatureFailureLevel;
    this.powerWarningLevel = copy.powerWarningLevel;
    this.powerFailureLevel = copy.powerFailureLevel;
    this.capacitorWarningLevel = copy.capacitorWarningLevel;
    this.capacitorFailureLevel = copy.capacitorFailureLevel;
  }

  public int getSystemUpdatePeriod() {
    return systemUpdatePeriod;
  }

  public void setSystemUpdatePeriod(int systemUpdatePeriod) {
    this.systemUpdatePeriod = systemUpdatePeriod;
  }

  public int getWarningPeriod() {
    return warningPeriod;
  }

  public void setWarningPeriod(int warningPeriod) {
    this.warningPeriod = warningPeriod;
  }

  public int getFailurePeriod() {
    return failurePeriod;
  }

  public void setFailurePeriod(int failurePeriod) {
    this.failurePeriod = failurePeriod;
  }

  public int getSelfHealingPeriod() {
    return selfHealingPeriod;
  }

  public void setSelfHealingPeriod(int selfHealingPeriod) {
    this.selfHealingPeriod = selfHealingPeriod;
  }

  public double getMoistureWarningLevel() {
    return moistureWarningLevel;
  }

  public void setMoistureWarningLevel(double moistureWarningLevel) {
    this.moistureWarningLevel = moistureWarningLevel;
  }

  public double getMoistureFailureLevel() {
    return moistureFailureLevel;
  }

  public void setMoistureFailureLevel(double moistureFailureLevel) {
    this.moistureFailureLevel = moistureFailureLevel;
  }

  public double getInternalTemperatureWarningLevel() {
    return internalTemperatureWarningLevel;
  }

  public void setInternalTemperatureWarningLevel(double internalTemperatureWarningLevel) {
    this.internalTemperatureWarningLevel = internalTemperatureWarningLevel;
  }

  public double getInternalTemperatureFailureLevel() {
    return internalTemperatureFailureLevel;
  }

  public void setInternalTemperatureFailureLevel(double internalTemperatureFailureLevel) {
    this.internalTemperatureFailureLevel = internalTemperatureFailureLevel;
  }

  public double getExternalTemperatureWarningLevel() {
    return externalTemperatureWarningLevel;
  }

  public void setExternalTemperatureWarningLevel(double externalTemperatureWarningLevel) {
    this.externalTemperatureWarningLevel = externalTemperatureWarningLevel;
  }

  public double getExternalTemperatureFailureLevel() {
    return externalTemperatureFailureLevel;
  }

  public void setExternalTemperatureFailureLevel(double externalTemperatureFailureLevel) {
    this.externalTemperatureFailureLevel = externalTemperatureFailureLevel;
  }

  public double getPowerWarningLevel() {
    return powerWarningLevel;
  }

  public void setPowerWarningLevel(double powerWarningLevel) {
    this.powerWarningLevel = powerWarningLevel;
  }

  public double getPowerFailureLevel() {
    return powerFailureLevel;
  }

  public void setPowerFailureLevel(double powerFailureLevel) {
    this.powerFailureLevel = powerFailureLevel;
  }

  public double getCapacitorWarningLevel() {
    return capacitorWarningLevel;
  }

  public void setCapacitorWarningLevel(double capacitorWarningLevel) {
    this.capacitorWarningLevel = capacitorWarningLevel;
  }

  public double getCapacitorFailureLevel() {
    return capacitorFailureLevel;
  }

  public void setCapacitorFailureLevel(double capacitorFailureLevel) {
    this.capacitorFailureLevel = capacitorFailureLevel;
  }

  /**
   * equals(Object) checks for Configuration object equivalence.
   *
   * @param o - Object o, the object to be campared against
   * @return - returns true if all fields are equal, and false if not
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Configuration that = (Configuration) o;
    return systemUpdatePeriod == that.systemUpdatePeriod &&
        warningPeriod == that.warningPeriod &&
        failurePeriod == that.failurePeriod &&
        selfHealingPeriod == that.selfHealingPeriod &&
        Double.compare(that.moistureWarningLevel, moistureWarningLevel) == 0 &&
        Double.compare(that.moistureFailureLevel, moistureFailureLevel) == 0 &&
        Double.compare(that.internalTemperatureWarningLevel, internalTemperatureWarningLevel) == 0
        &&
        Double.compare(that.internalTemperatureFailureLevel, internalTemperatureFailureLevel) == 0
        &&
        Double.compare(that.externalTemperatureWarningLevel, externalTemperatureWarningLevel) == 0
        &&
        Double.compare(that.externalTemperatureFailureLevel, externalTemperatureFailureLevel) == 0
        &&
        Double.compare(that.powerWarningLevel, powerWarningLevel) == 0 &&
        Double.compare(that.powerFailureLevel, powerFailureLevel) == 0 &&
        Double.compare(that.capacitorWarningLevel, capacitorWarningLevel) == 0 &&
        Double.compare(that.capacitorFailureLevel, capacitorFailureLevel) == 0;
  }

  /**
   * hashCode() generates a unique int hash value for each configuration object.
   *
   * @return - returns unique config object hash value as an int.
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(systemUpdatePeriod, warningPeriod, failurePeriod, selfHealingPeriod,
        moistureWarningLevel, moistureFailureLevel, internalTemperatureWarningLevel,
        internalTemperatureFailureLevel, externalTemperatureWarningLevel,
        externalTemperatureFailureLevel, powerWarningLevel, powerFailureLevel,
        capacitorWarningLevel,
        capacitorFailureLevel);
  }

  /**
   * returns a toString representation of the specified configuration object
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("systemUpdatePeriod", systemUpdatePeriod)
        .add("warningPeriod", warningPeriod)
        .add("failurePeriod", failurePeriod)
        .add("selfHealingPeriod", selfHealingPeriod)
        .add("moistureWarningLevel", moistureWarningLevel)
        .add("moistureFailureLevel", moistureFailureLevel)
        .add("internalTemperatureWarningLevel", internalTemperatureWarningLevel)
        .add("internalTemperatureFailureLevel", internalTemperatureFailureLevel)
        .add("externalTemperatureWarningLevel", externalTemperatureWarningLevel)
        .add("externalTemperatureFailureLevel", externalTemperatureFailureLevel)
        .add("powerWarningLevel", powerWarningLevel)
        .add("powerFailureLevel", powerFailureLevel)
        .add("capacitorWarningLevel", capacitorWarningLevel)
        .add("capacitorFailureLevel", capacitorFailureLevel)
        .toString();
  }
}
