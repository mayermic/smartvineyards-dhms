/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.config;

import com.nemo.dhms.dashboard.controllers.DashboardController;
import com.nemo.dhms.data.SensmitConnector;
import com.nemo.dhms.issues.JiraController;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * ConfigurationController Class provides methods that get, set, save and load current configuration data.
 * loadConfiguration(), loads the system .config file upon startup, and saveConfiguration() is called to
 * save the .config file after startup and when any change is made to the current configuration settings by
 * the user.  getters and setters return and update configuration values.
 */
public class ConfigurationController {

  public static Configuration configuration = null;

  private static boolean initialized = false;

  private ConfigurationController() {
  }

  /**
   * init() checks if a configuration object has been created.  Returns true if a config object
   * exists. If no config object is found, then init() creates and new configuration object and then
   * returns true.
   */
  public static void init() {
    if (initialized || configuration != null) {
      return;
    }

    configuration = new Configuration();

    initialized = true;
  }

  public static boolean isInitialized() {
    return initialized;
  }

  public static void setSensmitApiKey(String key) {
    SensmitConnector.setSensmitApiKey(key);
  }

  public static void setGoogleMapsApiKey(String key) {
    DashboardController.setGoogleMapsApiKey(key);
  }

  public static int getSystemUpdatePeriod() {
    return configuration.getSystemUpdatePeriod();
  }

  public static void setSystemUpdatePeriod(int systemUpdatePeriod) {
    configuration.setSystemUpdatePeriod(systemUpdatePeriod);
  }

  public static int getWarningPeriod() {
    return configuration.getWarningPeriod();
  }

  public static void setWarningPeriod(int warningPeriod) {
    configuration.setWarningPeriod(warningPeriod);
  }

  public static int getFailurePeriod() {
    return configuration.getFailurePeriod();
  }

  public static void setFailurePeriod(int failurePeriod) {
    configuration.setFailurePeriod(failurePeriod);
  }

  public static int getSelfHealingPeriod() {
    return configuration.getSelfHealingPeriod();
  }

  public static void setSelfHealingPeriod(int selfHealingPeriod) {
    configuration.setSelfHealingPeriod(selfHealingPeriod);
  }

  public static double getMoistureWarningLevel() {
    return configuration.getMoistureWarningLevel();
  }

  public static void setMoistureWarningLevel(double moistureWarningLevel) {
    configuration.setMoistureWarningLevel(moistureWarningLevel);
  }

  public static double getMoistureFailureLevel() {
    return configuration.getMoistureFailureLevel();
  }

  public static void setMoistureFailureLevel(double moistureFailureLevel) {
    configuration.setMoistureFailureLevel(moistureFailureLevel);
  }

  public static double getInternalTemperatureWarningLevel() {
    return configuration.getInternalTemperatureWarningLevel();
  }

  public static void setInternalTemperatureWarningLevel(double internalTemperatureWarningLevel) {
    configuration.setInternalTemperatureWarningLevel(internalTemperatureWarningLevel);
  }

  public static double getInternalTemperatureFailureLevel() {
    return configuration.getInternalTemperatureFailureLevel();
  }

  public static void setInternalTemperatureFailureLevel(double internalTemperatureFailureLevel) {
    configuration.setInternalTemperatureFailureLevel(internalTemperatureFailureLevel);
  }

  public static double getExternalTemperatureWarningLevel() {
    return configuration.getExternalTemperatureWarningLevel();
  }

  public static void setExternalTemperatureWarningLevel(double externalTemperatureWarningLevel) {
    configuration.setExternalTemperatureWarningLevel(externalTemperatureWarningLevel);
  }

  public static double getExternalTemperatureFailureLevel() {
    return configuration.getExternalTemperatureFailureLevel();
  }

  public static void setExternalTemperatureFailureLevel(double externalTemperatureFailureLevel) {
    configuration.setExternalTemperatureFailureLevel(externalTemperatureFailureLevel);
  }

  public static double getPowerWarningLevel() {
    return configuration.getPowerWarningLevel();
  }

  public static void setPowerWarningLevel(double powerWarningLevel) {
    configuration.setPowerWarningLevel(powerWarningLevel);
  }

  public static double getPowerFailureLevel() {
    return configuration.getPowerFailureLevel();
  }

  public static void setPowerFailureLevel(double powerFailureLevel) {
    configuration.setPowerFailureLevel(powerFailureLevel);
  }

  public static double getCapacitorWarningLevel() {
    return configuration.getCapacitorWarningLevel();
  }

  public static void setCapacitorWarningLevel(double capacitorWarningLevel) {
    configuration.setCapacitorWarningLevel(capacitorWarningLevel);
  }

  public static double getCapacitorFailureLevel() {
    return configuration.getCapacitorFailureLevel();
  }

  public static void setCapacitorFailureLevel(double capacitorFailureLevel) {
    configuration.setCapacitorFailureLevel(capacitorFailureLevel);
  }

  public static void setJiraTicketDefaultAssignee(String defaultAssignee) {
    JiraController.setDefaultAssignee(defaultAssignee);
  }

  public static String getJiraTicketDefaultAssignee() {
    return JiraController.getDefaultAssignee();
  }

  public static void setJiraTicketDefaultPriority(int defaultPriority) {
    if (defaultPriority > 0 || defaultPriority < 5) {
      String priority = String.valueOf(defaultPriority);
      JiraController.setDefaultPriority(priority);
    }
  }

  public static String getJiraTicketDefaultPriority() {
    return JiraController.getDefaultPriority();
  }

  /**
   * loadConfiguration() loads a configuration using values stored in the file specified by input
   * arg filename.
   *
   * @param filename - arg filename is the name of the file that the configuration values are to be
   * pulled from.
   * @throws - if file specified by filename can not be found, then a default configuration file
   * will be loaded.
   **/
  public static void loadConfiguration(String filename) {
    FileReader fReader = null;
    try {
      fReader = new FileReader(filename);
    } catch (FileNotFoundException e) {
      System.out.println("Failed to load Config file");
      e.printStackTrace();
    }
    BufferedReader bReader = null;
    if (fReader != null) {
      bReader = new BufferedReader(fReader);
    }
    try {
      if (bReader != null) {
        while (bReader.ready()) {
          ConfigurationController.setSystemUpdatePeriod(Integer.valueOf(bReader.readLine()));
          ConfigurationController.setWarningPeriod(Integer.valueOf(bReader.readLine()));
          ConfigurationController.setFailurePeriod(Integer.valueOf(bReader.readLine()));
          ConfigurationController.setSelfHealingPeriod(Integer.valueOf(bReader.readLine()));
          ConfigurationController.setMoistureWarningLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController.setMoistureFailureLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController
              .setInternalTemperatureWarningLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController
              .setInternalTemperatureFailureLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController
              .setExternalTemperatureWarningLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController
              .setExternalTemperatureFailureLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController.setPowerWarningLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController.setPowerFailureLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController.setCapacitorWarningLevel(Double.valueOf(bReader.readLine()));
          ConfigurationController.setCapacitorFailureLevel(Double.valueOf(bReader.readLine()));
        }
        bReader.close();
        fReader.close();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void saveConfiguration() {
    saveConfiguration("dhms.config");
  }

  /**
   * saveConfiguration() takes a String filename as input and attempts to open and read the
   * specified configuration file.  Provided saveConfiguration can open the specified file, it will
   * then overwrite tbe existing file configuration string name/values with the new string
   * name/values returned by a call to configuration.toString().  saveConfiguration will catch
   * exceptions if it can not create a new FileWriter or it can not complete a write to the newly
   * created configuration file.
   *
   * @param filename - String filename, the filename for the existing configuration file that will
   * be overwritten/updated.
   **/
  public static void saveConfiguration(String filename) {
    File newConfig = new File(filename);
    FileWriter newConfigWriter = null;
    try {
      if (!newConfig.exists()) {
        newConfig.createNewFile();
      }
      newConfigWriter = new FileWriter(newConfig, false);// false to overwrite. true will append.
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Integer.toString(ConfigurationController.getSystemUpdatePeriod()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(Integer.toString(ConfigurationController.getWarningPeriod()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(Integer.toString(ConfigurationController.getFailurePeriod()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Integer.toString(ConfigurationController.getSelfHealingPeriod()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getMoistureWarningLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getMoistureFailureLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(
            Double.toString(ConfigurationController.getInternalTemperatureWarningLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(
            Double.toString(ConfigurationController.getInternalTemperatureFailureLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(
            Double.toString(ConfigurationController.getExternalTemperatureWarningLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter.write(
            Double.toString(ConfigurationController.getExternalTemperatureFailureLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getPowerWarningLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getPowerFailureLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getCapacitorWarningLevel()) + "\n");
      }
      if (newConfigWriter != null) {
        newConfigWriter
            .write(Double.toString(ConfigurationController.getCapacitorFailureLevel()) + "\n");
      }

      if (newConfigWriter != null) {
        newConfigWriter.flush();
      }

      if (newConfigWriter != null) {
        newConfigWriter.close();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
