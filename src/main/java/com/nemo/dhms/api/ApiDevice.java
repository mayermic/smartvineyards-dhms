/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.api;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.nemo.dhms.system.Device;
import com.nemo.dhms.system.Location;

/**
 * ApiDevice Class, when called, creates an ApiDevice object containing the digitalId, meshId, status,
 * location, lastCaptureTime, and lastSystemUpdate time.  Methods equals() and hashcode() are utilized
 * object equivalence checks. A to toString() method is also included.
 */
class ApiDevice {

  private String digitalId;
  private String meshId;
  private String status;
  private Location location;
  private String lastCaptureTime;
  private String lastSystemUpdate;

  ApiDevice(Device device) {
    this.digitalId = device.getDigitalId();
    this.meshId = device.getMeshId();

    if (device.getStatus() != null) {
      this.status = device.getStatus().toString();
    } else {
      this.status = null;
    }

    this.location = new Location(device.getLocation());
    this.lastCaptureTime = device.getLastCaptureTime().toString();
    this.lastSystemUpdate = device.getLastSystemUpdate().toString();
  }

  public String getDigitalId() {
    return digitalId;
  }

  public void setDigitalId(String digitalId) {
    this.digitalId = digitalId;
  }

  public String getMeshId() {
    return meshId;
  }

  public void setMeshId(String meshId) {
    this.meshId = meshId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public String getLastCaptureTime() {
    return lastCaptureTime;
  }

  public void setLastCaptureTime(String lastCaptureTime) {
    this.lastCaptureTime = lastCaptureTime;
  }

  public String getLastSystemUpdate() {
    return lastSystemUpdate;
  }

  public void setLastSystemUpdate(String lastSystemUpdate) {
    this.lastSystemUpdate = lastSystemUpdate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiDevice apiDevice = (ApiDevice) o;
    return Objects.equal(digitalId, apiDevice.digitalId) &&
        Objects.equal(meshId, apiDevice.meshId) &&
        Objects.equal(status, apiDevice.status) &&
        Objects.equal(location, apiDevice.location) &&
        Objects.equal(lastCaptureTime, apiDevice.lastCaptureTime) &&
        Objects.equal(lastSystemUpdate, apiDevice.lastSystemUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(digitalId, meshId, status, location, lastCaptureTime, lastSystemUpdate);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("digitalId", digitalId)
        .add("meshId", meshId)
        .add("status", status)
        .add("location", location)
        .add("lastCaptureTime", lastCaptureTime)
        .add("lastSystemUpdate", lastSystemUpdate)
        .toString();
  }
}
