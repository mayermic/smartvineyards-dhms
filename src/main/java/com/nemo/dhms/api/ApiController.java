/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.api;

import com.nemo.dhms.system.Device;
import com.nemo.dhms.system.DeviceHistoryEntry;
import com.nemo.dhms.system.DeviceStatus;
import com.nemo.dhms.system.Mesh;
import com.nemo.dhms.system.MeshHistoryEntry;
import com.nemo.dhms.system.MeshStatus;
import com.nemo.dhms.system.SystemController;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * ApiController Class provides API methods that return current device and mesh data for deviceId,
 * meshId, device status, mesh status, device history, mesh history, device count, and mesh count. A
 * response with data on a specific device for mesh requires input of a valid deviceId or meshId.
 * Depending on the call, null input will return data for all devices or meshes.  Return responses,
 * depending on the call, will be an integer or string.
 */
@RestController
public class ApiController {

  /**
   * /api/devices request takes a valid device digitalId as input and returns current device data
   * for the specified digitalId.  If no digitalId is entered, then /api/devices returns data for
   * all devices. If any requested device returns a response of "null", then an empty list will be
   * returned.
   *
   * @param digitalId - the digitalId of the requested device. DigitalId is "null" for all devices.
   * @return - returns device data for the specified device, or data for all devices if digitalId is
   * "null".
   */
  @RequestMapping("/api/devices")
  public Collection<ApiDevice> devices(
      @RequestParam(value = "digitalId", defaultValue = "null") String digitalId,
      @RequestParam(value = "meshId", defaultValue = "null") String meshId) {

    ArrayList<ApiDevice> devices = new ArrayList<>();

    Collection<Device> systemDevices = SystemController.getAllDevices();

    if (digitalId.equals("null") && meshId.equals("null")) {
      for (Device device : systemDevices) {
        ApiDevice apiDevice = new ApiDevice(device);
        devices.add(apiDevice);
      }
    }
    else if (digitalId.equals("null")) {
      for (Device device : systemDevices) {
        if (device.getMeshId().equals(meshId)) {
          ApiDevice apiDevice = new ApiDevice(device);
          devices.add(apiDevice);
        }
      }
    }
    else {
      for (Device device : systemDevices) {
        if (device.getDigitalId().equals(digitalId)) {
          ApiDevice apiDevice = new ApiDevice(device);
          devices.add(apiDevice);
        }
      }
    }

    return devices;
  }

  /**
   * /api/meshes request takes a valid mesh meshId as input and returns current mesh data for the
   * specified meshId.  If no meshId is entered, then /api/meshes returns data for all meshes. If
   * any requested mesh returns a response of "null", then an empty list will be returned.
   *
   * @param meshId - the meshId of the requested mesh. meshId is "null" for all meshes.
   * @return - returns mesh data for the specified mesh, or data for all meshes if meshId is "null".
   */
  @RequestMapping("/api/meshes")
  public Collection<ApiMesh> meshes(
      @RequestParam(value = "meshId", defaultValue = "null") String meshId) {

    if (meshId.equals("null")) {
      ArrayList<ApiMesh> meshes = new ArrayList<>();

      Collection<Mesh> systemMeshes = SystemController.getAllMeshes();

      for (Mesh mesh : systemMeshes) {
        ApiMesh apiMesh = new ApiMesh(mesh);
        meshes.add(apiMesh);
      }

      return meshes;
    }
    Mesh systemMesh = SystemController.getMesh(meshId);

    if (systemMesh == null) {
      return Collections.emptyList();
    }

    ApiMesh apiMesh = new ApiMesh(systemMesh);

    Collection<ApiMesh> retMeshList = new ArrayList<>();
    retMeshList.add(apiMesh);

    return retMeshList;
  }

  /**
   * /api/devices/digitalIds takes a meshId string as input and returns the digitalIds for the
   * devices in the the requested mesh.  If no meshId is input, then a list containing all device
   * digitalId's for all meshes is returned.
   *
   * @param meshId - The meshId for the requested mesh.  "null" for a list of all digitalId's
   * @return - returns a list of digitalId's for devices in the requested mesh, or a list of all
   * device digitalId's if no meshId was specified.
   */
  @RequestMapping("/api/devices/digitalIds")
  public Collection<String> deviceDigitalIds(
      @RequestParam(value = "meshId", defaultValue = "null") String meshId) {
    Collection<String> digitalIds = new ArrayList<>();
    Collection<Device> devices = SystemController.getAllDevices();

    if (meshId.equals("null")) {
      for (Device device : devices) {
        ApiDevice apiDevice = new ApiDevice(device);
        digitalIds.add(apiDevice.getDigitalId());
      }
    } else {
      for (Device device : devices) {
        if (device.getMeshId().equals(meshId)) {
          ApiDevice apiDevice = new ApiDevice(device);
          digitalIds.add(apiDevice.getDigitalId());
        }
      }
    }
    return digitalIds;
  }

  /**
   * /api/meshes/meshIds requires on user input, and returns the meshId's for all meshes.
   *
   * @return - returns a list of meshId's for all meshes.
   */
  @RequestMapping("/api/meshes/meshIds")
  public Collection<String> meshMeshIds() {
    Collection<String> meshIds = new ArrayList<>();

    Collection<Mesh> meshes = SystemController.getAllMeshes();

    for (Mesh mesh : meshes) {
      meshIds.add(mesh.getMeshId());
    }

    return meshIds;
  }

  /**
   * /api/devices/status returns the current device health status for the requested digitalId.
   *
   * @param digitalId - digitalId for the requested device status.
   * @return - returns the current device status for the specified digitalId, or "null" if the
   * device specified by the requested digitalId is null.
   */
  @RequestMapping("/api/devices/status")
  public String deviceStatus(@RequestParam(value = "digitalId") String digitalId) {
    Device device = SystemController.getDevice(digitalId);
    if (device == null) {
      return "null";
    } else {
      return device.getStatus().toString();
    }
  }

  /**
   * /api/meshes/status returns the current mesh health status for the requested meshId.
   *
   * @param meshId - meshId for the requested mesh status.
   * @return - returns the current mesh status for the specified meshId, or "null" if the mesh
   * specified by the requested meshId is null.
   */
  @RequestMapping("/api/meshes/status")
  public String meshStatus(@RequestParam(value = "meshId") String meshId) {
    Mesh mesh = SystemController.getMesh(meshId);
    if (mesh == null) {
      return "null";
    } else {
      return mesh.getStatus().toString();
    }
  }

  /**
   * /api/devices/history returns the health history for the requested device, or an empty list if
   * the device history is null.
   *
   * @param digitalId - the digitalId for the requested device history
   * @return - returns a device history object for the requested device, or and empty list if the
   * history for the requested device is null.
   */
  @RequestMapping("/api/devices/history")
  public Collection<ApiHistoryEntry> deviceHistory(
      @RequestParam(value = "digitalId") String digitalId) {
    Collection<DeviceHistoryEntry> devHistEntry = SystemController.getDeviceHistory(digitalId);

    if (devHistEntry == null) {
      return Collections.emptyList();
    } else {
      Collection<ApiHistoryEntry> history = new ArrayList<>();

      for (DeviceHistoryEntry entry : devHistEntry) {
        ApiHistoryEntry apiHistoryEntry = new ApiHistoryEntry(entry);
        history.add(apiHistoryEntry);
      }

      return history;
    }
  }

  /**
   * /api/meshes/history returns the health history for the requested mesh, or an empty list if the
   * mesh history is null.
   *
   * @param meshId - the meshId for the requested mesh history
   * @return - returns a mesh history object for the requested mesh, or and empty list if the
   * history for the requested mesh is null.
   *
   * example api request api/meshes/history?meshId={INSERT_MESH_ID}
   */
  @RequestMapping("/api/meshes/history")
  public Collection<ApiHistoryEntry> meshHistory(@RequestParam(value = "meshId") String meshId) {
    Collection<MeshHistoryEntry> meshHistory = SystemController.getMeshHistory(meshId);

    if (meshHistory == null) {
      return Collections.emptyList();
    } else {
      Collection<ApiHistoryEntry> history = new ArrayList<>();

      for (MeshHistoryEntry entry : meshHistory) {
        ApiHistoryEntry apiHistoryEntry = new ApiHistoryEntry(entry);
        history.add(apiHistoryEntry);
      }

      return history;
    }
  }

  /**
   * /api/devices/count request takes a valid status as a String, and returns a count of devices
   * with a current status matching the requested value.  If "all" is entered, then a count of all
   * devices is returned regardless of current status value.
   *
   * @param status - user input of "healthy" or "unhealthy" requests a count of the meshes with the
   * specified status.  Input of "all" returns a count of all meshes.
   * @return - returns a count of meshes with current status of "healthy", "unhealthy", or all
   * meshes depending on status request.
   */
  @RequestMapping("/api/devices/count")
  public Integer deviceCount(
      @RequestParam(value = "status", defaultValue = "null") String status,
      @RequestParam(value = "meshId", defaultValue = "null") String meshId) {
    DeviceStatus selectedStatus = DeviceStatus.parseString(status);

    if (selectedStatus == null) {
      return SystemController.getDeviceCount();
    } else {
      Collection<Device> devices = SystemController.getAllDevices();

      int count = 0;

      for (Device device : devices) {
        if (device.getStatus() == selectedStatus) {
          count++;
        }
      }

      return count;
    }
  }

  /**
   * /api/meshes/count request takes a valid status as a String, and returns a count of meshes with
   * a current status matching the requested value.  If "all" is entered, then a count of all meshes
   * is returned regardless of current status value.
   *
   * @param status - user input of "healthy" or "unhealthy" requests a count of the meshes with the
   * specified status.  Input of "all" returns a count of all meshes.
   * @return - returns a count of meshes with current satuts of "healthy", "unhealthy", or all
   * meshes depending on status request.
   */
  @RequestMapping("/api/meshes/count")
  public Integer meshCount(@RequestParam(value = "status", defaultValue = "all") String status) {
    MeshStatus selectedStatus = MeshStatus.parseString(status);

    if (selectedStatus == null) {
      return SystemController.getMeshCount();
    } else {
      Collection<Mesh> meshes = SystemController.getAllMeshes();

      int count = 0;

      for (Mesh mesh : meshes) {
        if (mesh.getStatus() == selectedStatus) {
          count++;
        }
      }
      return count;
    }
  }
}
