/*
 * MIT License
 *
 * Copyright (c) 2018 Alex Michael, Andy Mayer, Bin Chen, Chhewang Sherpa, Mackenzie Wangenstein, Warren Black
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.nemo.dhms.api;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.nemo.dhms.system.Location;
import com.nemo.dhms.system.Mesh;

/**
 * ApiMesh Class, when called, creates an ApiMesh object containing the meshId, status, activateTime,
 * location, lastCaptureTime, and lastSystemUpdate time.  Methods equals() and hashcode() are utilized
 * object equivalence checks. A to toString() method is also included.
 */
class ApiMesh {

  private String meshId;
  private String lastDataTime;
  private String activateTime;
  private String status;
  private Location location;
  private String lastSystemUpdate;

  ApiMesh(Mesh mesh) {
    this.meshId = mesh.getMeshId();
    this.lastDataTime = String.valueOf(mesh.getLastDataTime());
    this.activateTime = mesh.getActivateTime().toString();

    if (mesh.getStatus() != null) {
      this.status = mesh.getStatus().toString();
    } else {
      this.status = null;
    }

    this.location = new Location(mesh.getLocation());
    this.lastSystemUpdate = mesh.getLastSystemUpdate().toString();
  }

  public String getMeshId() {
    return meshId;
  }

  public void setMeshId(String meshId) {
    this.meshId = meshId;
  }

  public String getLastDataTime() {
    return lastDataTime;
  }

  public void setLastDataTime(String lastDataTime) {
    this.lastDataTime = lastDataTime;
  }

  public String getActivateTime() {
    return activateTime;
  }

  public void setActivateTime(String activateTime) {
    this.activateTime = activateTime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public String getLastSystemUpdate() {
    return lastSystemUpdate;
  }

  public void setLastSystemUpdate(String lastSystemUpdate) {
    this.lastSystemUpdate = lastSystemUpdate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiMesh apiMesh = (ApiMesh) o;
    return Objects.equal(meshId, apiMesh.meshId) &&
        Objects.equal(lastDataTime, apiMesh.lastDataTime) &&
        Objects.equal(activateTime, apiMesh.activateTime) &&
        Objects.equal(status, apiMesh.status) &&
        Objects.equal(location, apiMesh.location) &&
        Objects.equal(lastSystemUpdate, apiMesh.lastSystemUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(meshId, lastDataTime, activateTime, status, location, lastSystemUpdate);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("meshId", meshId)
        .add("lastDataTime", lastDataTime)
        .add("activateTime", activateTime)
        .add("status", status)
        .add("location", location)
        .add("lastSystemUpdate", lastSystemUpdate)
        .toString();
  }
}
