# SmartVineyards Device Health Monitoring System

Device Health Monitoring System developed for SmartVineyards as part of the Portland State University Computer Science capstone program.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project can be run on any machine running the Java JVM.


### Installing

1. Download the maven-build artifact for this branch from the gitlab repository.

2. Run the jar downloaded or use the command line

```
java -jar dhms-X.X.X.jar
```

Open your web browser to: 

```
http://localhost/
```

## Deployment

This project is not considered ready for production deployment and should not be used as such.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Boot](https://spring.io/) - Web Framework
* [Vaadin](https://vaadin.com/) - UI Framework
* [ChartJS](https://github.com/moberwasserlechner/vaadin-chartjs) - Vaadin Add-on for ChartJS Charts
* [JSON in Java](http://www.JSON.org/) - Java JSON Library

## Contributing

As this project is currently under development as part of the Portland State University Computer Science Capstone program, we are currently not accepting contributions of any kind to this repository.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Alex Michael** - [@.alex](https://gitlab.com/.alex)
* **Andy Mayer** - [@mayermic](https://gitlab.com/mayermic)
* **Bin Chen** - [@binc](https://gitlab.com/binc)
* **Chewwang Sherpa** - [@chsherpa](https://gitlab.com/chsherpa)
* **Mackenzie Wangenstein** - [@mklitzke82](https://gitlab.com/mklitzke82)
* **Warren Black** - [@warblack](https://gitlab.com/warblack)

See also the list of [contributors](https://gitlab.com/mayermic/smartvineyards-dhms/graphs/develop) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Thank you Professor Bart Massey for the guidance and support during this project.